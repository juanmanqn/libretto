<?php

class WebUser extends CWebUser {

    /**
     * Overrides a Yii method that is used for roles in controllers (accessRules).
     *
     * @param string $operation Name of the operation required (here, a role).
     * @param mixed $params (opt) Parameters for this operation, usually the object to access.
     * @return bool Permission granted?
     */
    public function checkAccess($operation, $params = array()) {
        if (empty($this->id)) {
            // Not identified => no rights
            return false;
        }
        $role = $this->getState("Rol");
        if ($role === 'administrador') {
            return true; // admin role has access to everything
        }
        /**
         * chequeo si está asignado al Cuadernoes usuario asignado al proyecto
         */
        if ($operation === 'usuarioAsignadoCuaderno') {
            /* @var Usuario $usuario */
            /* if($role=='inspector'){
              return TRUE;
              } */

            $idUsuario = $this->getState('Id');
            if (isset($params['cuaderno'])) {
                $cuaderno = $params['cuaderno'];
                /* @var $cuaderno Cuaderno */
                if ($cuaderno->IdProductor == $idUsuario) {
                    return true;
                } elseif ($cuaderno->IdProfesional == $idUsuario) {
                    return true;
                } else {

                    foreach ($cuaderno->permisos as $permiso) {
                        /* @var $permiso Permisos */
                        if ($permiso->IdUsuario == $idUsuario) {
                            return true;
                        }
                    }
                }
            }
        }

        // allow access if the operation request is the current user's role
        return ($operation === $role);
    }

}
