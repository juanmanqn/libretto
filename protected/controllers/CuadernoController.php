<?php

class CuadernoController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $idCuaderno;

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('view', 'descargar'),
                'users' => array('@'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'delete'),
                'roles' => array('productor','profesional'),
            ),
           
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        /* TODO: chequear acceso */
        $model = $this->loadModel($id);
        $model->usuarioVinculado();
        $this->render('view', array(
            'model' => $model,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($idExplotacion) {
        $model = new Cuaderno;
        
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $explotacion = Explotacion::model()->findByPk($idExplotacion);
        /* @var $explotacion Explotacion */

        if($explotacion->IdProductor!=Yii::app()->user->Id){
             throw new CHttpException('404', 'El usuario no es el productor de la exploación');
        }
        
        if (isset($_POST['Cuaderno'])) {
            $model->attributes = $_POST['Cuaderno'];
            $model->IdExplotacion = $idExplotacion;
            $model->IdProductor = $explotacion->IdProductor;
            $model->ProductorIdLocalidad = $explotacion->idProductor->id->IdLocalidad;
            $model->ProductorNombreApellido = $explotacion->idProductor->id->NombreApellido;
            $model->NombreChacra = $explotacion->NombreChacra;
            $model->Estado = 1;
            $this->guardarArchivo($model, 'UbicacionHuerto');
            $this->guardarArchivo($model, 'UbicacionUmis');
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->Id));
        }

        $this->render('create', array(
            'model' => $model,
            'explotacion' => $explotacion
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $model->usuarioVinculado();
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Cuaderno'])) {
            $model->attributes = $_POST['Cuaderno'];
            $this->guardarArchivo($model, 'UbicacionHuerto');

            $this->guardarArchivo($model, 'UbicacionUmis');
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->Id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * 
     * @param Cuaderno $model
     * @return boolean
     * @throws CHttpException
     */
    protected function guardarArchivo(&$model, $atributo) {
        $nombreViejo = $model->getAttribute($atributo);

        $file = CUploadedFile::getInstance($model, $atributo);

        if (is_object($file) && get_class($file) == 'CUploadedFile') {      //
            $path = Yii::app()->basePath . DIRECTORY_SEPARATOR . 'archivos' . DIRECTORY_SEPARATOR . 'cuaderno';
            if (!is_dir($path))
                mkdir($path);

            /* --------------------------------- */
            $array = pathinfo($file->name);
            //print_r($array);exit;
            if (isset($array['extension'])) {
                $ext = strtolower($array['extension']);
            } else {
                $ext = '';
            }

            $nombrearchivo = str_replace(' ', '', str_replace('/','',$model->NumeroRenspa) . $atributo . date('Ymdm-his')) . '.' . $ext;
            $url = $path . DIRECTORY_SEPARATOR . $nombrearchivo; //url definitiva al archivo                                                            

            /* --------------------------------- */

            if (empty($ext) OR ( $ext != 'pdf' && $ext != 'png' && $ext != 'jpg' && $ext != 'jpeg' && $ext != 'gif')) {
                throw new CHttpException(204, " Solo puede subir archivos con extension (.txt, .pdf, .png, .jpg, .gif");
            } else {
                $file->saveAs($url);
                $model->setAttribute($atributo, $nombrearchivo);
            }
        }

        if ($model->getAttribute($atributo) == '') {
            $model->setAttribute($atributo, $nombreViejo);
        }
        return true;
    }

    /**
     * 
     * @param int $id del Cuaderno
     * @param string $tipo Umis\Huerto
     * @throws CHttpException
     */
    public function actionDescargar($idCuaderno, $tipo) {

        $model = $this->loadModel($idCuaderno);
        $model->usuarioVinculado();
        if ($tipo == 'Umis') {
            $archivo = $model->UbicacionUmis;
        } elseif ($tipo == 'Huerto') {
            $archivo = $model->UbicacionHuerto;
        } else {
            throw new CHttpException(204, 'Error en Tipo de archivo');
        }



        $path = Yii::app()->basePath . DIRECTORY_SEPARATOR . 'archivos' . DIRECTORY_SEPARATOR . 'cuaderno' . DIRECTORY_SEPARATOR;


        $file = $path . $archivo;
        //echo $file;

        /* ---------------------------- */


        /* ---------------------------- */

        $dirInfo = pathinfo($file);
        //print_r($dirInfo); exit;
        $nombre = $dirInfo['basename'];

        if (isset($dirInfo['extension'])) {
            $ext = strtolower($dirInfo['extension']);
        } else {
     
            //exit('hola');
            $this->render('error', array(
                'model'=>$model,
            'mensaje' => 'No se ha subido el Croquis',
        ));
            exit();
            //throw new Exception('No se ha subido el Croquis');
        }

        /* ----------------------------- */
        if (strtolower($ext) == 'pdf')
            header('Content-type: application/pdf');

        header("Content-Type: application/force-download");
        header("Content-disposition: attachment; filename=" . $archivo);
        /* ------------------------------ */
        //echo $file;exit;
        $file = file_get_contents($file);
        echo $file;
        die;
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $model = $this->loadModel($id);
        $model->usuarioVinculado();
        $idExplotacion = $model->IdExplotacion;
        $model->delete();
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('explotacion/view', 'id' => $idExplotacion));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Cuaderno');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Cuaderno('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Cuaderno']))
            $model->attributes = $_GET['Cuaderno'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Cuaderno the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Cuaderno::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Cuaderno $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'cuaderno-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
