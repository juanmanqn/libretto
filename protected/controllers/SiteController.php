<?php

class SiteController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }
     /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionAbout() {
        $this->render('about');
    }
    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        if (Yii::app()->user->isGuest) {
            $this->render('index');
        } else {
            switch (Yii::app()->user->Rol) {
                case 'administrador':
                    $this->render('homeadministrador', array());
                    break;
                case 'productor':
//                    $model = new Explotacion('search');
//                    $model->unsetAttributes();  // clear any default values
//                    if (isset($_GET['Explotacion']))
//                        $model->attributes = $_GET['Explotacion'];
//
//                    $this->render('homeproductor', array(
//                        'model' => $model,
//                    ));                    
                    $this->redirect(array('/explotacion/admin'));
                    break;
                case 'profesional':
                    $this->indexProfesional();
                    break;
                case 'inspector':
                    $this->indexInspector();
                    break;
            }
        }
    }

    protected function indexProfesional() {
        $model = new Permisos('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Permisos']))
            $model->attributes = $_GET['Permisos'];
        $model->IdUsuario = Yii::app()->user->Id;
        $this->render('homeprofesional', array(
            'model' => $model,
        ));
    }

    protected function indexInspector() {

        $model = new Permisos('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Permisos']))
            $model->attributes = $_GET['Permisos'];
        $model->IdUsuario = Yii::app()->user->Id;
        $this->render('homeinspector', array(
            'model' => $model,
        ));
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact() {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                        "Reply-To: {$model->email}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-Type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Gracias por contactarnos.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the contact page
     */
    public function actionAyuda() {
        if (!Yii::app()->user->isGuest)
            $usuario = Usuario::model()->findByPk(Yii::app()->user->Id);
        else {
            $usuario = null;
        }
        $this->render('ayuda', array('usuario' => $usuario));
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
                $this->redirect(Yii::app()->user->returnUrl);
            }
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionRegistro() {
        $model = new Usuario;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Usuario'])) {
            $model->attributes = $_POST['Usuario'];
            if ($model->save()) {
                if ($model->IdRol == 3) {
                    $productor = new Productor();
                    $productor->Id = $model->Id;
                    $productor->save();
                } elseif ($model->IdRol == 4) {
                    $profesional = new Profesional();
                    $profesional->Id = $model->Id;
                    $profesional->save();
                }

                $LoginForm = new LoginForm;
                $LoginForm->password = $model->Clave;
                $LoginForm->username = $model->Usuario;
                if ($LoginForm->login())
                    $this->redirect(array('/site/index'));
                else
                    $this->redirect(array('/site/login'));
            }
        }

        $this->render('registro', array(
            'model' => $model,
        ));
    }

}
