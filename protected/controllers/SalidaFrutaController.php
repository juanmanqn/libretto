<?php

class SalidaFrutaController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
        public $idCuaderno;
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','grafico','admin','imprimir'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','delete'),
				'roles'=>array('productor','profesional'),
			),			
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model=$this->loadModel($id);
                $cuaderno = $model->idCuaderno;
                $cuaderno->usuarioVinculado();
		$this->render('view',array(
			'model'=>$this->loadModel($id),                        
                        'cuaderno'=>$cuaderno
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($idCuaderno)
	{
		$model=new SalidaFruta;
                $cuaderno = Cuaderno::model()->findByPk($idCuaderno);
                $cuaderno->usuarioVinculado();
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SalidaFruta']))
		{
			$model->attributes=$_POST['SalidaFruta'];
                        $model->IdCuaderno = $idCuaderno;
			if($model->save())
				$this->redirect(array('admin','idCuaderno'=>$cuaderno->Id));
		}

		$this->render('create',array(
			'model'=>$model,
                        'cuaderno'=>$cuaderno
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
                $cuaderno = $model->idCuaderno;
                $cuaderno->usuarioVinculado();
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SalidaFruta']))
		{
			$model->attributes=$_POST['SalidaFruta'];
			if($model->save())
				$this->redirect(array('admin','idCuaderno'=>$cuaderno->Id));
		}

		$this->render('update',array(
			'model'=>$model,
                        'cuaderno'=>$cuaderno
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model=$this->loadModel($id);
                $model->idCuaderno->usuarioVinculado();
                
                $model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SalidaFruta');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin($idCuaderno)
	{
		$model=new SalidaFruta('search');
                $cuaderno = Cuaderno::model()->findByPk($idCuaderno);
                $cuaderno->usuarioVinculado();
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SalidaFruta']))
			$model->attributes=$_GET['SalidaFruta'];
                
                $model->IdCuaderno = $idCuaderno;

		$this->render('admin',array(
			'model'=>$model,
                        'cuaderno'=>$cuaderno
		));
	}

        public function actionImprimir($idCuaderno)
        {
            /* @var $cuaderno Cuaderno */
            $model=new SalidaFruta('search');
            $cuaderno = Cuaderno::model()->findByPk($idCuaderno);
            $cuaderno->usuarioVinculado();
                
            $this->render('imprimir', array(
                'model'=>$model,
                'cuaderno'=>$cuaderno                
            ));
        }
        
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return SalidaFruta the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=SalidaFruta::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param SalidaFruta $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='salida-fruta-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        public function actionGrafico($idCuaderno)
	{		
                $cuaderno = Cuaderno::model()->findByPk($idCuaderno);
		
                $modeloSalida = SalidaFruta::model()->findAll(array('order'=>'IdUmi, IdVariedad', 'condition'=>'IdCuaderno=:id', 'params' => array(':id'=> $cuaderno->Id)));
                
                $umi = "";
                $umis = array();
                $variedades = array();
                $id = -1;

                foreach ($modeloSalida as $salida) {
                    /* @var $salida SalidaFruta */

                    if ($umi != $salida->idUmi->Codigo){
                        $umi = $salida->idUmi->Codigo;
                        $umis[] = $umi;
                        $id++;        
                    }
                    if (!(isset($variedades[$salida->idVariedad->Codigo]))){
                        $variedades[$salida->idVariedad->Codigo] = array('name'=>$salida->idVariedad->Codigo);
                    }
                    $variedades[$salida->idVariedad->Codigo]['data'][$id] = $salida->getRindeKgHa();
                }
                $variedades2 = array();
                foreach ($variedades as $key => $variedad){
                    $variedades2[$key] = array('name'=>$key);
                    for ($index = 0; $index <= $id; $index++) {
                        if(!(isset($variedad['data'][$index]))){ 
                            $variedades2[$key]['data'][$index] = 0;
                        }
                        else{
                            $variedades2[$key]['data'][$index] = $variedad['data'][$index];
                        }

                    }

                }
                
		$this->render('grafico',array(
                        'cuaderno'=>$cuaderno,
                        'umis' => $umis,
                        'variedades' => $variedades2
		));
	}
}
