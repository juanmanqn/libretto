<?php

class CambioClaveController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'captcha'),
                'users' => array('*'),
            ),

            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }


    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new CambioClave;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['CambioClave'])) {
            $model->attributes = $_POST['CambioClave'];
            if ($model->validate()) {
                $usuario = Usuario::model()->find('Email=:Email', array(':Email' => $model->email));
                /**
                 * @var $usuario Usuario
                 * */
                if (is_null($usuario)) {
                    $model->addError('email', 'No se encuentra registrado el mail.');
                } else {
                    /*$model->idUsuario = $usuario->Id;
                    $model->token = md5(microtime() . $usuario->idUsuario);
                    /*
                     * envia mail
                     */
                    //$name = '=?UTF-8?B?' . base64_encode($usuario->NombreUsuario) . '?=';
                    $subject = '=?UTF-8?B?' . base64_encode('Cambio Clave Libretto') . '?=';
                    $headers = "From: Libretto <" . Yii::app()->params['adminEmail'] . ">\r\n" .
                            "Reply-To: " . Yii::app()->params['adminEmail'] . "\r\n" .
                            "MIME-Version: 1.0\r\n" .
                            "Content-type: text/plain; charset=UTF-8";
                    $envio = mail($model->email, $subject, 'Su usuario es: "' . $usuario->NombreApellido
                            . '" y su clave es: "' . $usuario->Clave . '".'
                            . 'Ingresar a ' . Yii::app()->baseUrl . Yii::app()->createUrl("/site/login"), $headers);
                    if ($envio) {


                        Yii::app()->user->setFlash('contact', 'Se ha enviado un mail a ' . $model->email .
                                '. Siga las instrucciones del mail para actualizar su clave.');
                        $this->refresh();
                        $this->render('create', array(
                            'model' => $model,
                        ));
                    } else {
                        $model->addError('email', 'No se pudo enviar el mail');
                    }
                }
            }
        }
        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
 


    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return CambioClave the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = CambioClave::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CambioClave $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'cambio-clave-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
