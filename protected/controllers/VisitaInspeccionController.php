<?php

class VisitaInspeccionController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
        
        public $idCuaderno;
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','admin'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'roles'=>array('inspector'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete'),
				'roles'=>array('admininstrador'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model=$this->loadModel($id);
                $cuaderno = $model->idCuaderno;
                $cuaderno->usuarioVinculado();
		$this->render('view',array(
			'model'=>$this->loadModel($id),                        
                        'cuaderno'=>$cuaderno
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($idCuaderno)
	{
		$model=new VisitaInspeccion;
                $cuaderno = Cuaderno::model()->findByPk($idCuaderno);
                $cuaderno->usuarioVinculado();
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['VisitaInspeccion']))
		{
			
                        $model->attributes=$_POST['VisitaInspeccion'];
                        $model->IdCuaderno = $idCuaderno;
                        
                        $inspector= Inspector::model()->findByPk(Yii::app()->user->Id);
                        if(is_null($inspector)){
                            throw new CHttpException('404','No existe el inspector');
                        }
                        /* @var $inspector Inspector */
                        $model->IdInspector=Yii::app()->user->Id;
                        $model->NombreApellido=$inspector->id->NombreApellido;
                        $model->Legajo=$inspector->Legajo;
                        
			if($model->save())
				$this->redirect(array('admin','idCuaderno'=>$cuaderno->Id));
		}

		$this->render('create',array(
			'model'=>$model,
                        'cuaderno'=>$cuaderno 
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
                $cuaderno = $model->idCuaderno;
                $cuaderno->usuarioVinculado();
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['VisitaInspeccion']))
		{
			$model->attributes=$_POST['VisitaInspeccion'];
                        $inspector= Inspector::model()->findByPk(Yii::app()->user->Id);
                        /* @var $inspector Inspector */
                        $model->IdInspector=Yii::app()->user->Id;
                        $model->NombreApellido=$inspector->id->NombreApellido;
                        $model->Legajo=$inspector->Legajo;
			if($model->save())
				$this->redirect(array('admin','idCuaderno'=>$cuaderno->Id));
		}

		$this->render('update',array(
			'model'=>$model,            
                        'cuaderno'=>$cuaderno 
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model=$this->loadModel($id);
                $model->idCuaderno->usuarioVinculado();
                
                $model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('VisitaInspeccion');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin($idCuaderno)
	{
		$model=new VisitaInspeccion('search');
                $cuaderno = Cuaderno::model()->findByPk($idCuaderno);
                $cuaderno->usuarioVinculado();
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['VisitaInspeccion']))
			$model->attributes=$_GET['VisitaInspeccion'];

		$model->IdCuaderno=$idCuaderno;
		$this->render('admin',array(
			'model'=>$model,
                        'cuaderno'=>$cuaderno
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return VisitaInspeccion the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=VisitaInspeccion::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param VisitaInspeccion $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='visita-inspeccion-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
