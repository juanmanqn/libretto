-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-06-2014 a las 16:20:10
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `elcuaderno`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `AplicacionTratamiento`
--

CREATE TABLE IF NOT EXISTS `AplicacionTratamiento` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdTratamientoFitosanitario` int(11) DEFAULT NULL,
  `FechaHoraInicio` datetime DEFAULT NULL,
  `FechaHoraFin` datetime DEFAULT NULL,
  `IdEquipo` int(11) DEFAULT NULL,
  `LitrosCuadro` int(11) DEFAULT NULL,
  `FechaCumplimientoTC` date DEFAULT NULL,
  `Clima` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `CapturaCarpocapsa`
--

CREATE TABLE IF NOT EXISTS `CapturaCarpocapsa` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCuadro` int(11) NOT NULL,
  `NumeroTrampa` int(11) NOT NULL,
  `Fecha` date DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Cuaderno`
--

CREATE TABLE IF NOT EXISTS `Cuaderno` (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Numero de Cuaderno',
  `NumeroCuaderno` int(11) NOT NULL DEFAULT '0',
  `IdProductor` int(11) NOT NULL,
  `IdExplotacion` int(11) NOT NULL,
  `Temporada` varchar(45) NOT NULL,
  `IdProfesional` int(11) DEFAULT NULL,
  `Recorredor` varchar(100) DEFAULT NULL,
  `TelefonoRecorredor` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdProductor` (`IdProductor`),
  KEY `IdExplotacion` (`IdExplotacion`),
  KEY `IdTecnico` (`IdProfesional`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `Cuaderno`
--

INSERT INTO `Cuaderno` (`Id`, `NumeroCuaderno`, `IdProductor`, `IdExplotacion`, `Temporada`, `IdProfesional`, `Recorredor`, `TelefonoRecorredor`) VALUES
(1, 10000, 2, 3, '2014/2015', 4, 'Jose Recorredor', '444444'),
(2, 4534535, 1, 1, '2014/2015', 3, 'Jose Recorredor', '444444'),
(3, 45345366, 4, 6, '2014/2015', 5, 'Jose Recorredor', '444444'),
(4, 4534539, 5, 5, '2014/2015', 2, 'Jose Recorredor', '444444');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Cuadro`
--

CREATE TABLE IF NOT EXISTS `Cuadro` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdUmi` int(11) NOT NULL,
  `NumeroCuadro` int(11) NOT NULL,
  `Altura` decimal(10,0) NOT NULL,
  `Ancho` decimal(10,0) NOT NULL,
  `DistanciaFilas` decimal(10,0) NOT NULL,
  `VolumenTrv` decimal(10,0) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Cuadro_Umi` (`IdUmi`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `Cuadro`
--

INSERT INTO `Cuadro` (`Id`, `IdUmi`, `NumeroCuadro`, `Altura`, `Ancho`, `DistanciaFilas`, `VolumenTrv`) VALUES
(1, 1, 1, '80', '80', '8', '888'),
(2, 2, 2, '50', '50', '8', '888'),
(3, 4, 3, '54', '50', '8', '888'),
(4, 5, 4, '54', '50', '8', '888'),
(5, 3, 1, '54', '50', '8', '888'),
(6, 6, 2, '54', '50', '8', '888'),
(7, 7, 3, '54', '50', '8', '888');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Departamento`
--

CREATE TABLE IF NOT EXISTS `Departamento` (
  `Id` int(11) NOT NULL,
  `Nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Departamento`
--

INSERT INTO `Departamento` (`Id`, `Nombre`) VALUES
(1, 'Confluencia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Equipo`
--

CREATE TABLE IF NOT EXISTS `Equipo` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `NumeroEquipo` int(11) NOT NULL,
  `Tipo` varchar(45) NOT NULL,
  `Marca` varchar(45) NOT NULL,
  `Modelo` varchar(45) DEFAULT NULL,
  `Anio` varchar(45) NOT NULL,
  `Potencia` int(11) DEFAULT NULL,
  `Volumen` int(11) DEFAULT NULL,
  `Tractorista` varchar(100) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `Equipo`
--

INSERT INTO `Equipo` (`Id`, `NumeroEquipo`, `Tipo`, `Marca`, `Modelo`, `Anio`, `Potencia`, `Volumen`, `Tractorista`) VALUES
(2, 2, 'Pulverizadora', 'asdad', 'asdad', '12313', 234, 234, 'Otro tractorista');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Explotacion`
--

CREATE TABLE IF NOT EXISTS `Explotacion` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `NombreChacra` varchar(45) NOT NULL,
  `NumeroInterno` varchar(45) DEFAULT NULL,
  `IdLocalidad` int(11) NOT NULL,
  `SuperficieTotal` decimal(10,2) NOT NULL,
  `SuperficieNeta` decimal(10,2) NOT NULL,
  `IdUbicacion` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `fk_Explotacion_Localidad` (`IdLocalidad`),
  KEY `fk_Explotacion_Ubicacion` (`IdUbicacion`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `Explotacion`
--

INSERT INTO `Explotacion` (`Id`, `NombreChacra`, `NumeroInterno`, `IdLocalidad`, `SuperficieTotal`, `SuperficieNeta`, `IdUbicacion`) VALUES
(1, 'Los frutales', '5', 1, '345.00', '345.00', NULL),
(3, 'Paraiso', '23', 2, '123.00', '122.00', 1),
(4, 'La estancia', '4', 4, '234.00', '234.00', NULL),
(5, 'La tranquera', '2', 5, '434.00', '343.00', NULL),
(6, 'La vaquita', '3', 2, '123.00', '122.00', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `FormularioA`
--

CREATE TABLE IF NOT EXISTS `FormularioA` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Fecha` date DEFAULT NULL,
  `IdProductor` int(11) DEFAULT NULL,
  `IdExplotacion` int(11) DEFAULT NULL,
  `ObservacionesSenasa` varchar(200) DEFAULT NULL,
  `FechaSenasa` date DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ItemFormularioA`
--

CREATE TABLE IF NOT EXISTS `ItemFormularioA` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdFormularioA` int(11) DEFAULT NULL,
  `NumeroCuadro` int(11) DEFAULT NULL,
  `SuperficieCuadro` decimal(10,0) DEFAULT NULL,
  `Especie` varchar(45) DEFAULT NULL,
  `TipoTCS` varchar(45) DEFAULT NULL,
  `DispNroPorCuadroDosis1` decimal(10,0) DEFAULT NULL,
  `DispNroPorHectareaDosis1` decimal(10,0) DEFAULT NULL,
  `DispNroPorCuadroDosis2` decimal(10,0) DEFAULT NULL,
  `DispNroPorHectareaDosis2` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `LaborCultural`
--

CREATE TABLE IF NOT EXISTS `LaborCultural` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCuadro` int(11) DEFAULT NULL,
  `Descripcion` varchar(200) NOT NULL,
  `Fecha` date NOT NULL,
  `FechaFinalizacion` date DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_LaborCultural_Cuadro` (`IdCuadro`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `LaborCultural`
--

INSERT INTO `LaborCultural` (`Id`, `IdCuadro`, `Descripcion`, `Fecha`, `FechaFinalizacion`) VALUES
(1, 1, 'Poda', '2014-06-11', '2014-06-11'),
(2, 2, 'Poda', '2014-06-11', '2014-06-11'),
(3, 1, 'Raleo', '2014-06-11', '2014-06-11'),
(4, 3, 'Poda', '2014-06-11', '2014-06-11'),
(5, 3, 'Raleo', '2014-06-11', '2014-06-11'),
(6, 3, 'Cosecha', '2014-06-11', '2014-06-11'),
(7, 1, 'Cosecha', '2014-06-11', '2014-06-11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Localidad`
--

CREATE TABLE IF NOT EXISTS `Localidad` (
  `Id` int(11) NOT NULL,
  `Nombre` varchar(200) NOT NULL,
  `IdDepartamento` int(11) DEFAULT NULL,
  `CodigoPostal` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdDepartamento` (`IdDepartamento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Localidad`
--

INSERT INTO `Localidad` (`Id`, `Nombre`, `IdDepartamento`, `CodigoPostal`) VALUES
(1, 'Neuquén', 1, '8300'),
(2, 'Centenario', 1, '098'),
(3, 'San Patricio del Chañar', 1, '3453'),
(4, 'Plottier', 1, '234'),
(5, 'Senillosa', 1, '8888');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `MonitoreoPlagas`
--

CREATE TABLE IF NOT EXISTS `MonitoreoPlagas` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdTipoPlaga` int(11) DEFAULT NULL,
  `Mes` varchar(45) DEFAULT NULL,
  `Semana` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `MuestreoDanioCarpocapsa`
--

CREATE TABLE IF NOT EXISTS `MuestreoDanioCarpocapsa` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCuadro` int(11) DEFAULT NULL,
  `TipoVariedad` varchar(45) DEFAULT NULL,
  `Superficie` decimal(10,0) DEFAULT NULL,
  `Fecha` date DEFAULT NULL,
  `Muestra` int(11) DEFAULT NULL,
  `FrutosDaniados` int(11) DEFAULT NULL,
  `PorcentajeDanio` decimal(10,0) DEFAULT NULL,
  `FechaCosecha` date DEFAULT NULL,
  `MuestraCosecha` int(11) DEFAULT NULL,
  `FrutosDaniadosCosecha` int(11) DEFAULT NULL,
  `PorcentajeDanioCosecha` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Plantacion`
--

CREATE TABLE IF NOT EXISTS `Plantacion` (
  `Id` int(11) NOT NULL,
  `IdCuadro` int(11) NOT NULL,
  `TipoProduccion` varchar(45) NOT NULL,
  `Especie` varchar(45) NOT NULL,
  `Variedad` varchar(45) NOT NULL,
  `Pie` varchar(45) NOT NULL,
  `Cond` varchar(45) NOT NULL,
  `Anio` varchar(45) NOT NULL,
  `DFilas` decimal(10,0) NOT NULL,
  `DPlantas` decimal(10,0) NOT NULL,
  `NumeroPlantas` int(11) NOT NULL,
  `SuperficieNeta` decimal(10,0) NOT NULL,
  `Catastro` varchar(45) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Productor`
--

CREATE TABLE IF NOT EXISTS `Productor` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `NumeroRenspa` varchar(100) NOT NULL,
  `Nombre` varchar(100) NOT NULL,
  `Domicilio` varchar(200) NOT NULL,
  `IdLocalidad` int(11) NOT NULL,
  `Telefono` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdLocalidad` (`IdLocalidad`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `Productor`
--

INSERT INTO `Productor` (`Id`, `NumeroRenspa`, `Nombre`, `Domicilio`, `IdLocalidad`, `Telefono`) VALUES
(1, '24524525', 'Pepe Argento', 'Belgrano 1334', 1, '453535'),
(2, '345', 'Carlitos Mendez', 'San Martin 321', 2, '4545'),
(4, '34535', 'Jose Perez', 'Avenida 123', 2, '15555555'),
(5, '545454', 'Juan Martin', 'Sarmiento 432', 1, '155544444');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Profesional`
--

CREATE TABLE IF NOT EXISTS `Profesional` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) NOT NULL,
  `Matricula` varchar(100) DEFAULT NULL,
  `Direccion` varchar(200) NOT NULL,
  `IdLocalidad` int(11) DEFAULT NULL,
  `Telefono` varchar(45) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `fk_Tecnico_Localidad` (`IdLocalidad`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `Profesional`
--

INSERT INTO `Profesional` (`Id`, `Nombre`, `Matricula`, `Direccion`, `IdLocalidad`, `Telefono`, `Email`) VALUES
(2, 'Roberto Gomez', '123456', 'Calle falsa 123', 1, '1231', 'asd@sda.com'),
(3, 'Tomas Sanchez', '234243', 'Godoy 243', 2, '55656', 'tomas@mail'),
(4, 'Carlos Coto', '23244', 'Antartida 2342', 2, '324424', 'carlos@mail'),
(5, 'Jose Lopez', '2342', 'Avenida 4252', 1, '564564', 'jose@mail');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `RegistrosFenologicos`
--

CREATE TABLE IF NOT EXISTS `RegistrosFenologicos` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCuaderno` int(11) NOT NULL,
  `Especie` varchar(45) NOT NULL,
  `Variedad` varchar(100) NOT NULL,
  `InicioFloracion` date DEFAULT NULL,
  `PlenaFloracion` date DEFAULT NULL,
  `CaidaPetalos` date DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Rol`
--

CREATE TABLE IF NOT EXISTS `Rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Rol` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `Rol`
--

INSERT INTO `Rol` (`id`, `Rol`) VALUES
(1, 'administrador'),
(2, 'empleado'),
(3, 'cliente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `SalidaFruta`
--

CREATE TABLE IF NOT EXISTS `SalidaFruta` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCuaderno` int(11) NOT NULL,
  `IdUmi` int(11) DEFAULT NULL,
  `Fecha` date NOT NULL,
  `Variedad` varchar(45) DEFAULT NULL,
  `CantidadBins` int(11) DEFAULT NULL,
  `NumeroRemito` varchar(100) DEFAULT NULL,
  `Destino` varchar(100) DEFAULT NULL,
  `Observaciones` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `TipoPlaga`
--

CREATE TABLE IF NOT EXISTS `TipoPlaga` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) NOT NULL COMMENT 'Nombre de la plaga, predador o enfermedad',
  `Tipo` varchar(45) DEFAULT NULL COMMENT 'Tipo: Plaga, Predador o Enfermedad',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `TratamientoFitosanitario`
--

CREATE TABLE IF NOT EXISTS `TratamientoFitosanitario` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdEquipo` int(11) DEFAULT NULL,
  `NombreComercial` varchar(100) DEFAULT NULL,
  `PrincipioActivo` varchar(100) DEFAULT NULL,
  `DosisMaquina` decimal(10,0) DEFAULT NULL,
  `TiempoCarencia` int(11) DEFAULT NULL,
  `TiempoReingreso` int(11) DEFAULT NULL,
  `NombreAplicador` varchar(100) DEFAULT NULL,
  `Motivo` varchar(45) DEFAULT NULL,
  `Variedad` varchar(100) DEFAULT NULL,
  `Observaciones` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Ubicacion`
--

CREATE TABLE IF NOT EXISTS `Ubicacion` (
  `Id` int(11) NOT NULL,
  `UbicacionUmis` varchar(200) NOT NULL,
  `UbicacionHuerto` varchar(200) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Ubicacion`
--

INSERT INTO `Ubicacion` (`Id`, `UbicacionUmis`, `UbicacionHuerto`) VALUES
(1, '1', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Umi`
--

CREATE TABLE IF NOT EXISTS `Umi` (
  `Id` int(11) NOT NULL,
  `Codigo` varchar(45) DEFAULT NULL,
  `Descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Umi`
--

INSERT INTO `Umi` (`Id`, `Codigo`, `Descripcion`) VALUES
(1, '78W', 'umi 78'),
(2, '79W', 'umi 79'),
(3, '78X', 'umi 78 X'),
(4, '78Y', 'umi 78 Y'),
(5, '79Y', 'umi 79 Y'),
(6, '79Z', 'umi 79 Z'),
(7, '78Z', 'umi 78 Z');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Usuario`
--

CREATE TABLE IF NOT EXISTS `Usuario` (
  `Usuario` varchar(255) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Clave` varchar(32) NOT NULL,
  `idRol` int(11) NOT NULL,
  `NombreApellido` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Usuario` (`Usuario`),
  KEY `idRol` (`idRol`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `Usuario`
--

INSERT INTO `Usuario` (`Usuario`, `id`, `Clave`, `idRol`, `NombreApellido`, `Email`) VALUES
('admin', 1, 'admin', 1, 'Administra Administrador', 'admin@tormenta.com'),
('desarrollador', 2, 'desarrollador', 2, 'Emplea Empleado', 'empleado@tormenta.com'),
('cliente', 3, 'cliente', 3, 'Clie Cliente', 'cliente@empresacliente.com'),
('usu1', 4, 'usu1', 2, 'Usu Uno', 'usu@tengo.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `VisitaInspeccion`
--

CREATE TABLE IF NOT EXISTS `VisitaInspeccion` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCuaderno` int(11) NOT NULL,
  `Fecha` date DEFAULT NULL,
  `Observaciones` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `Cuaderno`
--
ALTER TABLE `Cuaderno`
  ADD CONSTRAINT `fk_Cuaderno_Explotacion` FOREIGN KEY (`IdExplotacion`) REFERENCES `Explotacion` (`Id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Cuaderno_Productor` FOREIGN KEY (`IdProductor`) REFERENCES `Productor` (`Id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Cuaderno_Profesional` FOREIGN KEY (`IdProfesional`) REFERENCES `Profesional` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Cuadro`
--
ALTER TABLE `Cuadro`
  ADD CONSTRAINT `FK_Cuadro_Umi` FOREIGN KEY (`IdUmi`) REFERENCES `Umi` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Explotacion`
--
ALTER TABLE `Explotacion`
  ADD CONSTRAINT `fk_Explotacion_Localidad` FOREIGN KEY (`IdLocalidad`) REFERENCES `Localidad` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Explotacion_Ubicacion` FOREIGN KEY (`IdUbicacion`) REFERENCES `Ubicacion` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `LaborCultural`
--
ALTER TABLE `LaborCultural`
  ADD CONSTRAINT `FK_LaborCultural_Cuadro` FOREIGN KEY (`idCuadro`) REFERENCES `Cuadro` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Localidad`
--
ALTER TABLE `Localidad`
  ADD CONSTRAINT `fk_Localidad_Departamento` FOREIGN KEY (`IdDepartamento`) REFERENCES `Departamento` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Productor`
--
ALTER TABLE `Productor`
  ADD CONSTRAINT `fk_Productor_Localidad` FOREIGN KEY (`IdLocalidad`) REFERENCES `Localidad` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Profesional`
--
ALTER TABLE `Profesional`
  ADD CONSTRAINT `fk_Tecnico_Localidad` FOREIGN KEY (`IdLocalidad`) REFERENCES `Localidad` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Usuario`
--
ALTER TABLE `Usuario`
  ADD CONSTRAINT `Usuario_ibfk_1` FOREIGN KEY (`idRol`) REFERENCES `Rol` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
