-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-07-2014 a las 00:48:16
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `libretto`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `AplicacionTratamiento`
--

CREATE TABLE IF NOT EXISTS `AplicacionTratamiento` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCuadro` int(11) NOT NULL,
  `IdTratamientoFitosanitario` int(11) NOT NULL,
  `FechaHoraInicio` datetime NOT NULL,
  `FechaHoraFin` datetime NOT NULL,
  `LitrosCuadro` int(11) NOT NULL,
  `FechaCumplimientoTC` date DEFAULT NULL,
  `Clima` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdCuadro` (`IdCuadro`),
  KEY `IdTratamientoFitosanitario` (`IdTratamientoFitosanitario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `CapturaCarpocapsa`
--

CREATE TABLE IF NOT EXISTS `CapturaCarpocapsa` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdTrampa` int(11) NOT NULL,
  `Fecha` date NOT NULL,
  `Detalle` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdTrampa` (`IdTrampa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Cuaderno`
--

CREATE TABLE IF NOT EXISTS `Cuaderno` (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Numero de Cuaderno',
  `IdExplotacion` int(11) NOT NULL,
  `NumeroRenspa` varchar(100) NOT NULL,
  `NumeroCuaderno` int(11) NOT NULL DEFAULT '0',
  `Temporada` varchar(45) NOT NULL,
  `NombreChacra` varchar(100) NOT NULL,
  `NumeroInterno` varchar(100) DEFAULT NULL,
  `SuperficieTotal` decimal(10,2) DEFAULT NULL,
  `SuperficieNeta` decimal(10,2) DEFAULT NULL,
  `IdProductor` int(11) NOT NULL,
  `ProductorNombreApellido` varchar(255) NOT NULL,
  `ProductorEmail` varchar(255) DEFAULT NULL,
  `ProductorDireccion` varchar(255) DEFAULT NULL,
  `ProductorIdLocalidad` int(11) NOT NULL,
  `ProductorTelefono` varchar(45) DEFAULT NULL,
  `IdProfesional` int(11) DEFAULT NULL,
  `ProfesionalNombreApellido` varchar(255) DEFAULT NULL,
  `ProfesionalMatricula` varchar(255) DEFAULT NULL,
  `ProfesionalEmail` varchar(255) DEFAULT NULL,
  `ProfesionalDireccion` varchar(255) DEFAULT NULL,
  `ProfesionalIdLocalidad` int(11) DEFAULT NULL,
  `ProfesionalTelefono` varchar(45) DEFAULT NULL,
  `IdRecorredor` int(11) DEFAULT NULL,
  `RecorredorNombreApellido` varchar(255) DEFAULT NULL,
  `RecorredorTelefono` varchar(45) DEFAULT NULL,
  `UbicacionUmis` varchar(255) DEFAULT NULL,
  `UbicacionHuerto` varchar(255) DEFAULT NULL,
  `Estado` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdTecnico` (`IdProfesional`),
  KEY `IdExplotacion` (`IdExplotacion`),
  KEY `ProductorIdLocalidad` (`ProductorIdLocalidad`),
  KEY `ProfesionalIdLocalidad` (`ProfesionalIdLocalidad`),
  KEY `IdRecorredor` (`IdRecorredor`),
  KEY `IdProductor` (`IdProductor`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `Cuaderno`
--

INSERT INTO `Cuaderno` (`Id`, `IdExplotacion`, `NumeroRenspa`, `NumeroCuaderno`, `Temporada`, `NombreChacra`, `NumeroInterno`, `SuperficieTotal`, `SuperficieNeta`, `IdProductor`, `ProductorNombreApellido`, `ProductorEmail`, `ProductorDireccion`, `ProductorIdLocalidad`, `ProductorTelefono`, `IdProfesional`, `ProfesionalNombreApellido`, `ProfesionalMatricula`, `ProfesionalEmail`, `ProfesionalDireccion`, `ProfesionalIdLocalidad`, `ProfesionalTelefono`, `IdRecorredor`, `RecorredorNombreApellido`, `RecorredorTelefono`, `UbicacionUmis`, `UbicacionHuerto`, `Estado`) VALUES
(1, 6, '123456', 0, '2014', 'Messias', '1', '123.00', '123.00', 6, 'messi', NULL, NULL, 1, NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, '', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Cuadro`
--

CREATE TABLE IF NOT EXISTS `Cuadro` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdUmi` int(11) NOT NULL,
  `NumeroCuadro` int(11) NOT NULL,
  `Altura` decimal(10,2) NOT NULL,
  `Ancho` decimal(10,2) NOT NULL,
  `DistanciaFilas` decimal(10,2) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Cuadro_Umi` (`IdUmi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Destino`
--

CREATE TABLE IF NOT EXISTS `Destino` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Destino` varchar(255) NOT NULL,
  `IdCuaderno` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdCuaderno` (`IdCuaderno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Destinos de la Fruta' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Equipo`
--

CREATE TABLE IF NOT EXISTS `Equipo` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCuaderno` int(11) NOT NULL,
  `NumeroEquipo` int(11) NOT NULL,
  `TractorMarca` varchar(255) DEFAULT NULL,
  `TractorModelo` varchar(255) DEFAULT NULL,
  `TractorAnio` int(11) NOT NULL,
  `TractorPotencia` int(11) DEFAULT NULL,
  `PulverizadoraMarca` varchar(255) DEFAULT NULL,
  `PulverizadoraModelo` varchar(255) DEFAULT NULL,
  `PulverizadoraAnio` varchar(255) NOT NULL,
  `PulverizadoraVolumen` int(11) NOT NULL,
  `IdTractorista` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdCuaderno` (`IdCuaderno`),
  KEY `IdTractorista` (`IdTractorista`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Especie`
--

CREATE TABLE IF NOT EXISTS `Especie` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(255) NOT NULL,
  `Codigo` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Explotacion`
--

CREATE TABLE IF NOT EXISTS `Explotacion` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdProductor` int(11) NOT NULL,
  `NombreChacra` varchar(255) NOT NULL,
  `IdLocalidad` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdCuaderno` (`IdProductor`),
  KEY `FK_Explotacion_Localidad` (`IdLocalidad`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `Explotacion`
--

INSERT INTO `Explotacion` (`Id`, `IdProductor`, `NombreChacra`, `IdLocalidad`) VALUES
(2, 5, 'UnaExplotacion', 1),
(3, 5, 'OtraExplotacion', 3),
(6, 6, 'Messias', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `FormularioA`
--

CREATE TABLE IF NOT EXISTS `FormularioA` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCuadro` int(11) NOT NULL,
  `Superficie` decimal(10,2) NOT NULL,
  `IdEspecie` int(11) NOT NULL,
  `TipoTCS` varchar(255) DEFAULT NULL,
  `DispNroPorCuadroDosis1` decimal(10,2) DEFAULT NULL,
  `DispNroPorHectareaDosis1` decimal(10,2) DEFAULT NULL,
  `DispNroPorCuadroDosis2` decimal(10,2) DEFAULT NULL,
  `DispNroPorHectareaDosis2` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdCuadro` (`IdCuadro`),
  KEY `IdEspecie` (`IdEspecie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Inspector`
--

CREATE TABLE IF NOT EXISTS `Inspector` (
  `Id` int(11) NOT NULL,
  `Legajo` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `LaborCultural`
--

CREATE TABLE IF NOT EXISTS `LaborCultural` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCuadro` int(11) NOT NULL,
  `IdTipoLabor` int(11) NOT NULL,
  `Detalle` varchar(1000) NOT NULL,
  `Fecha` date NOT NULL,
  `Horas` int(11) NOT NULL,
  `IdEquipo` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_LaborCultural_Cuadro` (`IdCuadro`),
  KEY `IdTipoLabor` (`IdTipoLabor`),
  KEY `IdEquipo` (`IdEquipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Localidad`
--

CREATE TABLE IF NOT EXISTS `Localidad` (
  `Id` int(11) NOT NULL,
  `Nombre` varchar(200) NOT NULL,
  `IdProvincia` int(11) NOT NULL,
  `CodigoPostal` varchar(45) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdDepartamento` (`IdProvincia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Localidad`
--

INSERT INTO `Localidad` (`Id`, `Nombre`, `IdProvincia`, `CodigoPostal`) VALUES
(1, 'Neuquén', 1, '8300'),
(2, 'Centenario', 1, ''),
(3, 'Cinco Saltos', 2, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Log`
--

CREATE TABLE IF NOT EXISTS `Log` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Detalle` varchar(1000) NOT NULL,
  `IdUsuario` int(11) NOT NULL,
  `Fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  KEY `IdUsuario` (`IdUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `MuestreoDanioCarpocapsa`
--

CREATE TABLE IF NOT EXISTS `MuestreoDanioCarpocapsa` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCuadro` int(11) NOT NULL,
  `IdVariedad` int(11) NOT NULL,
  `Superficie` decimal(10,2) NOT NULL COMMENT 'Dato Calculado sobre plantaciones',
  `Fecha` date DEFAULT NULL,
  `Muestra` int(11) DEFAULT NULL,
  `FrutosDaniados` int(11) DEFAULT NULL,
  `PorcentajeDanio` decimal(10,2) DEFAULT NULL,
  `FechaCosecha` date DEFAULT NULL,
  `MuestraCosecha` int(11) DEFAULT NULL,
  `FrutosDaniadosCosecha` int(11) DEFAULT NULL,
  `PorcentajeDanioCosecha` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdVariedad` (`IdVariedad`),
  KEY `IdCuadro` (`IdCuadro`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Plaga`
--

CREATE TABLE IF NOT EXISTS `Plaga` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(255) NOT NULL COMMENT 'Nombre de la plaga, predador o enfermedad',
  `Especie` varchar(255) NOT NULL COMMENT 'Tipo: Plaga, Predador o Enfermedad',
  `Genero` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Plantacion`
--

CREATE TABLE IF NOT EXISTS `Plantacion` (
  `Id` int(11) NOT NULL,
  `IdCuadro` int(11) NOT NULL,
  `IdTipoProduccion` int(11) NOT NULL,
  `IdVariedad` int(11) NOT NULL COMMENT 'Se debe desplegar desde la elección de la Especialidad',
  `IdPie` int(11) NOT NULL,
  `IdCond` int(11) NOT NULL,
  `Anio` int(11) NOT NULL,
  `DFilas` decimal(10,2) NOT NULL,
  `DPlantas` decimal(10,2) NOT NULL,
  `NumeroPlantas` int(11) NOT NULL,
  `SuperficieNeta` decimal(10,2) NOT NULL,
  `Catastro` varchar(45) NOT NULL,
  `Latitud` double DEFAULT NULL,
  `Longitud` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdCuadro` (`IdCuadro`),
  KEY `IdTipoProduccion` (`IdTipoProduccion`),
  KEY `IdVariedad` (`IdVariedad`),
  KEY `IdPie` (`IdPie`),
  KEY `IdCond` (`IdCond`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `PortaInjertos`
--

CREATE TABLE IF NOT EXISTS `PortaInjertos` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdEspecie` int(11) NOT NULL,
  `Nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdEspecie` (`IdEspecie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Productor`
--

CREATE TABLE IF NOT EXISTS `Productor` (
  `Id` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Productor`
--

INSERT INTO `Productor` (`Id`) VALUES
(2),
(5),
(6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Profesional`
--

CREATE TABLE IF NOT EXISTS `Profesional` (
  `Id` int(11) NOT NULL,
  `Matricula` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Profesional`
--

INSERT INTO `Profesional` (`Id`, `Matricula`) VALUES
(7, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Provincia`
--

CREATE TABLE IF NOT EXISTS `Provincia` (
  `Id` int(11) NOT NULL,
  `Nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Provincia`
--

INSERT INTO `Provincia` (`Id`, `Nombre`) VALUES
(1, 'Neuquén'),
(2, 'Rio Negro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `RegistrosFenologicos`
--

CREATE TABLE IF NOT EXISTS `RegistrosFenologicos` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCuaderno` int(11) NOT NULL,
  `IdVariedad` int(11) NOT NULL,
  `InicioFloracion` date DEFAULT NULL,
  `PlenaFloracion` date DEFAULT NULL,
  `CaidaPetalos` date DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdCuaderno` (`IdCuaderno`),
  KEY `IdVariedad` (`IdVariedad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Rol`
--

CREATE TABLE IF NOT EXISTS `Rol` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Rol` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Rol`
--

INSERT INTO `Rol` (`Id`, `Rol`) VALUES
(1, 'administrador'),
(2, 'inspector'),
(3, 'productor'),
(4, 'profesional');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `SalidaFruta`
--

CREATE TABLE IF NOT EXISTS `SalidaFruta` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCuaderno` int(11) NOT NULL,
  `IdUmi` int(11) NOT NULL,
  `Fecha` date NOT NULL,
  `IdVariedad` int(11) NOT NULL,
  `CantidadBins` int(11) NOT NULL,
  `NumeroRemito` varchar(255) DEFAULT NULL,
  `IdDestino` int(11) NOT NULL,
  `Observaciones` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdCuaderno` (`IdCuaderno`),
  KEY `IdUmi` (`IdUmi`),
  KEY `IdVariedad` (`IdVariedad`),
  KEY `IdDestino` (`IdDestino`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `SistemaConduccion`
--

CREATE TABLE IF NOT EXISTS `SistemaConduccion` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(255) NOT NULL,
  `Codigo` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `TipoLabor`
--

CREATE TABLE IF NOT EXISTS `TipoLabor` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `TipoProduccion`
--

CREATE TABLE IF NOT EXISTS `TipoProduccion` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Tractorista`
--

CREATE TABLE IF NOT EXISTS `Tractorista` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCuaderno` int(11) NOT NULL,
  `NombreApellido` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdUsuario` (`IdCuaderno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Trampa`
--

CREATE TABLE IF NOT EXISTS `Trampa` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCuadro` int(11) NOT NULL,
  `Numero` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdCuadro` (`IdCuadro`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Tratamiento`
--

CREATE TABLE IF NOT EXISTS `Tratamiento` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `NombreComercial` varchar(255) NOT NULL,
  `PrincipioActivo` varchar(255) NOT NULL,
  `Dosis` decimal(10,2) NOT NULL,
  `IdUnidad` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `idUnidad` (`IdUnidad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `TratamientoFitosanitario`
--

CREATE TABLE IF NOT EXISTS `TratamientoFitosanitario` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdOrden` int(11) NOT NULL,
  `IdEquipo` int(11) NOT NULL,
  `IdTratamiento` int(11) NOT NULL,
  `DosisMaquina` decimal(10,2) NOT NULL,
  `IdUnidadDosis` int(11) NOT NULL,
  `TiempoCarencia` int(11) DEFAULT NULL,
  `TiempoReingreso` int(11) DEFAULT NULL,
  `IdAplicador` int(11) NOT NULL,
  `IdMotivo` int(11) NOT NULL,
  `IdVariedad` int(11) NOT NULL,
  `Observaciones` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdVariedad` (`IdVariedad`),
  KEY `IdMotivo` (`IdMotivo`),
  KEY `IdTratamiento` (`IdTratamiento`),
  KEY `IdTratamiento_2` (`IdTratamiento`),
  KEY `IdUnidadDosis` (`IdUnidadDosis`),
  KEY `IdEquipo` (`IdEquipo`),
  KEY `IdAplicador` (`IdAplicador`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Umi`
--

CREATE TABLE IF NOT EXISTS `Umi` (
  `Id` int(11) NOT NULL,
  `IdCuaderno` int(11) NOT NULL,
  `Codigo` varchar(45) DEFAULT NULL,
  `Descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdExplotacion` (`IdCuaderno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `UnidadDosis`
--

CREATE TABLE IF NOT EXISTS `UnidadDosis` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Unidad` varchar(20) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Usuario`
--

CREATE TABLE IF NOT EXISTS `Usuario` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Usuario` varchar(255) NOT NULL,
  `Clave` varchar(32) NOT NULL,
  `IdRol` int(11) NOT NULL,
  `NombreApellido` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Direccion` varchar(255) DEFAULT NULL,
  `IdLocalidad` int(11) DEFAULT NULL,
  `Telefono` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Usuario` (`Usuario`),
  KEY `idRol` (`IdRol`),
  KEY `IdLocalidad` (`IdLocalidad`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `Usuario`
--

INSERT INTO `Usuario` (`Id`, `Usuario`, `Clave`, `IdRol`, `NombreApellido`, `Email`, `Direccion`, `IdLocalidad`, `Telefono`) VALUES
(1, 'admin', 'admin', 1, 'AdminAdministra', 'admin@administra', NULL, NULL, NULL),
(2, 'usuario', 'usuario', 2, 'UsuarioUsa', 'usuario@usa', NULL, NULL, NULL),
(3, 'Pepito', 'pepito', 3, 'Pepito Perez', 'pepito@', 'Calle falsa 123', 1, '45645646'),
(4, 'Pepe2', 'pepe', 3, 'pepe2', 'pepe2@asaf', 'asdasd 123', 2, '454545'),
(5, 'pp', 'pp', 3, 'pp', 'pp', 'pp', 1, '234'),
(6, 'messi', 'messi', 3, 'messi', 'messi@asd', 'messi', 1, '123123'),
(7, 'sabella', 'sabella', 4, 'sabella', 'sabella@', 'sabella', 1, '123123');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Variedad`
--

CREATE TABLE IF NOT EXISTS `Variedad` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(255) NOT NULL,
  `Codigo` varchar(50) NOT NULL,
  `IdEspecie` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdEspecie` (`IdEspecie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `VisitaInspeccion`
--

CREATE TABLE IF NOT EXISTS `VisitaInspeccion` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCuaderno` int(11) NOT NULL,
  `IdInspector` int(11) NOT NULL,
  `NombreApellido` varchar(255) NOT NULL,
  `Legajo` varchar(255) NOT NULL,
  `Fecha` date DEFAULT NULL,
  `Observaciones` text,
  PRIMARY KEY (`Id`),
  KEY `IdCuaderno` (`IdCuaderno`),
  KEY `IdInspector` (`IdInspector`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `AplicacionTratamiento`
--
ALTER TABLE `AplicacionTratamiento`
  ADD CONSTRAINT `AplicacionTratamiento_ibfk_1` FOREIGN KEY (`IdCuadro`) REFERENCES `Cuadro` (`Id`),
  ADD CONSTRAINT `AplicacionTratamiento_ibfk_2` FOREIGN KEY (`IdTratamientoFitosanitario`) REFERENCES `TratamientoFitosanitario` (`Id`);

--
-- Filtros para la tabla `CapturaCarpocapsa`
--
ALTER TABLE `CapturaCarpocapsa`
  ADD CONSTRAINT `CapturaCarpocapsa_ibfk_1` FOREIGN KEY (`IdTrampa`) REFERENCES `Trampa` (`Id`);

--
-- Filtros para la tabla `Cuaderno`
--
ALTER TABLE `Cuaderno`
  ADD CONSTRAINT `Cuaderno_ibfk_1` FOREIGN KEY (`IdExplotacion`) REFERENCES `Explotacion` (`Id`);

--
-- Filtros para la tabla `Cuadro`
--
ALTER TABLE `Cuadro`
  ADD CONSTRAINT `FK_Cuadro_Umi` FOREIGN KEY (`IdUmi`) REFERENCES `Umi` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Destino`
--
ALTER TABLE `Destino`
  ADD CONSTRAINT `Destino_ibfk_1` FOREIGN KEY (`IdCuaderno`) REFERENCES `Cuaderno` (`Id`);

--
-- Filtros para la tabla `Equipo`
--
ALTER TABLE `Equipo`
  ADD CONSTRAINT `Equipo_ibfk_1` FOREIGN KEY (`IdCuaderno`) REFERENCES `Cuaderno` (`Id`),
  ADD CONSTRAINT `Equipo_ibfk_2` FOREIGN KEY (`IdTractorista`) REFERENCES `Tractorista` (`Id`);

--
-- Filtros para la tabla `Explotacion`
--
ALTER TABLE `Explotacion`
  ADD CONSTRAINT `FK_Explotacion_Localidad` FOREIGN KEY (`IdLocalidad`) REFERENCES `Localidad` (`Id`),
  ADD CONSTRAINT `Explotacion_ibfk_1` FOREIGN KEY (`IdProductor`) REFERENCES `Productor` (`Id`);

--
-- Filtros para la tabla `FormularioA`
--
ALTER TABLE `FormularioA`
  ADD CONSTRAINT `FormularioA_ibfk_1` FOREIGN KEY (`IdEspecie`) REFERENCES `Especie` (`Id`),
  ADD CONSTRAINT `FormularioA_ibfk_2` FOREIGN KEY (`IdCuadro`) REFERENCES `AplicacionTratamiento` (`IdCuadro`);

--
-- Filtros para la tabla `Inspector`
--
ALTER TABLE `Inspector`
  ADD CONSTRAINT `Inspector_ibfk_1` FOREIGN KEY (`Id`) REFERENCES `Usuario` (`id`);

--
-- Filtros para la tabla `LaborCultural`
--
ALTER TABLE `LaborCultural`
  ADD CONSTRAINT `FK_LaborCultural_Cuadro` FOREIGN KEY (`IdCuadro`) REFERENCES `Cuadro` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `LaborCultural_ibfk_1` FOREIGN KEY (`IdTipoLabor`) REFERENCES `TipoLabor` (`Id`),
  ADD CONSTRAINT `LaborCultural_ibfk_2` FOREIGN KEY (`IdCuadro`) REFERENCES `Cuadro` (`Id`);

--
-- Filtros para la tabla `Localidad`
--
ALTER TABLE `Localidad`
  ADD CONSTRAINT `Localidad_ibfk_1` FOREIGN KEY (`IdProvincia`) REFERENCES `Provincia` (`Id`);

--
-- Filtros para la tabla `MuestreoDanioCarpocapsa`
--
ALTER TABLE `MuestreoDanioCarpocapsa`
  ADD CONSTRAINT `MuestreoDanioCarpocapsa_ibfk_1` FOREIGN KEY (`IdVariedad`) REFERENCES `Variedad` (`Id`),
  ADD CONSTRAINT `MuestreoDanioCarpocapsa_ibfk_2` FOREIGN KEY (`IdCuadro`) REFERENCES `Cuadro` (`Id`);

--
-- Filtros para la tabla `Plantacion`
--
ALTER TABLE `Plantacion`
  ADD CONSTRAINT `Plantacion_ibfk_1` FOREIGN KEY (`IdCuadro`) REFERENCES `Cuadro` (`Id`),
  ADD CONSTRAINT `Plantacion_ibfk_3` FOREIGN KEY (`IdVariedad`) REFERENCES `Variedad` (`Id`),
  ADD CONSTRAINT `Plantacion_ibfk_4` FOREIGN KEY (`IdPie`) REFERENCES `PortaInjertos` (`Id`),
  ADD CONSTRAINT `Plantacion_ibfk_5` FOREIGN KEY (`IdCond`) REFERENCES `SistemaConduccion` (`Id`),
  ADD CONSTRAINT `Plantacion_ibfk_6` FOREIGN KEY (`IdTipoProduccion`) REFERENCES `TipoProduccion` (`Id`);

--
-- Filtros para la tabla `PortaInjertos`
--
ALTER TABLE `PortaInjertos`
  ADD CONSTRAINT `PortaInjertos_ibfk_1` FOREIGN KEY (`IdEspecie`) REFERENCES `Especie` (`Id`);

--
-- Filtros para la tabla `Productor`
--
ALTER TABLE `Productor`
  ADD CONSTRAINT `Productor_ibfk_1` FOREIGN KEY (`Id`) REFERENCES `Usuario` (`id`);

--
-- Filtros para la tabla `Profesional`
--
ALTER TABLE `Profesional`
  ADD CONSTRAINT `Profesional_ibfk_1` FOREIGN KEY (`Id`) REFERENCES `Usuario` (`id`);

--
-- Filtros para la tabla `RegistrosFenologicos`
--
ALTER TABLE `RegistrosFenologicos`
  ADD CONSTRAINT `RegistrosFenologicos_ibfk_1` FOREIGN KEY (`IdCuaderno`) REFERENCES `Cuadro` (`Id`),
  ADD CONSTRAINT `RegistrosFenologicos_ibfk_3` FOREIGN KEY (`IdVariedad`) REFERENCES `Variedad` (`Id`);

--
-- Filtros para la tabla `SalidaFruta`
--
ALTER TABLE `SalidaFruta`
  ADD CONSTRAINT `SalidaFruta_ibfk_1` FOREIGN KEY (`IdCuaderno`) REFERENCES `Cuaderno` (`Id`),
  ADD CONSTRAINT `SalidaFruta_ibfk_2` FOREIGN KEY (`IdUmi`) REFERENCES `Umi` (`Id`),
  ADD CONSTRAINT `SalidaFruta_ibfk_4` FOREIGN KEY (`IdVariedad`) REFERENCES `Variedad` (`Id`),
  ADD CONSTRAINT `SalidaFruta_ibfk_5` FOREIGN KEY (`IdDestino`) REFERENCES `Destino` (`Id`);

--
-- Filtros para la tabla `Tractorista`
--
ALTER TABLE `Tractorista`
  ADD CONSTRAINT `Tractorista_ibfk_1` FOREIGN KEY (`IdCuaderno`) REFERENCES `Cuaderno` (`Id`);

--
-- Filtros para la tabla `Trampa`
--
ALTER TABLE `Trampa`
  ADD CONSTRAINT `Trampa_ibfk_1` FOREIGN KEY (`IdCuadro`) REFERENCES `Cuadro` (`Id`);

--
-- Filtros para la tabla `Tratamiento`
--
ALTER TABLE `Tratamiento`
  ADD CONSTRAINT `Tratamiento_ibfk_1` FOREIGN KEY (`IdUnidad`) REFERENCES `UnidadDosis` (`Id`);

--
-- Filtros para la tabla `TratamientoFitosanitario`
--
ALTER TABLE `TratamientoFitosanitario`
  ADD CONSTRAINT `TratamientoFitosanitario_ibfk_1` FOREIGN KEY (`IdMotivo`) REFERENCES `Plaga` (`Id`),
  ADD CONSTRAINT `TratamientoFitosanitario_ibfk_2` FOREIGN KEY (`IdVariedad`) REFERENCES `Variedad` (`Id`),
  ADD CONSTRAINT `TratamientoFitosanitario_ibfk_3` FOREIGN KEY (`IdTratamiento`) REFERENCES `Tratamiento` (`Id`),
  ADD CONSTRAINT `TratamientoFitosanitario_ibfk_4` FOREIGN KEY (`IdUnidadDosis`) REFERENCES `UnidadDosis` (`Id`),
  ADD CONSTRAINT `TratamientoFitosanitario_ibfk_6` FOREIGN KEY (`IdEquipo`) REFERENCES `Equipo` (`Id`),
  ADD CONSTRAINT `TratamientoFitosanitario_ibfk_7` FOREIGN KEY (`IdAplicador`) REFERENCES `Tractorista` (`Id`);

--
-- Filtros para la tabla `Umi`
--
ALTER TABLE `Umi`
  ADD CONSTRAINT `Umi_ibfk_1` FOREIGN KEY (`IdCuaderno`) REFERENCES `Cuaderno` (`Id`);

--
-- Filtros para la tabla `Usuario`
--
ALTER TABLE `Usuario`
  ADD CONSTRAINT `Usuario_ibfk_1` FOREIGN KEY (`idRol`) REFERENCES `Rol` (`id`),
  ADD CONSTRAINT `Usuario_ibfk_2` FOREIGN KEY (`IdLocalidad`) REFERENCES `Localidad` (`Id`);

--
-- Filtros para la tabla `Variedad`
--
ALTER TABLE `Variedad`
  ADD CONSTRAINT `Variedad_ibfk_1` FOREIGN KEY (`IdEspecie`) REFERENCES `Especie` (`Id`);

--
-- Filtros para la tabla `VisitaInspeccion`
--
ALTER TABLE `VisitaInspeccion`
  ADD CONSTRAINT `VisitaInspeccion_ibfk_1` FOREIGN KEY (`IdCuaderno`) REFERENCES `Cuaderno` (`Id`),
  ADD CONSTRAINT `VisitaInspeccion_ibfk_2` FOREIGN KEY (`IdInspector`) REFERENCES `Inspector` (`Id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
