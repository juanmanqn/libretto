-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-08-2014 a las 03:55:32
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `libretto`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `AplicacionTratamiento`
--

CREATE TABLE IF NOT EXISTS `AplicacionTratamiento` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCuadro` int(11) NOT NULL,
  `IdTratamientoFitosanitario` int(11) NOT NULL,
  `FechaHoraInicio` datetime NOT NULL,
  `FechaHoraFin` datetime NOT NULL,
  `LitrosCuadro` int(11) NOT NULL,
  `FechaCumplimientoTC` date DEFAULT NULL,
  `Clima` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdCuadro` (`IdCuadro`),
  KEY `IdTratamientoFitosanitario` (`IdTratamientoFitosanitario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `AplicacionTratamiento`
--

INSERT INTO `AplicacionTratamiento` (`Id`, `IdCuadro`, `IdTratamientoFitosanitario`, `FechaHoraInicio`, `FechaHoraFin`, `LitrosCuadro`, `FechaCumplimientoTC`, `Clima`) VALUES
(2, 4, 1, '2014-08-02 00:00:00', '2014-08-03 00:00:00', 4, '2014-08-03', 'asd'),
(3, 3, 1, '2014-08-02 00:00:00', '2014-08-02 00:00:00', 7, '2014-08-03', 'asd');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `CapturaCarpocapsa`
--

CREATE TABLE IF NOT EXISTS `CapturaCarpocapsa` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdTrampa` int(11) NOT NULL,
  `Fecha` date NOT NULL,
  `Detalle` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdTrampa` (`IdTrampa`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `CapturaCarpocapsa`
--

INSERT INTO `CapturaCarpocapsa` (`Id`, `IdTrampa`, `Fecha`, `Detalle`) VALUES
(1, 1, '2014-08-02', 'asd');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Cuaderno`
--

CREATE TABLE IF NOT EXISTS `Cuaderno` (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Numero de Cuaderno',
  `IdExplotacion` int(11) NOT NULL,
  `NumeroRenspa` varchar(100) NOT NULL,
  `NumeroCuaderno` int(11) NOT NULL DEFAULT '0',
  `Temporada` varchar(45) NOT NULL,
  `NombreChacra` varchar(100) NOT NULL,
  `NumeroInterno` varchar(100) DEFAULT NULL,
  `SuperficieTotal` decimal(10,2) DEFAULT NULL,
  `SuperficieNeta` decimal(10,2) DEFAULT NULL,
  `IdProductor` int(11) NOT NULL,
  `ProductorNombreApellido` varchar(255) NOT NULL,
  `ProductorEmail` varchar(255) DEFAULT NULL,
  `ProductorDireccion` varchar(255) DEFAULT NULL,
  `ProductorIdLocalidad` int(11) NOT NULL,
  `ProductorTelefono` varchar(45) DEFAULT NULL,
  `IdProfesional` int(11) DEFAULT NULL,
  `ProfesionalNombreApellido` varchar(255) DEFAULT NULL,
  `ProfesionalMatricula` varchar(255) DEFAULT NULL,
  `ProfesionalEmail` varchar(255) DEFAULT NULL,
  `ProfesionalDireccion` varchar(255) DEFAULT NULL,
  `ProfesionalIdLocalidad` int(11) DEFAULT NULL,
  `ProfesionalTelefono` varchar(45) DEFAULT NULL,
  `IdRecorredor` int(11) DEFAULT NULL,
  `RecorredorNombreApellido` varchar(255) DEFAULT NULL,
  `RecorredorTelefono` varchar(45) DEFAULT NULL,
  `UbicacionUmis` varchar(255) DEFAULT NULL,
  `UbicacionHuerto` varchar(255) DEFAULT NULL,
  `Estado` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdTecnico` (`IdProfesional`),
  KEY `IdExplotacion` (`IdExplotacion`),
  KEY `ProductorIdLocalidad` (`ProductorIdLocalidad`),
  KEY `ProfesionalIdLocalidad` (`ProfesionalIdLocalidad`),
  KEY `IdRecorredor` (`IdRecorredor`),
  KEY `IdProductor` (`IdProductor`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `Cuaderno`
--

INSERT INTO `Cuaderno` (`Id`, `IdExplotacion`, `NumeroRenspa`, `NumeroCuaderno`, `Temporada`, `NombreChacra`, `NumeroInterno`, `SuperficieTotal`, `SuperficieNeta`, `IdProductor`, `ProductorNombreApellido`, `ProductorEmail`, `ProductorDireccion`, `ProductorIdLocalidad`, `ProductorTelefono`, `IdProfesional`, `ProfesionalNombreApellido`, `ProfesionalMatricula`, `ProfesionalEmail`, `ProfesionalDireccion`, `ProfesionalIdLocalidad`, `ProfesionalTelefono`, `IdRecorredor`, `RecorredorNombreApellido`, `RecorredorTelefono`, `UbicacionUmis`, `UbicacionHuerto`, `Estado`) VALUES
(1, 6, '123456', 0, '2014', 'Messias', '1', '123.00', '123.00', 6, 'messi', NULL, NULL, 1, NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, '123456UbicacionUmis2014080708-114846.pdf', '', 1),
(2, 6, '123123', 2, '2015', 'Messias2', '11', '1.00', '1.00', 6, 'messi', NULL, NULL, 1, NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, '', '', 1),
(4, 7, '456456', 555, '2015', 'Otra Chacra', '1', '55.00', '44.00', 6, 'messi', NULL, NULL, 1, NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, '', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Cuadro`
--

CREATE TABLE IF NOT EXISTS `Cuadro` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdUmi` int(11) NOT NULL,
  `NumeroCuadro` varchar(15) NOT NULL,
  `Altura` decimal(10,2) NOT NULL,
  `Ancho` decimal(10,2) NOT NULL,
  `DistanciaFilas` decimal(10,2) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Cuadro_Umi` (`IdUmi`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `Cuadro`
--

INSERT INTO `Cuadro` (`Id`, `IdUmi`, `NumeroCuadro`, `Altura`, `Ancho`, `DistanciaFilas`) VALUES
(2, 3, '2', '0.00', '0.00', '0.00'),
(3, 1, '3', '0.00', '0.00', '0.00'),
(4, 2, '4', '0.00', '0.00', '0.00'),
(5, 2, '5', '0.00', '0.00', '0.00'),
(6, 3, '6', '0.00', '0.00', '0.00'),
(7, 1, '4', '5.00', '5.00', '5.00'),
(8, 1, '5', '2.00', '2.00', '2.00'),
(10, 1, '11', '4.00', '4.00', '4.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Destino`
--

CREATE TABLE IF NOT EXISTS `Destino` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Destino` varchar(255) NOT NULL,
  `IdCuaderno` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdCuaderno` (`IdCuaderno`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Destinos de la Fruta' AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `Destino`
--

INSERT INTO `Destino` (`Id`, `Destino`, `IdCuaderno`) VALUES
(1, 'destino1', 1),
(2, 'destino2', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Equipo`
--

CREATE TABLE IF NOT EXISTS `Equipo` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCuaderno` int(11) NOT NULL,
  `NumeroEquipo` int(11) NOT NULL,
  `TractorMarca` varchar(255) DEFAULT NULL,
  `TractorModelo` varchar(255) DEFAULT NULL,
  `TractorAnio` int(11) NOT NULL,
  `TractorPotencia` int(11) DEFAULT NULL,
  `PulverizadoraMarca` varchar(255) DEFAULT NULL,
  `PulverizadoraModelo` varchar(255) DEFAULT NULL,
  `PulverizadoraAnio` varchar(255) NOT NULL,
  `PulverizadoraVolumen` int(11) NOT NULL,
  `IdTractorista` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdCuaderno` (`IdCuaderno`),
  KEY `IdTractorista` (`IdTractorista`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `Equipo`
--

INSERT INTO `Equipo` (`Id`, `IdCuaderno`, `NumeroEquipo`, `TractorMarca`, `TractorModelo`, `TractorAnio`, `TractorPotencia`, `PulverizadoraMarca`, `PulverizadoraModelo`, `PulverizadoraAnio`, `PulverizadoraVolumen`, `IdTractorista`) VALUES
(1, 1, 1, 'mercedez', '123', 1980, 500, 'pulve', '321', '1989', 333, 1),
(2, 1, 2, 'trac', '234', 1990, 243, 'pulve 2', '456', '2000', 34, 1),
(3, 2, 3, 'asdasd', 'asdasdad', 123, 3242, 'ffff', 'fff3', '124', 34, 2),
(5, 4, 1, 'asd', 'asd', 123, 324, 'asd', 'asd', '1444', 123, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Especie`
--

CREATE TABLE IF NOT EXISTS `Especie` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(255) NOT NULL,
  `Codigo` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `Especie`
--

INSERT INTO `Especie` (`Id`, `Nombre`, `Codigo`) VALUES
(1, 'MANZANA', 'MZ'),
(2, 'PERA', 'PE'),
(3, 'DURAZNO', 'DUZ'),
(4, 'NECTARINAS', 'NEC'),
(5, 'CIRUELA', 'CIR'),
(6, 'CEREZA', 'CERZ'),
(7, 'VID VINIFICAR', 'VID'),
(8, 'NOGAL', 'NOG'),
(9, 'VID MESA', 'VM'),
(10, 'ALAMOS', 'ALM'),
(11, 'PASTURAS', 'PAS'),
(12, 'SIN CULTIVOS', 'SICU');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Explotacion`
--

CREATE TABLE IF NOT EXISTS `Explotacion` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdProductor` int(11) NOT NULL,
  `NombreChacra` varchar(255) NOT NULL,
  `IdLocalidad` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdCuaderno` (`IdProductor`),
  KEY `FK_Explotacion_Localidad` (`IdLocalidad`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `Explotacion`
--

INSERT INTO `Explotacion` (`Id`, `IdProductor`, `NombreChacra`, `IdLocalidad`) VALUES
(2, 5, 'UnaExplotacion', 1),
(3, 5, 'OtraExplotacion', 3),
(6, 6, 'Messias', 1),
(7, 6, 'Otra Chacra', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `FormularioA`
--

CREATE TABLE IF NOT EXISTS `FormularioA` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCuadro` int(11) NOT NULL,
  `Superficie` decimal(10,2) NOT NULL,
  `IdEspecie` int(11) NOT NULL,
  `TipoTCS` varchar(255) DEFAULT NULL,
  `DispNroPorCuadroDosis1` decimal(10,2) DEFAULT NULL,
  `DispNroPorHectareaDosis1` decimal(10,2) DEFAULT NULL,
  `DispNroPorCuadroDosis2` decimal(10,2) DEFAULT NULL,
  `DispNroPorHectareaDosis2` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdCuadro` (`IdCuadro`),
  KEY `IdEspecie` (`IdEspecie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Inspector`
--

CREATE TABLE IF NOT EXISTS `Inspector` (
  `Id` int(11) NOT NULL,
  `Legajo` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Inspector`
--

INSERT INTO `Inspector` (`Id`, `Legajo`) VALUES
(3, '444');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `LaborCultural`
--

CREATE TABLE IF NOT EXISTS `LaborCultural` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCuadro` int(11) NOT NULL,
  `IdTipoLabor` int(11) NOT NULL,
  `Detalle` varchar(1000) NOT NULL,
  `Fecha` date NOT NULL,
  `Horas` int(11) NOT NULL,
  `IdEquipo` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_LaborCultural_Cuadro` (`IdCuadro`),
  KEY `IdTipoLabor` (`IdTipoLabor`),
  KEY `IdEquipo` (`IdEquipo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `LaborCultural`
--

INSERT INTO `LaborCultural` (`Id`, `IdCuadro`, `IdTipoLabor`, `Detalle`, `Fecha`, `Horas`, `IdEquipo`) VALUES
(1, 3, 2, 'sdfsdfsf', '2014-08-12', 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Localidad`
--

CREATE TABLE IF NOT EXISTS `Localidad` (
  `Id` int(11) NOT NULL,
  `Nombre` varchar(200) NOT NULL,
  `IdProvincia` int(11) NOT NULL,
  `CodigoPostal` varchar(45) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdDepartamento` (`IdProvincia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Localidad`
--

INSERT INTO `Localidad` (`Id`, `Nombre`, `IdProvincia`, `CodigoPostal`) VALUES
(1, 'Neuquén', 1, '8300'),
(2, 'Centenario', 1, ''),
(3, 'Cinco Saltos', 2, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Log`
--

CREATE TABLE IF NOT EXISTS `Log` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Detalle` varchar(1000) NOT NULL,
  `IdUsuario` int(11) NOT NULL,
  `Fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  KEY `IdUsuario` (`IdUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `MuestreoDanioCarpocapsa`
--

CREATE TABLE IF NOT EXISTS `MuestreoDanioCarpocapsa` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCuadro` int(11) NOT NULL,
  `IdVariedad` int(11) NOT NULL,
  `Superficie` decimal(10,2) NOT NULL COMMENT 'Dato Calculado sobre plantaciones',
  `Fecha` date DEFAULT NULL,
  `Muestra` int(11) DEFAULT NULL,
  `FrutosDaniados` int(11) DEFAULT NULL,
  `PorcentajeDanio` decimal(10,2) DEFAULT NULL,
  `FechaCosecha` date DEFAULT NULL,
  `MuestraCosecha` int(11) DEFAULT NULL,
  `FrutosDaniadosCosecha` int(11) DEFAULT NULL,
  `PorcentajeDanioCosecha` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdVariedad` (`IdVariedad`),
  KEY `IdCuadro` (`IdCuadro`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `MuestreoDanioCarpocapsa`
--

INSERT INTO `MuestreoDanioCarpocapsa` (`Id`, `IdCuadro`, `IdVariedad`, `Superficie`, `Fecha`, `Muestra`, `FrutosDaniados`, `PorcentajeDanio`, `FechaCosecha`, `MuestraCosecha`, `FrutosDaniadosCosecha`, `PorcentajeDanioCosecha`) VALUES
(1, 3, 1, '12.00', '2014-08-01', 1, 12, '5.00', '2014-12-25', 1, 5, '3.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Plaga`
--

CREATE TABLE IF NOT EXISTS `Plaga` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(255) NOT NULL COMMENT 'Nombre de la plaga, predador o enfermedad',
  `Especie` varchar(255) NOT NULL COMMENT 'Tipo: Plaga, Predador o Enfermedad',
  `Genero` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `Plaga`
--

INSERT INTO `Plaga` (`Id`, `Nombre`, `Especie`, `Genero`) VALUES
(1, 'Plaga1', 'Especie1', 'Genero1'),
(2, 'Plaga2', 'Especie1', 'Genero1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Plantacion`
--

CREATE TABLE IF NOT EXISTS `Plantacion` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCuadro` int(11) NOT NULL,
  `IdTipoProduccion` int(11) NOT NULL,
  `IdVariedad` int(11) NOT NULL COMMENT 'Se debe desplegar desde la elección de la Especialidad',
  `IdPie` int(11) NOT NULL,
  `IdCond` int(11) NOT NULL,
  `Anio` int(11) NOT NULL,
  `DFilas` decimal(10,2) NOT NULL,
  `DPlantas` decimal(10,2) NOT NULL,
  `NumeroPlantas` int(11) NOT NULL,
  `SuperficieNeta` decimal(10,2) NOT NULL,
  `Catastro` varchar(45) NOT NULL,
  `Latitud` double DEFAULT NULL,
  `Longitud` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdCuadro` (`IdCuadro`),
  KEY `IdTipoProduccion` (`IdTipoProduccion`),
  KEY `IdVariedad` (`IdVariedad`),
  KEY `IdPie` (`IdPie`),
  KEY `IdCond` (`IdCond`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `Plantacion`
--

INSERT INTO `Plantacion` (`Id`, `IdCuadro`, `IdTipoProduccion`, `IdVariedad`, `IdPie`, `IdCond`, `Anio`, `DFilas`, `DPlantas`, `NumeroPlantas`, `SuperficieNeta`, `Catastro`, `Latitud`, `Longitud`) VALUES
(1, 3, 1, 3, 4, 1, 1998, '1.50', '2.50', 45, '0.20', '34534535N', -38.975342, -68.044064),
(2, 2, 1, 8, 3, 1, 1988, '3.50', '3.00', 435, '0.46', '0321234', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `PortaInjertos`
--

CREATE TABLE IF NOT EXISTS `PortaInjertos` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdEspecie` int(11) NOT NULL,
  `Nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdEspecie` (`IdEspecie`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=66 ;

--
-- Volcado de datos para la tabla `PortaInjertos`
--

INSERT INTO `PortaInjertos` (`Id`, `IdEspecie`, `Nombre`) VALUES
(1, 1, 'EM 07'),
(2, 2, 'FRANCO'),
(3, 2, 'MEMBRILLERO'),
(4, 1, 'EM 09'),
(5, 1, 'MM 111'),
(6, 1, 'MI 793'),
(7, 1, 'EM 4'),
(8, 1, 'FRANCO'),
(9, 1, 'PI 80'),
(10, 3, 'FRANCO'),
(11, 4, 'FRANCO'),
(12, 3, 'CUARESMILLO'),
(13, 4, 'CUARESMILLO'),
(14, 3, 'NEMAGUARD'),
(15, 4, 'NEMAGUARD'),
(16, 3, 'NEMARED'),
(17, 4, 'NEMARED'),
(18, 3, 'GF. 667'),
(19, 4, 'GF. 677'),
(20, 3, 'GF. 655'),
(21, 4, 'GF. 655'),
(22, 3, 'SERIE I.S.'),
(23, 4, 'SERIE I.S.'),
(24, 5, 'MIRABOLAN B - E.M.'),
(25, 5, 'MIRABOLAN 29-C'),
(26, 5, 'CITATION'),
(27, 6, 'MAHALEB'),
(28, 6, 'SANTA LUCÍA 64'),
(29, 6, 'PONTALEB'),
(30, 6, 'MAXMA'),
(31, 6, 'GISELA'),
(32, 6, 'CAB 6P'),
(33, 6, 'MERICIER'),
(34, 6, 'COLT'),
(35, 6, 'FRANCO'),
(36, 8, 'FRANCO'),
(37, 8, 'NEGRO CALIFORNIANO'),
(38, 8, 'NEGRO'),
(39, 8, 'PARADOX'),
(40, 8, 'AUTOENRAIZADAS'),
(41, 10, 'AUTOENRAIZADAS'),
(42, 1, 'otro'),
(43, 3, 'otro'),
(44, 2, 'otro'),
(45, 4, 'otro'),
(46, 5, 'otro'),
(47, 6, 'otro'),
(48, 7, 'otro'),
(49, 8, 'otro'),
(50, 9, 'otro'),
(51, 10, 'otro'),
(64, 11, 'otro'),
(65, 12, 'otro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Productor`
--

CREATE TABLE IF NOT EXISTS `Productor` (
  `Id` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Productor`
--

INSERT INTO `Productor` (`Id`) VALUES
(2),
(5),
(6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Profesional`
--

CREATE TABLE IF NOT EXISTS `Profesional` (
  `Id` int(11) NOT NULL,
  `Matricula` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Profesional`
--

INSERT INTO `Profesional` (`Id`, `Matricula`) VALUES
(7, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Provincia`
--

CREATE TABLE IF NOT EXISTS `Provincia` (
  `Id` int(11) NOT NULL,
  `Nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Provincia`
--

INSERT INTO `Provincia` (`Id`, `Nombre`) VALUES
(1, 'Neuquén'),
(2, 'Rio Negro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `RegistrosFenologicos`
--

CREATE TABLE IF NOT EXISTS `RegistrosFenologicos` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCuaderno` int(11) NOT NULL,
  `IdVariedad` int(11) NOT NULL,
  `InicioFloracion` date DEFAULT NULL,
  `PlenaFloracion` date DEFAULT NULL,
  `CaidaPetalos` date DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdCuaderno` (`IdCuaderno`),
  KEY `IdVariedad` (`IdVariedad`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `RegistrosFenologicos`
--

INSERT INTO `RegistrosFenologicos` (`Id`, `IdCuaderno`, `IdVariedad`, `InicioFloracion`, `PlenaFloracion`, `CaidaPetalos`) VALUES
(3, 1, 8, '2014-08-07', '2014-08-14', '2014-08-31'),
(4, 1, 2, '2014-08-12', '2014-08-30', '2014-09-28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Rol`
--

CREATE TABLE IF NOT EXISTS `Rol` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Rol` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Rol`
--

INSERT INTO `Rol` (`Id`, `Rol`) VALUES
(1, 'administrador'),
(2, 'inspector'),
(3, 'productor'),
(4, 'profesional');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `SalidaFruta`
--

CREATE TABLE IF NOT EXISTS `SalidaFruta` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCuaderno` int(11) NOT NULL,
  `IdUmi` int(11) NOT NULL,
  `Fecha` date NOT NULL,
  `IdVariedad` int(11) NOT NULL,
  `CantidadBins` int(11) NOT NULL,
  `NumeroRemito` varchar(255) DEFAULT NULL,
  `IdDestino` int(11) NOT NULL,
  `Observaciones` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdCuaderno` (`IdCuaderno`),
  KEY `IdUmi` (`IdUmi`),
  KEY `IdVariedad` (`IdVariedad`),
  KEY `IdDestino` (`IdDestino`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `SalidaFruta`
--

INSERT INTO `SalidaFruta` (`Id`, `IdCuaderno`, `IdUmi`, `Fecha`, `IdVariedad`, `CantidadBins`, `NumeroRemito`, `IdDestino`, `Observaciones`) VALUES
(1, 1, 2, '2014-08-12', 1, 234, '2342352362', 1, 'dfgaijgaidjgapidjg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `SistemaConduccion`
--

CREATE TABLE IF NOT EXISTS `SistemaConduccion` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(255) NOT NULL,
  `Codigo` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `SistemaConduccion`
--

INSERT INTO `SistemaConduccion` (`Id`, `Nombre`, `Codigo`) VALUES
(1, 'Libre Tradicional', '1'),
(2, 'Eje Central', '2'),
(3, 'Espaldera', '3'),
(4, 'Paragüita', '4'),
(5, 'Tatura', '5'),
(6, 'Doble Eje', '6'),
(7, 'Drapeau', '7'),
(8, 'Vaso', '8'),
(9, 'Otro', '9');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `TipoLabor`
--

CREATE TABLE IF NOT EXISTS `TipoLabor` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `Id` (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `TipoLabor`
--

INSERT INTO `TipoLabor` (`Id`, `Nombre`) VALUES
(1, 'Riego'),
(2, 'Poda'),
(3, 'Raleo'),
(4, 'Cosecha');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `TipoProduccion`
--

CREATE TABLE IF NOT EXISTS `TipoProduccion` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `TipoProduccion`
--

INSERT INTO `TipoProduccion` (`Id`, `Nombre`) VALUES
(1, 'Conv');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Tractorista`
--

CREATE TABLE IF NOT EXISTS `Tractorista` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCuaderno` int(11) NOT NULL,
  `NombreApellido` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdUsuario` (`IdCuaderno`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `Tractorista`
--

INSERT INTO `Tractorista` (`Id`, `IdCuaderno`, `NombreApellido`) VALUES
(1, 1, 'Ramon'),
(2, 2, 'Argento'),
(3, 2, 'Charango'),
(5, 4, 'Tractorista 2 cha');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Trampa`
--

CREATE TABLE IF NOT EXISTS `Trampa` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCuadro` int(11) NOT NULL,
  `Numero` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdCuadro` (`IdCuadro`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `Trampa`
--

INSERT INTO `Trampa` (`Id`, `IdCuadro`, `Numero`) VALUES
(1, 3, '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Tratamiento`
--

CREATE TABLE IF NOT EXISTS `Tratamiento` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `NombreComercial` varchar(255) NOT NULL,
  `PrincipioActivo` varchar(255) NOT NULL,
  `Dosis` decimal(10,2) NOT NULL,
  `IdUnidad` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `idUnidad` (`IdUnidad`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `Tratamiento`
--

INSERT INTO `Tratamiento` (`Id`, `NombreComercial`, `PrincipioActivo`, `Dosis`, `IdUnidad`) VALUES
(1, 'Aceite', 'Aceite', '2.00', 1),
(2, 'Polisulfuro', 'Polisulfuro', '3.00', 1),
(3, 'Entrus', 'Entrus', '2.00', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `TratamientoFitosanitario`
--

CREATE TABLE IF NOT EXISTS `TratamientoFitosanitario` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCuaderno` int(11) NOT NULL,
  `IdEquipo` int(11) NOT NULL,
  `IdTratamiento` int(11) NOT NULL,
  `DosisMaquina` decimal(10,2) NOT NULL,
  `IdUnidadDosis` int(11) NOT NULL,
  `TiempoCarencia` int(11) DEFAULT NULL,
  `TiempoReingreso` int(11) DEFAULT NULL,
  `IdAplicador` int(11) NOT NULL,
  `IdMotivo` int(11) NOT NULL,
  `IdVariedad` int(11) NOT NULL,
  `Observaciones` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdVariedad` (`IdVariedad`),
  KEY `IdMotivo` (`IdMotivo`),
  KEY `IdTratamiento` (`IdTratamiento`),
  KEY `IdTratamiento_2` (`IdTratamiento`),
  KEY `IdUnidadDosis` (`IdUnidadDosis`),
  KEY `IdEquipo` (`IdEquipo`),
  KEY `IdAplicador` (`IdAplicador`),
  KEY `FK_TratamientoFitosanitario_Cuaderno` (`IdCuaderno`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `TratamientoFitosanitario`
--

INSERT INTO `TratamientoFitosanitario` (`Id`, `IdCuaderno`, `IdEquipo`, `IdTratamiento`, `DosisMaquina`, `IdUnidadDosis`, `TiempoCarencia`, `TiempoReingreso`, `IdAplicador`, `IdMotivo`, `IdVariedad`, `Observaciones`) VALUES
(1, 1, 1, 1, '2.00', 1, 23, 23, 1, 1, 7, 'asdasdasd'),
(2, 1, 2, 1, '1.00', 1, 1, 1, 1, 1, 1, 'asd'),
(3, 1, 1, 1, '4.00', 1, 4, 4, 1, 1, 72, 'adasdasda'),
(4, 2, 3, 1, '3.00', 1, 4, 4, 2, 1, 2, 'adasdad'),
(5, 4, 5, 1, '123.00', 1, 23, 23, 5, 1, 7, 'asdada');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Umi`
--

CREATE TABLE IF NOT EXISTS `Umi` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCuaderno` int(11) NOT NULL,
  `Codigo` varchar(45) DEFAULT NULL,
  `Descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdExplotacion` (`IdCuaderno`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `Umi`
--

INSERT INTO `Umi` (`Id`, `IdCuaderno`, `Codigo`, `Descripcion`) VALUES
(1, 1, '78X', 'SIETE OCHO EQUIS'),
(2, 1, '78Y', 'SIETE OCHO YE'),
(3, 1, '78W', 'SIETE OCHO DOBLE VE'),
(4, 1, '78Z', 'SIETE OCHO ZETA'),
(5, 2, 'Z99', 'ZETA NUEVE NUEVE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `UnidadDosis`
--

CREATE TABLE IF NOT EXISTS `UnidadDosis` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Unidad` varchar(20) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `UnidadDosis`
--

INSERT INTO `UnidadDosis` (`Id`, `Unidad`) VALUES
(1, '%'),
(2, 'grs.'),
(3, 'lt.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Usuario`
--

CREATE TABLE IF NOT EXISTS `Usuario` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Usuario` varchar(255) NOT NULL,
  `Clave` varchar(32) NOT NULL,
  `IdRol` int(11) NOT NULL,
  `NombreApellido` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Direccion` varchar(255) DEFAULT NULL,
  `IdLocalidad` int(11) DEFAULT NULL,
  `Telefono` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Usuario` (`Usuario`),
  KEY `idRol` (`IdRol`),
  KEY `IdLocalidad` (`IdLocalidad`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `Usuario`
--

INSERT INTO `Usuario` (`Id`, `Usuario`, `Clave`, `IdRol`, `NombreApellido`, `Email`, `Direccion`, `IdLocalidad`, `Telefono`) VALUES
(1, 'admin', 'admin', 1, 'AdminAdministra', 'admin@administra', NULL, NULL, NULL),
(2, 'usuario', 'usuario', 2, 'UsuarioUsa', 'usuario@usa', NULL, NULL, NULL),
(3, 'Pepito', 'pepito', 3, 'Pepito Perez', 'pepito@', 'Calle falsa 123', 1, '45645646'),
(4, 'Pepe2', 'pepe', 3, 'pepe2', 'pepe2@asaf', 'asdasd 123', 2, '454545'),
(5, 'pp', 'pp', 3, 'pp', 'pp', 'pp', 1, '234'),
(6, 'messi', 'messi', 3, 'messi', 'messi@asd', 'messi', 1, '123123'),
(7, 'sabella', 'sabella', 4, 'sabella', 'sabella@', 'sabella', 1, '123123');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Variedad`
--

CREATE TABLE IF NOT EXISTS `Variedad` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(255) NOT NULL,
  `Codigo` varchar(50) NOT NULL,
  `IdEspecie` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdEspecie` (`IdEspecie`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=114 ;

--
-- Volcado de datos para la tabla `Variedad`
--

INSERT INTO `Variedad` (`Id`, `Nombre`, `Codigo`, `IdEspecie`) VALUES
(1, 'RED CHIEF', 'RCH', 1),
(2, 'GRANNY SMITH', 'GRA', 1),
(3, 'TOD RED', 'TRD', 1),
(4, 'GALAXY', 'GALX', 1),
(5, 'BROOKFIELD', 'BRO', 1),
(6, 'PINK LADY', 'PLY', 1),
(7, 'WILLIAM´S', 'WILL', 2),
(8, 'PACKHAM´S TRIUMPH', 'PTRI', 2),
(9, 'BEURRE D´ANJOU', 'DANJ', 2),
(10, 'ABATE FETEL', 'AFET', 2),
(11, 'CONFERENCE', 'CONF', 2),
(12, 'BUERRÈ BOSC', 'BBOS', 2),
(13, 'WINTER NELIS', 'WNEL', 2),
(14, 'CLAPP FAVORITA', 'CLAP', 2),
(15, 'GIFFARD', 'GIFF', 2),
(16, 'GOLDEN RUSSET BOSC', 'GBOS', 2),
(17, 'SENSATION', 'SENS', 2),
(18, 'O''HENRY', 'OHE', 3),
(19, 'RICH LADY', 'RLA', 3),
(20, 'SUMMER ZEE', 'SZEE', 3),
(21, 'ROYAL GLORY', 'RGLO', 3),
(22, 'ZEE DIAMOND', 'ZEED', 3),
(23, 'QUEEN CREST', 'QUEE', 3),
(24, 'MARIA BIANCA', 'MBIA', 3),
(25, 'ZEE LADY', 'ZEEL', 3),
(26, 'PAVIA DE MARZO', 'PMAR', 3),
(27, 'SWEET SEPTEMBER', 'SSET', 3),
(28, 'SEPTEMBER SNOW', 'SNOW', 3),
(29, '54 LD', '54LD', 3),
(30, 'SUMMER LADY', 'SUML', 3),
(31, 'CALDESI 2000', 'CALD', 4),
(32, 'ARTIC JAY', 'ASAY', 4),
(33, 'ARTIC SNOW', 'ASNO', 4),
(34, 'EARLY SUNGRAND', 'ESUN', 4),
(35, 'FLAMEKIST', 'FLAM', 4),
(36, 'FANTASIA', 'FANT', 4),
(37, 'SUMMER GRAND', 'SGRA', 4),
(38, 'SUMMER BRIGHT', 'SBRI', 4),
(39, 'STARL RED GLOD', 'SRGL', 4),
(40, 'AUGUST RED', 'AUGR', 4),
(41, 'FLAMING RED', 'FLAR', 4),
(42, 'BIG TOP', 'BIGT', 4),
(43, 'BLACK AMBER', 'BAMB', 5),
(44, 'BLUE GIANT', 'BGIA', 5),
(45, 'FORTUNE', 'FORT', 5),
(46, 'FRIAR', 'FRIA', 5),
(47, 'LINDA ROSA', 'LIND', 5),
(48, 'ROYAL DIAMOND', 'ROYD', 5),
(49, 'LARRY ANN', 'LANN', 5),
(50, 'SON GOLD', 'SGOL', 5),
(51, 'ANGELENO', 'ANGE', 5),
(52, '121 GD 233', '121G', 5),
(53, 'ROYSUM', 'RSUM', 5),
(54, 'EARLY BURLAT', 'EBUR', 6),
(55, 'VAN', 'VAN', 6),
(56, 'BING', 'BING', 6),
(57, 'LAPINS', 'LAPI', 6),
(58, 'NEWSTAR', 'NEWS', 6),
(59, 'SUNBURST', 'SUNB', 6),
(60, 'SUMMIT', 'SUMM', 6),
(61, 'SWEETHEART', 'SWEE', 6),
(62, 'SYLVIA', 'SYLV', 6),
(63, 'BROOKS', 'BRKS', 6),
(64, 'CHELAN', 'CHEL', 6),
(65, 'MALBEC', 'MALB', 7),
(66, 'MERLOT', 'MERL', 7),
(67, 'PINOT NOIR', 'PINO', 7),
(68, 'CABERNET SAUVIGNON', 'CABE', 7),
(69, 'SYRAH', 'SYRA', 7),
(70, 'CHARDONAY', 'CHAR', 7),
(71, 'SEMILLON', 'SEMI', 7),
(72, 'SAUVIGNON BLANC', 'SAUV', 7),
(73, 'RED DELICIOUS', 'RD', 1),
(74, 'GALA', 'GALA', 1),
(75, 'FUJI', 'FUJI', 1),
(76, 'RAINER', 'RAIN', 6),
(77, 'STELLA', 'STEL', 6),
(78, 'SPRING LADY', 'SPLA', 3),
(79, 'FAIRTIME', 'FAIR', 3),
(80, 'MAY CREST', 'MCRE', 3),
(81, 'ZEE GLO', 'ZEE', 4),
(82, 'ALFONSO LAVALLÉ', 'LAV', 9),
(83, 'ITALIA', 'ITAL', 9),
(84, 'MOSCATEL ROSADA', 'MOSC', 9),
(85, 'CHANDLER', 'CHAN', 8),
(86, 'CISCO', 'CISC', 8),
(87, 'FRANQUETTE', 'FRAN', 8),
(88, 'MOSCATEL SIN SEMILLA', 'MSS', 9),
(89, 'TORRONTES RIOJANO', 'TRR', 7),
(90, 'WINTER BARTLETT', 'WBT', 2),
(91, 'RED BARTLETT', 'RBT', 2),
(92, 'RED D''ANJOU', 'RDJ', 2),
(93, 'ELEGAN LADY', 'ELL', 3),
(94, 'SUMMER SET', 'SMS', 3),
(95, 'FAIRLANE', 'FRL', 4),
(96, 'MAYETTE', 'MYT', 8),
(97, 'IVARTO', 'IVT', 8),
(98, 'HOWARD', 'HWR', 8),
(99, 'TULARE', 'TUL', 8),
(100, 'CRIOLLA O SEEDLING', 'CCS', 8),
(101, 'ITALICA (Criollo)', 'ACRI', 10),
(102, 'THAYSSIANA (Chileno)', 'ATHY', 10),
(103, 'JEAN POURTET (Blanc de Garonne)', 'ALP', 10),
(104, 'P.xCANESCENS (H. Español))', 'HESP', 10),
(105, 'P. ALBA (Bolleana)', 'HBOL', 10),
(106, 'P.xCANADENSIS (I-214)', 'H214', 10),
(107, 'P.xCANADENSIS (I-488)', 'H488', 10),
(108, 'P.xCANADENSIS (Conti 12)', 'HC12', 10),
(109, 'P.xCANADENSIS (Guardi)', 'HGDI', 10),
(110, 'POPULUS SIMONII (Chino)', 'PSII', 10),
(111, 'ROYAL GALA', 'RG', 1),
(112, 'PASTURAS VARIAS', 'PAVS', 11),
(113, 'Sin Cultivos', 'SINC', 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `VisitaInspeccion`
--

CREATE TABLE IF NOT EXISTS `VisitaInspeccion` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCuaderno` int(11) NOT NULL,
  `IdInspector` int(11) DEFAULT NULL,
  `NombreApellido` varchar(255) NOT NULL,
  `Legajo` varchar(255) NOT NULL,
  `Fecha` date DEFAULT NULL,
  `Observaciones` text,
  PRIMARY KEY (`Id`),
  KEY `IdCuaderno` (`IdCuaderno`),
  KEY `IdInspector` (`IdInspector`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `VisitaInspeccion`
--

INSERT INTO `VisitaInspeccion` (`Id`, `IdCuaderno`, `IdInspector`, `NombreApellido`, `Legajo`, `Fecha`, `Observaciones`) VALUES
(2, 1, NULL, 'Pepe', '123', '2014-08-05', 'una obsss');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `AplicacionTratamiento`
--
ALTER TABLE `AplicacionTratamiento`
  ADD CONSTRAINT `AplicacionTratamiento_ibfk_1` FOREIGN KEY (`IdCuadro`) REFERENCES `Cuadro` (`Id`),
  ADD CONSTRAINT `AplicacionTratamiento_ibfk_2` FOREIGN KEY (`IdTratamientoFitosanitario`) REFERENCES `TratamientoFitosanitario` (`Id`);

--
-- Filtros para la tabla `CapturaCarpocapsa`
--
ALTER TABLE `CapturaCarpocapsa`
  ADD CONSTRAINT `CapturaCarpocapsa_ibfk_1` FOREIGN KEY (`IdTrampa`) REFERENCES `Trampa` (`Id`);

--
-- Filtros para la tabla `Cuaderno`
--
ALTER TABLE `Cuaderno`
  ADD CONSTRAINT `Cuaderno_ibfk_1` FOREIGN KEY (`IdExplotacion`) REFERENCES `Explotacion` (`Id`);

--
-- Filtros para la tabla `Cuadro`
--
ALTER TABLE `Cuadro`
  ADD CONSTRAINT `FK_Cuadro_Umi` FOREIGN KEY (`IdUmi`) REFERENCES `Umi` (`Id`);

--
-- Filtros para la tabla `Destino`
--
ALTER TABLE `Destino`
  ADD CONSTRAINT `Destino_ibfk_1` FOREIGN KEY (`IdCuaderno`) REFERENCES `Cuaderno` (`Id`);

--
-- Filtros para la tabla `Equipo`
--
ALTER TABLE `Equipo`
  ADD CONSTRAINT `Equipo_ibfk_1` FOREIGN KEY (`IdCuaderno`) REFERENCES `Cuaderno` (`Id`),
  ADD CONSTRAINT `Equipo_ibfk_2` FOREIGN KEY (`IdTractorista`) REFERENCES `Tractorista` (`Id`);

--
-- Filtros para la tabla `Explotacion`
--
ALTER TABLE `Explotacion`
  ADD CONSTRAINT `Explotacion_ibfk_1` FOREIGN KEY (`IdProductor`) REFERENCES `Productor` (`Id`),
  ADD CONSTRAINT `FK_Explotacion_Localidad` FOREIGN KEY (`IdLocalidad`) REFERENCES `Localidad` (`Id`);

--
-- Filtros para la tabla `FormularioA`
--
ALTER TABLE `FormularioA`
  ADD CONSTRAINT `FormularioA_ibfk_1` FOREIGN KEY (`IdEspecie`) REFERENCES `Especie` (`Id`),
  ADD CONSTRAINT `FormularioA_ibfk_2` FOREIGN KEY (`IdCuadro`) REFERENCES `Cuadro` (`Id`);

--
-- Filtros para la tabla `Inspector`
--
ALTER TABLE `Inspector`
  ADD CONSTRAINT `Inspector_ibfk_1` FOREIGN KEY (`Id`) REFERENCES `Usuario` (`id`);

--
-- Filtros para la tabla `LaborCultural`
--
ALTER TABLE `LaborCultural`
  ADD CONSTRAINT `LaborCultural_ibfk_2` FOREIGN KEY (`IdEquipo`) REFERENCES `Equipo` (`Id`),
  ADD CONSTRAINT `FK_LaborCultural_Cuadro` FOREIGN KEY (`IdCuadro`) REFERENCES `Cuadro` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `LaborCultural_ibfk_1` FOREIGN KEY (`IdTipoLabor`) REFERENCES `TipoLabor` (`Id`);

--
-- Filtros para la tabla `Localidad`
--
ALTER TABLE `Localidad`
  ADD CONSTRAINT `Localidad_ibfk_1` FOREIGN KEY (`IdProvincia`) REFERENCES `Provincia` (`Id`);

--
-- Filtros para la tabla `MuestreoDanioCarpocapsa`
--
ALTER TABLE `MuestreoDanioCarpocapsa`
  ADD CONSTRAINT `MuestreoDanioCarpocapsa_ibfk_1` FOREIGN KEY (`IdVariedad`) REFERENCES `Variedad` (`Id`),
  ADD CONSTRAINT `MuestreoDanioCarpocapsa_ibfk_2` FOREIGN KEY (`IdCuadro`) REFERENCES `Cuadro` (`Id`);

--
-- Filtros para la tabla `Plantacion`
--
ALTER TABLE `Plantacion`
  ADD CONSTRAINT `Plantacion_ibfk_1` FOREIGN KEY (`IdCuadro`) REFERENCES `Cuadro` (`Id`),
  ADD CONSTRAINT `Plantacion_ibfk_3` FOREIGN KEY (`IdVariedad`) REFERENCES `Variedad` (`Id`),
  ADD CONSTRAINT `Plantacion_ibfk_4` FOREIGN KEY (`IdPie`) REFERENCES `PortaInjertos` (`Id`),
  ADD CONSTRAINT `Plantacion_ibfk_5` FOREIGN KEY (`IdCond`) REFERENCES `SistemaConduccion` (`Id`),
  ADD CONSTRAINT `Plantacion_ibfk_6` FOREIGN KEY (`IdTipoProduccion`) REFERENCES `TipoProduccion` (`Id`);

--
-- Filtros para la tabla `PortaInjertos`
--
ALTER TABLE `PortaInjertos`
  ADD CONSTRAINT `PortaInjertos_ibfk_1` FOREIGN KEY (`IdEspecie`) REFERENCES `Especie` (`Id`);

--
-- Filtros para la tabla `Productor`
--
ALTER TABLE `Productor`
  ADD CONSTRAINT `Productor_ibfk_1` FOREIGN KEY (`Id`) REFERENCES `Usuario` (`id`);

--
-- Filtros para la tabla `Profesional`
--
ALTER TABLE `Profesional`
  ADD CONSTRAINT `Profesional_ibfk_1` FOREIGN KEY (`Id`) REFERENCES `Usuario` (`id`);

--
-- Filtros para la tabla `RegistrosFenologicos`
--
ALTER TABLE `RegistrosFenologicos`
  ADD CONSTRAINT `RegistrosFenologicos_ibfk_1` FOREIGN KEY (`IdCuaderno`) REFERENCES `Cuaderno` (`Id`),
  ADD CONSTRAINT `RegistrosFenologicos_ibfk_3` FOREIGN KEY (`IdVariedad`) REFERENCES `Variedad` (`Id`);

--
-- Filtros para la tabla `SalidaFruta`
--
ALTER TABLE `SalidaFruta`
  ADD CONSTRAINT `FK_SalidaFruta_Umi` FOREIGN KEY (`IdUmi`) REFERENCES `Umi` (`Id`),
  ADD CONSTRAINT `SalidaFruta_ibfk_1` FOREIGN KEY (`IdCuaderno`) REFERENCES `Cuaderno` (`Id`),
  ADD CONSTRAINT `SalidaFruta_ibfk_4` FOREIGN KEY (`IdVariedad`) REFERENCES `Variedad` (`Id`),
  ADD CONSTRAINT `SalidaFruta_ibfk_5` FOREIGN KEY (`IdDestino`) REFERENCES `Destino` (`Id`);

--
-- Filtros para la tabla `Tractorista`
--
ALTER TABLE `Tractorista`
  ADD CONSTRAINT `Tractorista_ibfk_1` FOREIGN KEY (`IdCuaderno`) REFERENCES `Cuaderno` (`Id`);

--
-- Filtros para la tabla `Trampa`
--
ALTER TABLE `Trampa`
  ADD CONSTRAINT `Trampa_ibfk_1` FOREIGN KEY (`IdCuadro`) REFERENCES `Cuadro` (`Id`);

--
-- Filtros para la tabla `Tratamiento`
--
ALTER TABLE `Tratamiento`
  ADD CONSTRAINT `Tratamiento_ibfk_1` FOREIGN KEY (`IdUnidad`) REFERENCES `UnidadDosis` (`Id`);

--
-- Filtros para la tabla `TratamientoFitosanitario`
--
ALTER TABLE `TratamientoFitosanitario`
  ADD CONSTRAINT `FK_TratamientoFitosanitario_Cuaderno` FOREIGN KEY (`IdCuaderno`) REFERENCES `Cuaderno` (`Id`),
  ADD CONSTRAINT `TratamientoFitosanitario_ibfk_1` FOREIGN KEY (`IdMotivo`) REFERENCES `Plaga` (`Id`),
  ADD CONSTRAINT `TratamientoFitosanitario_ibfk_2` FOREIGN KEY (`IdVariedad`) REFERENCES `Variedad` (`Id`),
  ADD CONSTRAINT `TratamientoFitosanitario_ibfk_3` FOREIGN KEY (`IdTratamiento`) REFERENCES `Tratamiento` (`Id`),
  ADD CONSTRAINT `TratamientoFitosanitario_ibfk_4` FOREIGN KEY (`IdUnidadDosis`) REFERENCES `UnidadDosis` (`Id`),
  ADD CONSTRAINT `TratamientoFitosanitario_ibfk_6` FOREIGN KEY (`IdEquipo`) REFERENCES `Equipo` (`Id`),
  ADD CONSTRAINT `TratamientoFitosanitario_ibfk_7` FOREIGN KEY (`IdAplicador`) REFERENCES `Tractorista` (`Id`);

--
-- Filtros para la tabla `Umi`
--
ALTER TABLE `Umi`
  ADD CONSTRAINT `Umi_ibfk_1` FOREIGN KEY (`IdCuaderno`) REFERENCES `Cuaderno` (`Id`);

--
-- Filtros para la tabla `Usuario`
--
ALTER TABLE `Usuario`
  ADD CONSTRAINT `Usuario_ibfk_1` FOREIGN KEY (`idRol`) REFERENCES `Rol` (`id`),
  ADD CONSTRAINT `Usuario_ibfk_2` FOREIGN KEY (`IdLocalidad`) REFERENCES `Localidad` (`Id`);

--
-- Filtros para la tabla `Variedad`
--
ALTER TABLE `Variedad`
  ADD CONSTRAINT `Variedad_ibfk_1` FOREIGN KEY (`IdEspecie`) REFERENCES `Especie` (`Id`);

--
-- Filtros para la tabla `VisitaInspeccion`
--
ALTER TABLE `VisitaInspeccion`
  ADD CONSTRAINT `VisitaInspeccion_ibfk_1` FOREIGN KEY (`IdCuaderno`) REFERENCES `Cuaderno` (`Id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
