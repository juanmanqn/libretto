<?php

/**
 * This is the model class for table "TratamientoFitosanitario".
 *
 * The followings are the available columns in table 'TratamientoFitosanitario':
 * @property integer $Id
 * @property integer $IdCuaderno
 * @property integer $IdEquipo
 * @property integer $IdTratamiento
 * @property string $DosisMaquina
 * @property integer $IdUnidadDosis
 * @property integer $TiempoCarencia
 * @property integer $TiempoReingreso
 * @property integer $IdAplicador
 * @property integer $IdMotivo
 * @property integer $IdVariedad
 * @property string $Observaciones
 *
 * The followings are the available model relations:
 * @property AplicacionTratamiento[] $aplicacionTratamientos
 * @property Cuaderno $idCuaderno
 * @property Plaga $idMotivo
 * @property Variedad $idVariedad
 * @property Tratamiento $idTratamiento
 * @property UnidadDosis $idUnidadDosis
 * @property Equipo $idEquipo
 * @property Tractorista $idAplicador
 */
class TratamientoFitosanitario extends CActiveRecord
{
        
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'TratamientoFitosanitario';
	}
        
        public function getLitrosAplicaciones(){
            $litros=0;
            foreach ($this->aplicacionTratamientos as $aplicaciones) {
                /* @var $aplicaciones AplicacionTratamiento*/
                $litros+=$aplicaciones->LitrosCuadro;
            }
            return $litros;
        }
        
        public function getCostoAplicaciones(){
           
            return $this->getLitrosAplicaciones()*$this->idTratamiento->CostoPorLitro;
        }
        

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('IdCuaderno, IdEquipo, IdTratamiento, DosisMaquina, IdUnidadDosis, IdAplicador, IdMotivo, IdVariedad', 'required'),
			array('IdCuaderno, IdEquipo, IdTratamiento, IdUnidadDosis, TiempoCarencia, TiempoReingreso, IdAplicador, IdMotivo, IdVariedad', 'numerical', 'integerOnly'=>true),
			array('DosisMaquina', 'length', 'max'=>10),
			array('Observaciones', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Id, IdCuaderno, IdEquipo, IdTratamiento, DosisMaquina, IdUnidadDosis, TiempoCarencia, TiempoReingreso, IdAplicador, IdMotivo, IdVariedad, Observaciones', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'aplicacionTratamientos' => array(self::HAS_MANY, 'AplicacionTratamiento', 'IdTratamientoFitosanitario'),
			'idCuaderno' => array(self::BELONGS_TO, 'Cuaderno', 'IdCuaderno'),
			'idMotivo' => array(self::BELONGS_TO, 'Plaga', 'IdMotivo'),
			'idVariedad' => array(self::BELONGS_TO, 'Variedad', 'IdVariedad'),
			'idTratamiento' => array(self::BELONGS_TO, 'Tratamiento', 'IdTratamiento'),
			'idUnidadDosis' => array(self::BELONGS_TO, 'UnidadDosis', 'IdUnidadDosis'),
			'idEquipo' => array(self::BELONGS_TO, 'Equipo', 'IdEquipo'),
			'idAplicador' => array(self::BELONGS_TO, 'Tractorista', 'IdAplicador'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'IdCuaderno' => 'Cuaderno',
			'IdEquipo' => 'Equipo',
			'IdTratamiento' => 'Tratamiento',
			'DosisMaquina' => 'Dosis Maquina',
			'IdUnidadDosis' => 'Unidad Dosis',
			'TiempoCarencia' => 'Tiempo Carencia',
			'TiempoReingreso' => 'Tiempo Reingreso',
			'IdAplicador' => 'Aplicador',
			'IdMotivo' => 'Motivo',
			'IdVariedad' => 'Variedad',
			'Observaciones' => 'Observaciones',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                //$criteria->with=array('idAplicador','idEquipo');
                
		$criteria->compare('Id',$this->Id);
		$criteria->compare('IdCuaderno',$this->IdCuaderno);
		$criteria->compare('IdEquipo',$this->IdEquipo);
		$criteria->compare('IdTratamiento',$this->IdTratamiento);
		$criteria->compare('DosisMaquina',$this->DosisMaquina,true);
		$criteria->compare('IdUnidadDosis',$this->IdUnidadDosis);
		$criteria->compare('TiempoCarencia',$this->TiempoCarencia);
		$criteria->compare('TiempoReingreso',$this->TiempoReingreso);
		$criteria->compare('IdAplicador',$this->IdAplicador);
		$criteria->compare('IdMotivo',$this->IdMotivo);
		$criteria->compare('IdVariedad',$this->IdVariedad);
		$criteria->compare('Observaciones',$this->Observaciones,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TratamientoFitosanitario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
