<?php

/**
 * This is the model class for table "FormularioA".
 *
 * The followings are the available columns in table 'FormularioA':
 * @property integer $Id
 * @property integer $IdCuadro
 * @property string $Superficie
 * @property integer $IdEspecie
 * @property string $TipoTCS
 * @property string $DispNroPorCuadroDosis1
 * @property string $DispNroPorHectareaDosis1
 * @property string $DispNroPorCuadroDosis2
 * @property string $DispNroPorHectareaDosis2
 *
 * The followings are the available model relations:
 * @property Especie $idEspecie
 * @property Cuadro $idCuadro
 */
class FormularioA extends CActiveRecord
{
    
    public $idCuaderno;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'FormularioA';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('IdCuadro,  IdEspecie', 'required'),
			array('IdCuadro, IdEspecie', 'numerical', 'integerOnly'=>true),
			array(' DispNroPorCuadroDosis1, DispNroPorHectareaDosis1, DispNroPorCuadroDosis2, DispNroPorHectareaDosis2', 'length', 'max'=>10),
			array('TipoTCS', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Id, IdCuadro,  IdEspecie, TipoTCS, DispNroPorCuadroDosis1, DispNroPorHectareaDosis1, DispNroPorCuadroDosis2, DispNroPorHectareaDosis2', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idEspecie' => array(self::BELONGS_TO, 'Especie', 'IdEspecie'),
			'idCuadro' => array(self::BELONGS_TO, 'Cuadro', 'IdCuadro'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'IdCuadro' => 'Id Cuadro',
			'IdEspecie' => 'Id Especie',
			'TipoTCS' => 'Tipo Tcs',
			'DispNroPorCuadroDosis1' => 'Disp Nro Por Cuadro Dosis1',
			'DispNroPorHectareaDosis1' => 'Disp Nro Por Hectarea Dosis1',
			'DispNroPorCuadroDosis2' => 'Disp Nro Por Cuadro Dosis2',
			'DispNroPorHectareaDosis2' => 'Disp Nro Por Hectarea Dosis2',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                $criteria->with=array('idCuadro','idCuadro.idUmi');

		$criteria->compare('Id',$this->Id);
		$criteria->compare('IdCuadro',$this->IdCuadro);
		$criteria->compare('IdEspecie',$this->IdEspecie);
		$criteria->compare('TipoTCS',$this->TipoTCS,true);
		$criteria->compare('DispNroPorCuadroDosis1',$this->DispNroPorCuadroDosis1,true);
		$criteria->compare('DispNroPorHectareaDosis1',$this->DispNroPorHectareaDosis1,true);
		$criteria->compare('DispNroPorCuadroDosis2',$this->DispNroPorCuadroDosis2,true);
		$criteria->compare('DispNroPorHectareaDosis2',$this->DispNroPorHectareaDosis2,true);
                $criteria->compare('idUmi.IdCuaderno',$this->idCuaderno);
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FormularioA the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
