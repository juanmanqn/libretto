<?php

/**
 * This is the model class for table "Localidad".
 *
 * The followings are the available columns in table 'Localidad':
 * @property integer $Id
 * @property string $Nombre
 * @property integer $IdProvincia
 * @property string $CodigoPostal
 *
 * The followings are the available model relations:
 * @property Explotacion[] $explotacions
 * @property Provincia $idProvincia
 * @property Usuario[] $usuarios
 */
class Localidad extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Localidad';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Id, Nombre, IdProvincia, CodigoPostal', 'required'),
			array('Id, IdProvincia', 'numerical', 'integerOnly'=>true),
			array('Nombre', 'length', 'max'=>200),
			array('CodigoPostal', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Id, Nombre, IdProvincia, CodigoPostal', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'explotacions' => array(self::HAS_MANY, 'Explotacion', 'IdLocalidad'),
			'idProvincia' => array(self::BELONGS_TO, 'Provincia', 'IdProvincia'),
			'usuarios' => array(self::HAS_MANY, 'Usuario', 'IdLocalidad'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'Nombre' => 'Nombre',
			'IdProvincia' => 'Id Provincia',
			'CodigoPostal' => 'Codigo Postal',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Id',$this->Id);
		$criteria->compare('Nombre',$this->Nombre,true);
		$criteria->compare('IdProvincia',$this->IdProvincia);
		$criteria->compare('CodigoPostal',$this->CodigoPostal,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Localidad the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
