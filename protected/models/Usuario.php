<?php

/**
 * This is the model class for table "Usuario".
 *
 * The followings are the available columns in table 'Usuario':
 * @property string $Usuario
 * @property integer $Id
 * @property string $Clave
 * @property integer $IdRol
 * @property string $NombreApellido
 * @property string $Email
 * @property string $Direccion
 * @property integer $IdLocalidad
 * @property string $Telefono
 *
 * The followings are the available model relations:
 * @property Permisos[] $permisoses
 * @property Inspector $inspector
 * @property Productor $productor
 * @property Profesional $profesional
 * @property Rol $idRol
 * @property Localidad $idLocalidad
 */
class Usuario extends CActiveRecord
{
        public $Localidad;
        public $Rol;
                
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Usuario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Usuario, Clave, IdRol, NombreApellido, Email', 'required'),
			array('IdRol, IdLocalidad', 'numerical', 'integerOnly'=>true),
			array('Usuario, NombreApellido, Email, Direccion', 'length', 'max'=>255),
			array('Clave', 'length', 'max'=>32),
			array('Telefono', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Usuario, Id, Clave, IdRol, NombreApellido, Email, Direccion, IdLocalidad, Telefono, Rol, Localidad', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'inspector' => array(self::HAS_ONE, 'Inspector', 'Id'),
			'productor' => array(self::HAS_ONE, 'Productor', 'Id'),
			'profesional' => array(self::HAS_ONE, 'Profesional', 'Id'),
			'idRol' => array(self::BELONGS_TO, 'Rol', 'IdRol'),
			'idLocalidad' => array(self::BELONGS_TO, 'Localidad', 'IdLocalidad'),
                    	'permisoses' => array(self::HAS_MANY, 'Permisos', 'IdUsuario'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Usuario' => 'Usuario',
			'Id' => 'ID',
			'Clave' => 'Clave',
			'IdRol' => 'Rol',
			'NombreApellido' => 'Nombre',
			'Email' => 'Email',
			'Direccion' => 'Direccion',
			'IdLocalidad' => 'Localidad',
			'Telefono' => 'Telefono',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                $criteria->with = array('idLocalidad','idRol');
                
		$criteria->compare('Usuario',$this->Usuario,true);
		$criteria->compare('Id',$this->Id);
		$criteria->compare('Clave',$this->Clave,true);
		$criteria->compare('idRol.Rol',$this->Rol);
		$criteria->compare('NombreApellido',$this->NombreApellido,true);
		$criteria->compare('Email',$this->Email,true);
		$criteria->compare('Direccion',$this->Direccion,true);
		$criteria->compare('idLocalidad.Nombre', $this->Localidad, true);
		$criteria->compare('Telefono',$this->Telefono,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'sort' => array(
                            'attributes' => array(
                                'Localidad' => array(
                                    'asc' => 'idLocalidad.Nombre',
                                    'desc' => 'idLocalidad.Nombre DESC',
                                ),
                                'Rol' => array(
                                    'asc' => 'idRol.Rol',
                                    'desc' => 'idRol.Rol DESC',
                                ),
                                '*',
                            ),
                        ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Usuario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
//        protected function beforeSave() {
//            parent::beforeSave();
//            if($this->IdRol==3){
//                $productor = new Productor();
//                $productor->Id = $this->Id;
//                return $productor->save();                
//            }
//        }
}
