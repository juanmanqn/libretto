<?php

/**
 * This is the model class for table "RegistrosFenologicos".
 *
 * The followings are the available columns in table 'RegistrosFenologicos':
 * @property integer $Id
 * @property integer $IdCuaderno
 * @property integer $IdVariedad
 * @property string $InicioFloracion
 * @property string $PlenaFloracion
 * @property string $CaidaPetalos
 *
 * The followings are the available model relations:
 * @property Cuaderno $idCuaderno
 * @property Variedad $idVariedad
 */
class RegistrosFenologicos extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'RegistrosFenologicos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('IdCuaderno, IdVariedad', 'required'),
			array('IdCuaderno, IdVariedad', 'numerical', 'integerOnly'=>true),
			array('InicioFloracion, PlenaFloracion, CaidaPetalos', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Id, IdCuaderno, IdVariedad, InicioFloracion, PlenaFloracion, CaidaPetalos', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idCuaderno' => array(self::BELONGS_TO, 'Cuaderno', 'IdCuaderno'),
			'idVariedad' => array(self::BELONGS_TO, 'Variedad', 'IdVariedad'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'IdCuaderno' => 'Id Cuaderno',
			'IdVariedad' => 'Id Variedad',
			'InicioFloracion' => 'Inicio Floracion',
			'PlenaFloracion' => 'Plena Floracion',
			'CaidaPetalos' => 'Caida Petalos',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Id',$this->Id);
		$criteria->compare('IdCuaderno',$this->IdCuaderno);
		$criteria->compare('IdVariedad',$this->IdVariedad);
		$criteria->compare('InicioFloracion',$this->InicioFloracion,true);
		$criteria->compare('PlenaFloracion',$this->PlenaFloracion,true);
		$criteria->compare('CaidaPetalos',$this->CaidaPetalos,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RegistrosFenologicos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
