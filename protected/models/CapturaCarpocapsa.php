<?php

/**
 * This is the model class for table "CapturaCarpocapsa".
 *
 * The followings are the available columns in table 'CapturaCarpocapsa':
 * @property integer $Id
 * @property integer $IdTrampa
 * @property string $Fecha
 * @property string $Detalle
 * @property integer $Cantidad
 *
 * The followings are the available model relations:
 * @property Trampa $idTrampa
 */
class CapturaCarpocapsa extends CActiveRecord
{
        public $idCuaderno;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'CapturaCarpocapsa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('IdTrampa, Fecha, Detalle, Cantidad', 'required'),
			array('IdTrampa, Cantidad', 'numerical', 'integerOnly'=>true),
			array('Detalle', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Id, IdTrampa, Fecha, Detalle, Cantidad', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idTrampa' => array(self::BELONGS_TO, 'Trampa', 'IdTrampa'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'IdTrampa' => 'Trampa',
			'Fecha' => 'Fecha',
			'Detalle' => 'Detalle',
			'Cantidad' => 'Cantidad',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

                
                $criteria->with=array('idTrampa','idTrampa.idCuadro','idTrampa.idCuadro.idUmi');
		$criteria->compare('idUmi.IdCuaderno',$this->idCuaderno);
                
                
		$criteria->compare('Id',$this->Id);
		$criteria->compare('IdTrampa',$this->IdTrampa);
		$criteria->compare('Fecha',$this->Fecha,true);
		$criteria->compare('Detalle',$this->Detalle,true);
		$criteria->compare('Cantidad',$this->Cantidad);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CapturaCarpocapsa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
