<?php

/**
 * This is the model class for table "Umi".
 *
 * The followings are the available columns in table 'Umi':
 * @property integer $Id
 * @property integer $IdCuaderno
 * @property string $Codigo
 * @property string $Descripcion
 *
 * The followings are the available model relations:
 * @property Cuadro[] $cuadros
 * @property SalidaFruta[] $salidaFrutas
 * @property Cuaderno $idCuaderno
 */
class Umi extends CActiveRecord
{
        public function getSuperficeVariedad($idVariedad) {
             $suma=0;
            foreach ($this->cuadros as $cuadro) {
                /* @var $cuadro Cuadro*/
                $suma+=$cuadro->getSuperficieVariedad($idVariedad);
            }
            return $suma;
        }
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Umi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('IdCuaderno', 'required'),
			array('IdCuaderno', 'numerical', 'integerOnly'=>true),
			array('Codigo, Descripcion', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Id, IdCuaderno, Codigo, Descripcion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cuadros' => array(self::HAS_MANY, 'Cuadro', 'IdUmi'),
			'salidaFrutas' => array(self::HAS_MANY, 'SalidaFruta', 'IdUmi'),
			'idCuaderno' => array(self::BELONGS_TO, 'Cuaderno', 'IdCuaderno'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'IdCuaderno' => 'Cuaderno',
			'Codigo' => 'UMI',
			'Descripcion' => 'Descripcion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Id',$this->Id);
		$criteria->compare('IdCuaderno',$this->IdCuaderno);
		$criteria->compare('Codigo',$this->Codigo,true);
		$criteria->compare('Descripcion',$this->Descripcion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Umi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
