<?php

/**
 * This is the model class for table "SalidaFruta".
 *
 * The followings are the available columns in table 'SalidaFruta':
 * @property integer $Id
 * @property integer $IdCuaderno
 * @property integer $IdUmi
 * @property string $Fecha
 * @property integer $IdVariedad
 * @property integer $CantidadBins
 * @property string $NumeroRemito
 * @property integer $IdDestino
 * @property string $Observaciones
 * @property integer $Kilos
 *
 * The followings are the available model relations:
 * @property Umi $idUmi
 * @property Cuaderno $idCuaderno
 * @property Variedad $idVariedad
 * @property Destino $idDestino
 */
class SalidaFruta extends CActiveRecord
{
    public function getRindeKgHa() {
        $Ha=$this->idUmi->getSuperficeVariedad($this->IdVariedad);
        if ($Ha!=0){
            return round(($this->Kilos/$Ha),2);
        }else{
            return 0;
        }
    }
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'SalidaFruta';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('IdCuaderno, IdUmi, Fecha, IdVariedad, CantidadBins, IdDestino, Kilos', 'required'),
			array('IdCuaderno, IdUmi, IdVariedad, CantidadBins, IdDestino, Kilos', 'numerical', 'integerOnly'=>true),
			array('NumeroRemito', 'length', 'max'=>255),
			array('Observaciones', 'length', 'max'=>1000),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Id, IdCuaderno, IdUmi, Fecha, IdVariedad, CantidadBins, NumeroRemito, IdDestino, Observaciones, Kilos', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idUmi' => array(self::BELONGS_TO, 'Umi', 'IdUmi'),
			'idCuaderno' => array(self::BELONGS_TO, 'Cuaderno', 'IdCuaderno'),
			'idVariedad' => array(self::BELONGS_TO, 'Variedad', 'IdVariedad'),
			'idDestino' => array(self::BELONGS_TO, 'Destino', 'IdDestino'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'IdCuaderno' => 'Id Cuaderno',
			'IdUmi' => 'Id Umi',
			'Fecha' => 'Fecha',
			'IdVariedad' => 'Id Variedad',
			'CantidadBins' => 'Cantidad Bins',
			'NumeroRemito' => 'Numero Remito',
			'IdDestino' => 'Id Destino',
			'Observaciones' => 'Observaciones',
			'Kilos' => 'Kilos',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Id',$this->Id);
		$criteria->compare('IdCuaderno',$this->IdCuaderno);
		$criteria->compare('IdUmi',$this->IdUmi);
		$criteria->compare('Fecha',$this->Fecha,true);
		$criteria->compare('IdVariedad',$this->IdVariedad);
		$criteria->compare('CantidadBins',$this->CantidadBins);
		$criteria->compare('NumeroRemito',$this->NumeroRemito,true);
		$criteria->compare('IdDestino',$this->IdDestino);
		$criteria->compare('Observaciones',$this->Observaciones,true);
		$criteria->compare('Kilos',$this->Kilos);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SalidaFruta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
