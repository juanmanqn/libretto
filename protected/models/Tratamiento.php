<?php

/**
 * This is the model class for table "Tratamiento".
 *
 * The followings are the available columns in table 'Tratamiento':
 * @property integer $Id
 * @property string $NombreComercial
 * @property string $PrincipioActivo
 * @property string $Dosis
 * @property integer $IdUnidad
 * @property integer $IdCuaderno
 * @property double $CostoPorLitro
 *
 * The followings are the available model relations:
 * @property Cuaderno $idCuaderno
 * @property UnidadDosis $idUnidad
 * @property TratamientoFitosanitario[] $tratamientoFitosanitarios
 */
class Tratamiento extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Tratamiento';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('NombreComercial, PrincipioActivo, Dosis, IdUnidad, IdCuaderno, CostoPorLitro', 'required'),
			array('IdUnidad, IdCuaderno', 'numerical', 'integerOnly'=>true),
			array('CostoPorLitro', 'numerical'),
			array('NombreComercial, PrincipioActivo', 'length', 'max'=>255),
			array('Dosis', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Id, NombreComercial, PrincipioActivo, Dosis, IdUnidad, IdCuaderno, CostoPorLitro', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idCuaderno' => array(self::BELONGS_TO, 'Cuaderno', 'IdCuaderno'),
			'idUnidad' => array(self::BELONGS_TO, 'UnidadDosis', 'IdUnidad'),
			'tratamientoFitosanitarios' => array(self::HAS_MANY, 'TratamientoFitosanitario', 'IdTratamiento'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'NombreComercial' => 'Nombre Comercial',
			'PrincipioActivo' => 'Principio Activo',
			'Dosis' => 'Dosis',
			'IdUnidad' => 'Unidad',
			'IdCuaderno' => 'Cuaderno',
			'CostoPorLitro' => 'Costo Por Litro',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Id',$this->Id);
		$criteria->compare('NombreComercial',$this->NombreComercial,true);
		$criteria->compare('PrincipioActivo',$this->PrincipioActivo,true);
		$criteria->compare('Dosis',$this->Dosis,true);
		$criteria->compare('IdUnidad',$this->IdUnidad);
		$criteria->compare('IdCuaderno',$this->IdCuaderno);
		$criteria->compare('CostoPorLitro',$this->CostoPorLitro);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tratamiento the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
