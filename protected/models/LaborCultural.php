<?php

/**
 * This is the model class for table "LaborCultural".
 *
 * The followings are the available columns in table 'LaborCultural':
 * @property integer $Id
 * @property integer $IdCuadro
 * @property integer $IdTipoLabor
 * @property string $Detalle
 * @property string $Fecha
 * @property integer $Horas
 * @property integer $IdEquipo
 *
 * The followings are the available model relations:
 * @property Equipo $idEquipo
 * @property Cuadro $idCuadro
 * @property TipoLabor $idTipoLabor
 */
class LaborCultural extends CActiveRecord
{
	
    public function getCosto() {
        return $this->Horas*$this->idTipoLabor->CostoPorHora;
    }
        /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'LaborCultural';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('IdCuadro, IdTipoLabor, Detalle, Fecha, Horas', 'required'),
			array('IdCuadro, IdTipoLabor, Horas, IdEquipo', 'numerical', 'integerOnly'=>true),
			array('Detalle', 'length', 'max'=>1000),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Id, IdCuadro, IdTipoLabor, Detalle, Fecha, Horas, IdEquipo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idEquipo' => array(self::BELONGS_TO, 'Equipo', 'IdEquipo'),
			'idCuadro' => array(self::BELONGS_TO, 'Cuadro', 'IdCuadro'),
			'idTipoLabor' => array(self::BELONGS_TO, 'TipoLabor', 'IdTipoLabor'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'IdCuadro' => 'Cuadro',
			'IdTipoLabor' => 'Tipo Labor',
			'Detalle' => 'Detalle',
			'Fecha' => 'Fecha',
			'Horas' => 'Jornal',
			'IdEquipo' => 'Equipo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Id',$this->Id);
		$criteria->compare('IdCuadro',$this->IdCuadro);
		$criteria->compare('IdTipoLabor',$this->IdTipoLabor);
		$criteria->compare('Detalle',$this->Detalle,true);
		$criteria->compare('Fecha',$this->Fecha,true);
		$criteria->compare('Horas',$this->Horas);
		$criteria->compare('IdEquipo',$this->IdEquipo);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LaborCultural the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
