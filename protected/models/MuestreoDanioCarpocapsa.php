<?php

/**
 * This is the model class for table "MuestreoDanioCarpocapsa".
 *
 * The followings are the available columns in table 'MuestreoDanioCarpocapsa':
 * @property integer $Id
 * @property integer $IdCuadro
 * @property integer $IdVariedad
 * @property string $Fecha
 * @property integer $Muestra
 * @property integer $FrutosDaniados
 * @property string $PorcentajeDanio
 * @property string $FechaCosecha
 * @property integer $MuestraCosecha
 * @property integer $FrutosDaniadosCosecha
 * @property string $PorcentajeDanioCosecha
 *
 * The followings are the available model relations:
 * @property Variedad $idVariedad
 * @property Cuadro $idCuadro
 */
class MuestreoDanioCarpocapsa extends CActiveRecord
{
        public $idCuaderno;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'MuestreoDanioCarpocapsa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('IdCuadro, IdVariedad', 'required'),
			array('IdCuadro, IdVariedad, Muestra, FrutosDaniados, MuestraCosecha, FrutosDaniadosCosecha', 'numerical', 'integerOnly'=>true),
			array('PorcentajeDanio, PorcentajeDanioCosecha', 'length', 'max'=>10),
			array('Fecha, FechaCosecha', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Id, IdCuadro, IdVariedad, Fecha, Muestra, FrutosDaniados, PorcentajeDanio, FechaCosecha, MuestraCosecha, FrutosDaniadosCosecha, PorcentajeDanioCosecha', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idVariedad' => array(self::BELONGS_TO, 'Variedad', 'IdVariedad'),
			'idCuadro' => array(self::BELONGS_TO, 'Cuadro', 'IdCuadro'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'IdCuadro' => 'Cuadro',
			'IdVariedad' => 'Variedad',
			'Fecha' => 'Fecha',
			'Muestra' => 'Muestra',
			'FrutosDaniados' => 'Frutos Dañados',
			'PorcentajeDanio' => 'Porcentaje Daño',
			'FechaCosecha' => 'Fecha Cosecha',
			'MuestraCosecha' => 'Muestra Cosecha',
			'FrutosDaniadosCosecha' => 'Frutos Dañados Cosecha',
			'PorcentajeDanioCosecha' => 'Porcentaje Daño Cosecha',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

                $criteria->with=array('idCuadro','idCuadro.idUmi');
                
		$criteria->compare('Id',$this->Id);
		$criteria->compare('IdCuadro',$this->IdCuadro);
		$criteria->compare('IdVariedad',$this->IdVariedad);
		$criteria->compare('Fecha',$this->Fecha,true);
		$criteria->compare('Muestra',$this->Muestra);
		$criteria->compare('FrutosDaniados',$this->FrutosDaniados);
		$criteria->compare('PorcentajeDanio',$this->PorcentajeDanio,true);
		$criteria->compare('FechaCosecha',$this->FechaCosecha,true);
		$criteria->compare('MuestraCosecha',$this->MuestraCosecha);
		$criteria->compare('FrutosDaniadosCosecha',$this->FrutosDaniadosCosecha);
		$criteria->compare('PorcentajeDanioCosecha',$this->PorcentajeDanioCosecha,true);
                $criteria->compare('idUmi.IdCuaderno',$this->idCuaderno);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MuestreoDanioCarpocapsa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
