<?php

/**
 * This is the model class for table "Tractorista".
 *
 * The followings are the available columns in table 'Tractorista':
 * @property integer $Id
 * @property integer $IdCuaderno
 * @property string $NombreApellido
 *
 * The followings are the available model relations:
 * @property Equipo[] $equipos
 * @property Cuaderno $idCuaderno
 * @property TratamientoFitosanitario[] $tratamientoFitosanitarios
 */
class Tractorista extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Tractorista';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('IdCuaderno, NombreApellido', 'required'),
			array('IdCuaderno', 'numerical', 'integerOnly'=>true),
			array('NombreApellido', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Id, IdCuaderno, NombreApellido', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'equipos' => array(self::HAS_MANY, 'Equipo', 'IdTractorista'),
			'idCuaderno' => array(self::BELONGS_TO, 'Cuaderno', 'IdCuaderno'),
			'tratamientoFitosanitarios' => array(self::HAS_MANY, 'TratamientoFitosanitario', 'IdAplicador'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'IdCuaderno' => 'Cuaderno',
			'NombreApellido' => 'Tractorista',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Id',$this->Id);
		$criteria->compare('IdCuaderno',$this->IdCuaderno);
		$criteria->compare('NombreApellido',$this->NombreApellido,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tractorista the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
