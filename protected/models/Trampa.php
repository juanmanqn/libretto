<?php

/**
 * This is the model class for table "Trampa".
 *
 * The followings are the available columns in table 'Trampa':
 * @property integer $Id
 * @property integer $IdCuadro
 * @property string $Numero
 *
 * The followings are the available model relations:
 * @property CapturaCarpocapsa[] $capturaCarpocapsas
 * @property Cuadro $idCuadro
 */
class Trampa extends CActiveRecord
{
    public $idCuaderno;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Trampa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('IdCuadro, Numero', 'required'),
			array('IdCuadro', 'numerical', 'integerOnly'=>true),
			array('Numero', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Id, IdCuadro, Numero', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'capturaCarpocapsas' => array(self::HAS_MANY, 'CapturaCarpocapsa', 'IdTrampa'),
			'idCuadro' => array(self::BELONGS_TO, 'Cuadro', 'IdCuadro'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'IdCuadro' => 'Id Cuadro',
			'Numero' => 'Numero',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                $criteria->with=array('idCuadro','idCuadro.idUmi');
		$criteria->compare('Id',$this->Id);
		$criteria->compare('IdCuadro',$this->IdCuadro);
		$criteria->compare('Numero',$this->Numero,true);
                $criteria->compare('idUmi.IdCuaderno',$this->idCuaderno);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Trampa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
