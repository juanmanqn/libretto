<?php

/**
 * This is the model class for table "VisitaInspeccion".
 *
 * The followings are the available columns in table 'VisitaInspeccion':
 * @property integer $Id
 * @property integer $IdCuaderno
 * @property integer $IdInspector
 * @property string $NombreApellido
 * @property string $Legajo
 * @property string $Fecha
 * @property string $Observaciones
 *
 * The followings are the available model relations:
 * @property Cuaderno $idCuaderno
 */
class VisitaInspeccion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'VisitaInspeccion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('IdCuaderno, NombreApellido, Legajo', 'required'),
			array('IdCuaderno, IdInspector', 'numerical', 'integerOnly'=>true),
			array('NombreApellido, Legajo', 'length', 'max'=>255),
			array('Fecha, Observaciones', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Id, IdCuaderno, IdInspector, NombreApellido, Legajo, Fecha, Observaciones', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idCuaderno' => array(self::BELONGS_TO, 'Cuaderno', 'IdCuaderno'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'IdCuaderno' => 'Id Cuaderno',
			'IdInspector' => 'Id Inspector',
			'NombreApellido' => 'Nombre Apellido',
			'Legajo' => 'Legajo',
			'Fecha' => 'Fecha',
			'Observaciones' => 'Observaciones',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Id',$this->Id);
		$criteria->compare('IdCuaderno',$this->IdCuaderno);
		$criteria->compare('IdInspector',$this->IdInspector);
		$criteria->compare('NombreApellido',$this->NombreApellido,true);
		$criteria->compare('Legajo',$this->Legajo,true);
		$criteria->compare('Fecha',$this->Fecha,true);
		$criteria->compare('Observaciones',$this->Observaciones,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VisitaInspeccion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
