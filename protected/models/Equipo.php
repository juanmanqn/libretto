<?php

/**
 * This is the model class for table "Equipo".
 *
 * The followings are the available columns in table 'Equipo':
 * @property integer $Id
 * @property integer $IdCuaderno
 * @property string $NumeroEquipo
 * @property string $TractorMarca
 * @property string $TractorModelo
 * @property integer $TractorAnio
 * @property integer $TractorPotencia
 * @property string $PulverizadoraMarca
 * @property string $PulverizadoraModelo
 * @property string $PulverizadoraAnio
 * @property integer $PulverizadoraVolumen
 * @property integer $IdTractorista
 *
 * The followings are the available model relations:
 * @property Cuaderno $idCuaderno
 * @property Tractorista $idTractorista
 * @property LaborCultural[] $laborCulturals
 * @property TratamientoFitosanitario[] $tratamientoFitosanitarios
 */
class Equipo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Equipo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('IdCuaderno, NumeroEquipo, TractorAnio, PulverizadoraAnio, PulverizadoraVolumen, IdTractorista', 'required'),
			array('IdCuaderno, TractorAnio, TractorPotencia, PulverizadoraVolumen, IdTractorista', 'numerical', 'integerOnly'=>true),
			array('NumeroEquipo, TractorMarca, TractorModelo, PulverizadoraMarca, PulverizadoraModelo, PulverizadoraAnio', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Id, IdCuaderno, NumeroEquipo, TractorMarca, TractorModelo, TractorAnio, TractorPotencia, PulverizadoraMarca, PulverizadoraModelo, PulverizadoraAnio, PulverizadoraVolumen, IdTractorista', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idCuaderno' => array(self::BELONGS_TO, 'Cuaderno', 'IdCuaderno'),
			'idTractorista' => array(self::BELONGS_TO, 'Tractorista', 'IdTractorista'),
			'laborCulturals' => array(self::HAS_MANY, 'LaborCultural', 'IdEquipo'),
			'tratamientoFitosanitarios' => array(self::HAS_MANY, 'TratamientoFitosanitario', 'IdEquipo'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'IdCuaderno' => 'Cuaderno',
			'NumeroEquipo' => 'Nombre Equipo',
			'TractorMarca' => 'Tractor Marca',
			'TractorModelo' => 'Tractor Modelo (Versión)',
			'TractorAnio' => 'Tractor Año',
			'TractorPotencia' => 'Tractor Potencia',
			'PulverizadoraMarca' => 'Pulverizadora Marca',
			'PulverizadoraModelo' => 'Pulverizadora Modelo (Versión)',
			'PulverizadoraAnio' => 'Pulverizadora Año',
			'PulverizadoraVolumen' => 'Pulverizadora Volumen',
			'IdTractorista' => 'Tractorista',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Id',$this->Id);
		$criteria->compare('IdCuaderno',$this->IdCuaderno);
		$criteria->compare('NumeroEquipo',$this->NumeroEquipo,true);
		$criteria->compare('TractorMarca',$this->TractorMarca,true);
		$criteria->compare('TractorModelo',$this->TractorModelo,true);
		$criteria->compare('TractorAnio',$this->TractorAnio);
		$criteria->compare('TractorPotencia',$this->TractorPotencia);
		$criteria->compare('PulverizadoraMarca',$this->PulverizadoraMarca,true);
		$criteria->compare('PulverizadoraModelo',$this->PulverizadoraModelo,true);
		$criteria->compare('PulverizadoraAnio',$this->PulverizadoraAnio,true);
		$criteria->compare('PulverizadoraVolumen',$this->PulverizadoraVolumen);
		$criteria->compare('IdTractorista',$this->IdTractorista);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Equipo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
