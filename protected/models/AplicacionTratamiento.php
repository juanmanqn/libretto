<?php

/**
 * This is the model class for table "AplicacionTratamiento".
 *
 * The followings are the available columns in table 'AplicacionTratamiento':
 * @property integer $Id
 * @property integer $IdCuadro
 * @property integer $IdTratamientoFitosanitario
 * @property string $FechaHoraInicio
 * @property string $FechaHoraFin
 * @property integer $LitrosCuadro
 * @property string $FechaCumplimientoTC
 * @property string $Clima
 *
 * The followings are the available model relations:
 * @property Cuadro $idCuadro
 * @property TratamientoFitosanitario $idTratamientoFitosanitario
 */
class AplicacionTratamiento extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'AplicacionTratamiento';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('IdCuadro, IdTratamientoFitosanitario, FechaHoraInicio, FechaHoraFin, LitrosCuadro', 'required'),
			array('IdCuadro, IdTratamientoFitosanitario, LitrosCuadro', 'numerical', 'integerOnly'=>true),
			array('Clima', 'length', 'max'=>255),
			array('FechaCumplimientoTC', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Id, IdCuadro, IdTratamientoFitosanitario, FechaHoraInicio, FechaHoraFin, LitrosCuadro, FechaCumplimientoTC, Clima', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idCuadro' => array(self::BELONGS_TO, 'Cuadro', 'IdCuadro'),
			'idTratamientoFitosanitario' => array(self::BELONGS_TO, 'TratamientoFitosanitario', 'IdTratamientoFitosanitario'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'IdCuadro' => 'Id Cuadro',
			'IdTratamientoFitosanitario' => 'Id Tratamiento Fitosanitario',
			'FechaHoraInicio' => 'Fecha Hora Inicio',
			'FechaHoraFin' => 'Fecha Hora Fin',
			'LitrosCuadro' => 'Litros Cuadro',
			'FechaCumplimientoTC' => 'Fecha Cumplimiento Tc',
			'Clima' => 'Clima',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Id',$this->Id);
		$criteria->compare('IdCuadro',$this->IdCuadro);
		$criteria->compare('IdTratamientoFitosanitario',$this->IdTratamientoFitosanitario);
		$criteria->compare('FechaHoraInicio',$this->FechaHoraInicio,true);
		$criteria->compare('FechaHoraFin',$this->FechaHoraFin,true);
		$criteria->compare('LitrosCuadro',$this->LitrosCuadro);
		$criteria->compare('FechaCumplimientoTC',$this->FechaCumplimientoTC,true);
		$criteria->compare('Clima',$this->Clima,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AplicacionTratamiento the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
