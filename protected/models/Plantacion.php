<?php

/**
 * This is the model class for table "Plantacion".
 *
 * The followings are the available columns in table 'Plantacion':
 * @property integer $Id
 * @property integer $IdCuadro
 * @property integer $IdTipoProduccion
 * @property integer $IdVariedad
 * @property integer $IdPie
 * @property integer $IdCond
 * @property integer $Anio
 * @property string $DFilas
 * @property string $DPlantas
 * @property integer $NumeroPlantas
 * @property string $SuperficieNeta
 * @property string $Catastro
 * @property double $Latitud
 * @property double $Longitud
 *
 * The followings are the available model relations:
 * @property Cuadro $idCuadro
 * @property Variedad $idVariedad
 * @property PortaInjertos $idPie
 * @property SistemaConduccion $idCond
 * @property TipoProduccion $idTipoProduccion
 */
class Plantacion extends CActiveRecord
{
    public $idCuaderno;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Plantacion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('IdCuadro, IdTipoProduccion, IdVariedad, IdPie, IdCond, Anio, DFilas, DPlantas, NumeroPlantas, SuperficieNeta, Catastro', 'required'),
			array('IdCuadro, IdTipoProduccion, IdVariedad, IdPie, IdCond, Anio, NumeroPlantas', 'numerical', 'integerOnly'=>true),
			array('Latitud, Longitud', 'numerical'),
			array('DFilas, DPlantas, SuperficieNeta', 'length', 'max'=>10),
			array('Catastro', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Id, IdCuadro, IdTipoProduccion, IdVariedad, IdPie, IdCond, Anio, DFilas, DPlantas, NumeroPlantas, SuperficieNeta, Catastro, Latitud, Longitud', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idCuadro' => array(self::BELONGS_TO, 'Cuadro', 'IdCuadro'),
			'idVariedad' => array(self::BELONGS_TO, 'Variedad', 'IdVariedad'),
			'idPie' => array(self::BELONGS_TO, 'PortaInjertos', 'IdPie'),
			'idCond' => array(self::BELONGS_TO, 'SistemaConduccion', 'IdCond'),
			'idTipoProduccion' => array(self::BELONGS_TO, 'TipoProduccion', 'IdTipoProduccion'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'IdCuadro' => 'Cuadro',
			'IdTipoProduccion' => 'Tipo Produccion',
			'IdVariedad' => 'Variedad',
			'IdPie' => 'Pie',
			'IdCond' => 'Sistema Conducción',
			'Anio' => 'Año',
			'DFilas' => 'D/filas',
			'DPlantas' => 'D/plantas',
			'NumeroPlantas' => 'N° Plantas',
			'SuperficieNeta' => 'Sup. Neta',
			'Catastro' => 'Catastro',
			'Latitud' => 'Latitud',
			'Longitud' => 'Longitud',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                
                $criteria->with=array('idCuadro','idCuadro.idUmi');

		$criteria->compare('Id',$this->Id);
		$criteria->compare('IdCuadro',$this->IdCuadro);
		$criteria->compare('IdTipoProduccion',$this->IdTipoProduccion);
		$criteria->compare('IdVariedad',$this->IdVariedad);
		$criteria->compare('IdPie',$this->IdPie);
		$criteria->compare('IdCond',$this->IdCond);
		$criteria->compare('Anio',$this->Anio);
		$criteria->compare('DFilas',$this->DFilas,true);
		$criteria->compare('DPlantas',$this->DPlantas,true);
		$criteria->compare('NumeroPlantas',$this->NumeroPlantas);
		$criteria->compare('SuperficieNeta',$this->SuperficieNeta,true);
		$criteria->compare('Catastro',$this->Catastro,true);
		$criteria->compare('Latitud',$this->Latitud);
		$criteria->compare('Longitud',$this->Longitud);
                $criteria->compare('idUmi.IdCuaderno',$this->idCuaderno);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Plantacion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
