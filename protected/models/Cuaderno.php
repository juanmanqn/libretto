<?php

/**
 * This is the model class for table "Cuaderno".
 *
 * The followings are the available columns in table 'Cuaderno':
 * @property integer $Id
 * @property integer $IdExplotacion
 * @property string $NumeroRenspa
 * @property integer $NumeroCuaderno
 * @property string $Temporada
 * @property string $NombreChacra
 * @property string $NumeroInterno
 * @property string $SuperficieTotal
 * @property string $SuperficieNeta
 * @property integer $IdProductor
 * @property string $ProductorNombreApellido
 * @property string $ProductorEmail
 * @property string $ProductorDireccion
 * @property integer $ProductorIdLocalidad
 * @property string $ProductorTelefono
 * @property integer $IdProfesional
 * @property string $ProfesionalNombreApellido
 * @property string $ProfesionalMatricula
 * @property string $ProfesionalEmail
 * @property string $ProfesionalDireccion
 * @property integer $ProfesionalIdLocalidad
 * @property string $ProfesionalTelefono
 * @property integer $IdRecorredor
 * @property string $RecorredorNombreApellido
 * @property string $RecorredorTelefono
 * @property string $UbicacionUmis
 * @property string $UbicacionHuerto
 * @property integer $Estado
 *
 * The followings are the available model relations:
 * @property Explotacion $idExplotacion
 * @property Destino[] $destinos
 * @property Equipo[] $equipos
 * @property SalidaFruta[] $salidaFrutas
 * @property Tractorista[] $tractoristas
 * @property Umi[] $umis
 * @property VisitaInspeccion[] $visitaInspeccions
 * @property Permisos[] $permisos
 */
class Cuaderno extends CActiveRecord
{
    public $productor;
    public $chacra;
    
    
    public function usuarioVinculado() {
        if (!Yii::app()->user->checkAccess('usuarioAsignadoCuaderno', array('cuaderno' => $this))) {
            throw new CHttpException('404', 'El usuario no esta vinculado al Proyecto');
        }
    }
    
    
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Cuaderno';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('IdExplotacion, NumeroRenspa, Temporada, NombreChacra, IdProductor, ProductorNombreApellido, ProductorIdLocalidad, Estado', 'required'),
			array('IdExplotacion, NumeroCuaderno, IdProductor, ProductorIdLocalidad, IdProfesional, ProfesionalIdLocalidad, IdRecorredor, Estado', 'numerical', 'integerOnly'=>true),
			array('NumeroRenspa, NombreChacra, NumeroInterno', 'length', 'max'=>100),
			array('Temporada, ProductorTelefono, ProfesionalTelefono, RecorredorTelefono', 'length', 'max'=>45),
			array('SuperficieTotal, SuperficieNeta', 'length', 'max'=>10),
			array('ProductorNombreApellido, ProductorEmail, ProductorDireccion, ProfesionalNombreApellido, ProfesionalMatricula, ProfesionalEmail, ProfesionalDireccion, RecorredorNombreApellido, UbicacionUmis, UbicacionHuerto', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Id, IdExplotacion, NumeroRenspa, NumeroCuaderno, Temporada, NombreChacra, NumeroInterno, SuperficieTotal, SuperficieNeta, IdProductor, ProductorNombreApellido, ProductorEmail, ProductorDireccion, ProductorIdLocalidad, ProductorTelefono, IdProfesional, ProfesionalNombreApellido, ProfesionalMatricula, ProfesionalEmail, ProfesionalDireccion, ProfesionalIdLocalidad, ProfesionalTelefono, IdRecorredor, RecorredorNombreApellido, RecorredorTelefono, UbicacionUmis, UbicacionHuerto, Estado, productor, chacra', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idExplotacion' => array(self::BELONGS_TO, 'Explotacion', 'IdExplotacion'),
			'destinos' => array(self::HAS_MANY, 'Destino', 'IdCuaderno'),
			'equipos' => array(self::HAS_MANY, 'Equipo', 'IdCuaderno'),
                        'permisos' => array(self::HAS_MANY, 'Permisos', 'IdCuaderno'),
			'registrosFenologicoses' => array(self::HAS_MANY, 'RegistrosFenologicos', 'IdCuaderno'),
			'salidaFrutas' => array(self::HAS_MANY, 'SalidaFruta', 'IdCuaderno'),
			'tipoLabors' => array(self::HAS_MANY, 'TipoLabor', 'IdCuaderno'),
			'tractoristas' => array(self::HAS_MANY, 'Tractorista', 'IdCuaderno'),
			'tratamientos' => array(self::HAS_MANY, 'Tratamiento', 'IdCuaderno'),
			'tratamientoFitosanitarios' => array(self::HAS_MANY, 'TratamientoFitosanitario', 'IdCuaderno'),
                    
                    
			'umis' => array(self::HAS_MANY, 'Umi', 'IdCuaderno'),
			'visitaInspeccions' => array(self::HAS_MANY, 'VisitaInspeccion', 'IdCuaderno'),
                    
                        
                    
                    
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'IdExplotacion' => 'Id Explotacion',
			'NumeroRenspa' => 'Numero Renspa',
			'NumeroCuaderno' => 'Numero Cuaderno',
			'Temporada' => 'Temporada',
			'NombreChacra' => 'Nombre Chacra',
			'NumeroInterno' => 'Numero Interno',
			'SuperficieTotal' => 'Superficie Total',
			'SuperficieNeta' => 'Superficie Neta',
			'IdProductor' => 'Id Productor',
			'ProductorNombreApellido' => 'Productor Nombre Apellido',
			'ProductorEmail' => 'Productor Email',
			'ProductorDireccion' => 'Productor Direccion',
			'ProductorIdLocalidad' => 'Productor Id Localidad',
			'ProductorTelefono' => 'Productor Telefono',
			'IdProfesional' => 'Id Profesional',
			'ProfesionalNombreApellido' => 'Profesional Nombre Apellido',
			'ProfesionalMatricula' => 'Profesional Matricula',
			'ProfesionalEmail' => 'Profesional Email',
			'ProfesionalDireccion' => 'Profesional Direccion',
			'ProfesionalIdLocalidad' => 'Profesional Id Localidad',
			'ProfesionalTelefono' => 'Profesional Telefono',
			'IdRecorredor' => 'Id Recorredor',
			'RecorredorNombreApellido' => 'Recorredor Nombre Apellido',
			'RecorredorTelefono' => 'Recorredor Telefono',
			'UbicacionUmis' => 'Ubicacion Umis',
			'UbicacionHuerto' => 'Ubicacion Huerto',
			'Estado' => 'Estado',
                        'productor'=>'Productor'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                $criteria->with=array('idExplotacion.idProductor.id');
		$criteria->compare('Id',$this->Id);
		$criteria->compare('IdExplotacion',$this->IdExplotacion);
		$criteria->compare('NumeroRenspa',$this->NumeroRenspa,true);
		$criteria->compare('NumeroCuaderno',$this->NumeroCuaderno);
		$criteria->compare('Temporada',$this->Temporada,true);
		$criteria->compare('NombreChacra',$this->NombreChacra,true);
		$criteria->compare('NumeroInterno',$this->NumeroInterno,true);
		$criteria->compare('SuperficieTotal',$this->SuperficieTotal,true);
		$criteria->compare('SuperficieNeta',$this->SuperficieNeta,true);
		$criteria->compare('IdProductor',$this->IdProductor);
		$criteria->compare('ProductorNombreApellido',$this->ProductorNombreApellido,true);
		$criteria->compare('ProductorEmail',$this->ProductorEmail,true);
		$criteria->compare('ProductorDireccion',$this->ProductorDireccion,true);
		$criteria->compare('ProductorIdLocalidad',$this->ProductorIdLocalidad);
		$criteria->compare('ProductorTelefono',$this->ProductorTelefono,true);
		$criteria->compare('IdProfesional',$this->IdProfesional);
		$criteria->compare('ProfesionalNombreApellido',$this->ProfesionalNombreApellido,true);
		$criteria->compare('ProfesionalMatricula',$this->ProfesionalMatricula,true);
		$criteria->compare('ProfesionalEmail',$this->ProfesionalEmail,true);
		$criteria->compare('ProfesionalDireccion',$this->ProfesionalDireccion,true);
		$criteria->compare('ProfesionalIdLocalidad',$this->ProfesionalIdLocalidad);
		$criteria->compare('ProfesionalTelefono',$this->ProfesionalTelefono,true);
		$criteria->compare('IdRecorredor',$this->IdRecorredor);
		$criteria->compare('RecorredorNombreApellido',$this->RecorredorNombreApellido,true);
		$criteria->compare('RecorredorTelefono',$this->RecorredorTelefono,true);
		$criteria->compare('UbicacionUmis',$this->UbicacionUmis,true);
		$criteria->compare('UbicacionHuerto',$this->UbicacionHuerto,true);
		$criteria->compare('Estado',$this->Estado);
                $criteria->compare('id.NombreApellido',$this->productor,true);
                $criteria->compare('idExplotacion.NombreChacra',$this->chacra,true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Cuaderno the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
