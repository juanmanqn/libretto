<?php

/**
 * This is the model class for table "Explotacion".
 *
 * The followings are the available columns in table 'Explotacion':
 * @property integer $Id
 * @property integer $IdProductor
 * @property string $NombreChacra
 * @property integer $IdLocalidad
 * @property string $Dirección
 *
 * The followings are the available model relations:
 * @property Cuaderno[] $cuadernos
 * @property Productor $idProductor
 * @property Localidad $idLocalidad
 */
class Explotacion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Explotacion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('IdProductor, NombreChacra, IdLocalidad, Direccion', 'required'),
			array('IdProductor, IdLocalidad', 'numerical', 'integerOnly'=>true),
			array('NombreChacra, Direccion', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Id, IdProductor, NombreChacra, IdLocalidad, Direccion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cuadernos' => array(self::HAS_MANY, 'Cuaderno', 'IdExplotacion'),
			'idProductor' => array(self::BELONGS_TO, 'Productor', 'IdProductor'),
			'idLocalidad' => array(self::BELONGS_TO, 'Localidad', 'IdLocalidad'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'IdProductor' => 'Productor',
			'NombreChacra' => 'Nombre Chacra',
			'IdLocalidad' => 'Localidad',
			'Direccion' => 'Direccion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Id',$this->Id);
		$criteria->compare('IdProductor',$this->IdProductor);
		$criteria->compare('NombreChacra',$this->NombreChacra,true);
		$criteria->compare('IdLocalidad',$this->IdLocalidad);
		$criteria->compare('Direccion',$this->Direccion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Explotacion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
