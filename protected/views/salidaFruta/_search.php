<?php
/* @var $this SalidaFrutaController */
/* @var $model SalidaFruta */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'Id'); ?>
		<?php echo $form->textField($model,'Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdCuaderno'); ?>
		<?php echo $form->textField($model,'IdCuaderno'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdUmi'); ?>
		<?php echo $form->textField($model,'IdUmi'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Fecha'); ?>
		<?php echo $form->textField($model,'Fecha'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdVariedad'); ?>
		<?php echo $form->textField($model,'IdVariedad'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'CantidadBins'); ?>
		<?php echo $form->textField($model,'CantidadBins'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NumeroRemito'); ?>
		<?php echo $form->textField($model,'NumeroRemito',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdDestino'); ?>
		<?php echo $form->textField($model,'IdDestino'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Observaciones'); ?>
		<?php echo $form->textField($model,'Observaciones',array('size'=>60,'maxlength'=>1000)); ?>
	</div>

	<div class="row buttons">
		<?php echo TbHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->