<?php

ob_start();
$this->widget('ext.PdfGrid.EPDFGrid', array(
    'id'        => 'salidas-pdf',
    'fileName'  => 'SalidasFrutas',//Nombre del archivo generado sin la extension pdf (.pdf)
    'dataProvider'  => $model->search(), //puede ser $model->search()
    'columns'   => array(     
        'Id',
        //'IdCuaderno',
        'idUmi.Codigo',
        'Fecha',
        'idVariedad.Codigo',
        'CantidadBins',
        'Kilos',
        array('header'=>'Superficie Ha',
                'value'=>'$data->idUmi->getSuperficeVariedad($data->IdVariedad)'),
        array('header'=>'Rinde Kg/m2',
            'value'=>'$data->getRindeKgM2()'),
        'NumeroRemito',
        'idDestino.Destino',
    ),
    'config'    => array(
        'title'     => 'Salidas Frutas',
        'subTitle'  => 'Cuaderno: '.$cuaderno->NumeroRenspa,
        //'colWidths' => array(40, 90, 40, 70),
    ),
));   
ob_end_flush(); 

