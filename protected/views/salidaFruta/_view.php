<?php
/* @var $this SalidaFrutaController */
/* @var $data SalidaFruta */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Id), array('view', 'id'=>$data->Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdCuaderno')); ?>:</b>
	<?php echo CHtml::encode($data->IdCuaderno); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdUmi')); ?>:</b>
	<?php echo CHtml::encode($data->IdUmi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Fecha')); ?>:</b>
	<?php echo CHtml::encode($data->Fecha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdVariedad')); ?>:</b>
	<?php echo CHtml::encode($data->IdVariedad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CantidadBins')); ?>:</b>
	<?php echo CHtml::encode($data->CantidadBins); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NumeroRemito')); ?>:</b>
	<?php echo CHtml::encode($data->NumeroRemito); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('IdDestino')); ?>:</b>
	<?php echo CHtml::encode($data->IdDestino); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Observaciones')); ?>:</b>
	<?php echo CHtml::encode($data->Observaciones); ?>
	<br />

	*/ ?>

</div>