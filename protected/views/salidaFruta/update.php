<?php
/* @var $this SalidaFrutaController */
/* @var $model SalidaFruta */

$this->breadcrumbs=array(
        $cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Salidas Fruta'=>array('salidaFruta/admin','idCuaderno'=>$cuaderno->Id),
	'Modificar',
);

?>

<h1>Modificar Salida de Fruta <?php echo $model->Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,'cuaderno'=>$cuaderno)); ?>