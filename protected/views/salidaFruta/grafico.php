<?php
/* @var $this SalidaFrutaController */
/* @var $model SalidaFruta */

$this->breadcrumbs=array(	
	$cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Salidas Fruta',
);

$this->menu=array(	
	array('label'=>'Nueva Salida Fruta', 'url'=>array('create', 'idCuaderno'=> $cuaderno->Id)),
        array('label'=>'Listado', 'url'=>array('admin', 'idCuaderno'=> $cuaderno->Id)),
);

$this->idCuaderno = $cuaderno->Id;
?>

<h1>Salida Frutas</h1>

<?php
//$model->idUmi->getSuperficeVariedad($model->IdVariedad);



$this->Widget('ext.highcharts.highcharts.HighchartsWidget', array(
   'options'=>array(
      'chart' => array('type'=>'column'),
      'title' => array('text' => 'Salidas de Frutas (Rinde kg/Ha)'),
      'xAxis' => array(
         'categories' => array_values($umis), //array('78X','78Y','78W'),
             //'14th','15th','16th','17th','18th','19th','20th','21th','22th','23th','24th','25th','26th','27th','28th')
      ),
      'yAxis' => array(
         'title' => array('text' => 'Rinde')
      ),
      'credits' => array('enabled' => false),
      'series' => array_values($variedades)
//        array(
//          array('name' => 'Variedad1', 'data' => array(20, 25, 25)),
//          array('name' => 'Variedad2', 'data' => array(15, 17, 14)),
//          array('name' => 'Variedad3', 'data' => array(5, 7, 8)),
//          array('name' => 'Variedad4', 'data' => array(25, 27, 23)),
//       )
   )
));
