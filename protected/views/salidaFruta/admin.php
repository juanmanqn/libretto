<?php
/* @var $this SalidaFrutaController */
/* @var $model SalidaFruta */

$this->breadcrumbs=array(	
	$cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Salidas Fruta',
);

$this->menu=array(	
	array('label'=>'Nueva Salida Fruta', 'url'=>array('create', 'idCuaderno'=> $cuaderno->Id)),
        array('label'=>'Grafico', 'url'=>array('grafico', 'idCuaderno'=> $cuaderno->Id)),
        array('label'=>'Imprimir', 'url'=>array('imprimir', 'idCuaderno'=> $cuaderno->Id)),
);

$this->idCuaderno = $cuaderno->Id;
?>

<h1>Salida Frutas</h1>

<?php
//$model->idUmi->getSuperficeVariedad($model->IdVariedad);
$this->widget('bootstrap.widgets.TbGridView', array(
        'type' => TbHtml::GRID_TYPE_STRIPED,
	'id'=>'salida-fruta-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'Id',
		//'IdCuaderno',
		'idUmi.Codigo',
		'Fecha',
		'idVariedad.Codigo',
		'CantidadBins',
                'Kilos',
                array('header'=>'Superficie Ha',
                        'value'=>'$data->idUmi->getSuperficeVariedad($data->IdVariedad)'),
                array('header'=>'Rinde Kg/Ha',
                    'value'=>'$data->getRindeKgHa()'),
		'NumeroRemito',
		'idDestino.Destino',
		//'Observaciones',		
		array(
			'class'=>'TbButtonColumn',
		),
	),
)); 

