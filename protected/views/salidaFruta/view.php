<?php
/* @var $this SalidaFrutaController */
/* @var $model SalidaFruta */

$this->breadcrumbs=array(
	$cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Salidas Fruta'=>array('salidaFruta/admin','idCuaderno'=>$cuaderno->Id),
	'Modificar #'.$model->Id,
);

?>

<h1>SalidaFruta #<?php echo $model->Id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'idCuaderno.NumeroRenspa',
		'idUmi.Codigo',
		'Fecha',
		'idVariedad.Codigo',
		'CantidadBins',
                'Kilos',
                array('label'=>'Superficie Ha',
                        'value'=>$model->idUmi->getSuperficeVariedad($model->IdVariedad)),
                array('label'=>'Rinde Kg/Ha',
                    'value'=>$model->getRindeKgHa()),
		'NumeroRemito',
		'idDestino.Destino',
		'Observaciones',
	),
)); ?>
