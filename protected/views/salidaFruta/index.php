<?php
/* @var $this SalidaFrutaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Salida Frutas',
);

$this->menu=array(
	array('label'=>'Nuevo SalidaFruta', 'url'=>array('create')),
	array('label'=>'Listar SalidaFruta', 'url'=>array('admin')),
);
?>

<h1>Salida Frutas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
