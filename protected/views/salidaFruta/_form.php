<?php
/* @var $this SalidaFrutaController */
/* @var $model SalidaFruta */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'salida-fruta-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son obligatorios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
                <?php echo $form->labelEx($model,'IdUmi'); ?>
		<?php echo $form->dropDownList($model,'IdUmi',CHtml::listData(Umi::model()->findAll('IdCuaderno=:IdCuaderno',
                        array(':IdCuaderno'=>$cuaderno->Id)), 'Id', 'Codigo')); ?>
		<?php echo $form->error($model,'IdUmi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Fecha'); ?>
		<?php
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model' => $model,
                        'attribute' => 'Fecha',
                        'language' => 'es', 
                        'options' => array(
                            'dateFormat' => 'yy-mm-dd',     // format of "2012-12-25" 
                        ),
                    ));
                ?>   
		<?php echo $form->error($model,'Fecha'); ?>
	</div>

	<div class="row">
            <?php 
                if ($model->isNewRecord){
                    $especieId = 0;
                }
                else {
                    $especieId = $model->idVariedad->IdEspecie;
                }
            ?>
            <?php echo $form->labelEx($model, 'Especie'); ?>
            <?php $items2 = CHtml::listData(Especie::model()->findAll(), 'Id', 'Nombre'); ?>
            <?php echo CHtml::DropDownList('idEspecie',$especieId, $items2, array('id' => 'IdEspecie', 'prompt' => 'Seleccione una opción'));?>            
        </div>        
        
	<div class="row">
                <?php 
                    if ($model->isNewRecord){
                        $arrayVariedad = array();
                    }
                    else {
                        $arrayVariedad = CHtml::listData(Variedad::model()->findAll('IdEspecie=:IdEspecie',
                                array(':IdEspecie'=>$model->idVariedad->IdEspecie)),'Id','Nombre');
                    }
                ?>
		<?php echo $form->labelEx($model,'IdVariedad'); ?>
		<?php echo $form->dropDownList($model,'IdVariedad', $arrayVariedad, array('empty'=>'Seleccionar..')); ?>
                <?php ECascadeDropDown::master('IdEspecie')->setDependent('SalidaFruta_IdVariedad', array('dependentLoadingLabel' => 'Cargando Variedades...'), '/variedad/data'); ?>
		<?php echo $form->error($model,'IdVariedad'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'CantidadBins'); ?>
		<?php echo $form->textField($model,'CantidadBins'); ?>
		<?php echo $form->error($model,'CantidadBins'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Kilos'); ?>
		<?php echo $form->textField($model,'Kilos'); ?>
		<?php echo $form->error($model,'Kilos'); ?>
	</div>        
        
	<div class="row">
		<?php echo $form->labelEx($model,'NumeroRemito'); ?>
		<?php echo $form->textField($model,'NumeroRemito',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'NumeroRemito'); ?>
	</div>

        <div class="row">                
                <?php echo $form->labelEx($model,'IdDestino'); ?>
		<?php echo $form->dropDownList($model,'IdDestino',CHtml::listData(Destino::model()->findAll('IdCuaderno=:IdCuaderno',
                        array(':IdCuaderno'=>$cuaderno->Id)), 'Id', 'Destino')); ?>
		<?php echo $form->error($model,'IdDestino'); ?>
	</div>
        
	<div class="row">
		<?php echo $form->labelEx($model,'Observaciones'); ?>
		<?php echo $form->textArea($model,'Observaciones',array('size'=>60,'maxlength'=>1000)); ?>
		<?php echo $form->error($model,'Observaciones'); ?>
	</div>

	<div class="row buttons">
		<?php echo TbHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->