<?php
/* @var $this ExplotacionController */
/* @var $data Explotacion */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Id), array('view', 'id'=>$data->Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdProductor')); ?>:</b>
	<?php echo CHtml::encode($data->IdProductor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NombreChacra')); ?>:</b>
	<?php echo CHtml::encode($data->NombreChacra); ?>
	<br />


</div>