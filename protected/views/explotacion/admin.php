<?php
/* @var $this ExplotacionController */
/* @var $model Explotacion */

$this->breadcrumbs=array(
	'Listar',
);

$this->menu=array(	
	array('label'=>'Nueva Explotacion', 'url'=>array('create')),
);

 if(!Yii::app()->user->checkAccess('administrador')):?>
<h1>Bienvenido Productor a <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>

    <p >
    Aplicación informática para el registro de la información de los cuadernos de campos.
</p>
<p>
    Esta herramienta permite a los productores mejorar el control de sus procesos productivos, contribuyendo a la gestión
    y planificación de sus establecimientos. Además, provee a los organismos públicos
    y privados, ya sean certificadores o fiscalizadores, el acceso a datos seguros y confiables.
</p>
 
<hr/>
<h2>Mis Explotaciones</h2>
<?php else:?>
<h2>Explotaciones</h2>
<?php endif;?>
<?php $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => TbHtml::GRID_TYPE_STRIPED,
	'id'=>'explotacion-grid',
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'columns'=>array(
		//'Id',
		array('name'=>'idProductor.id.NombreApellido',
                    'visible'=>Yii::app()->user->checkAccess('administrador')),
		'NombreChacra',
                'Direccion',
                array(
                    'name'=>'Provincia', 
                    'value'=>'$data->idLocalidad->idProvincia->Nombre' 
                ),
                array(
                    'name'=>'Localidad', 
                    'value'=>'$data->idLocalidad->Nombre' 
                ),
		array(
			'class'=>'TbButtonColumn',
		),
	),
)); ?>
