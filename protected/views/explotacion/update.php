<?php
/* @var $this ExplotacionController */
/* @var $model Explotacion */

$this->breadcrumbs=array(
	'Explotaciones'=>array('index'),
	$model->Id. ' '=>array('view','id'=>$model->Id),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Nueva Explotacion', 'url'=>array('create')),
	array('label'=>'Ver Explotacion', 'url'=>array('view', 'id'=>$model->Id)),
	array('label'=>'Listar Explotaciones', 'url'=>array('admin')),
);
?>

<h1>Modificar Explotacion <?php echo $model->Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>