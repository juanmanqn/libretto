<?php
/* @var $this ExplotacionController */
/* @var $model Explotacion */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'explotacion-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">Campos con <span class="required">*</span> son obligatorios.</p>

    <?php echo $form->errorSummary($model); ?>

        <?php if (Yii::app()->user->checkAccess('administrador')): ?>
        <div class="row">
            <?php echo $form->labelEx($model, 'IdProductor'); ?>
        <?php echo $form->textField($model, 'IdProductor'); ?>
        <?php echo $form->error($model, 'IdProductor'); ?>
        </div>
        <?php endif; ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'NombreChacra'); ?>
        <?php echo $form->textField($model, 'NombreChacra', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'NombreChacra'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model, 'Direccion'); ?>
        <?php echo $form->textField($model, 'Direccion', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'Direccion'); ?>
    </div>

    <div class="row">
        
          <?php 
                if ($model->isNewRecord){
                    $provinciaId = 0;
                }
                else {
                    $provinciaId = $model->idLocalidad->idProvincia;
                }
            ?>
        <?php echo $form->labelEx($model, 'Provincia'); ?>

        <?php
            $provinciaItems = CHtml::listData(Provincia::model()->findAll(), 'Id', 'Nombre');
            echo CHtml::DropDownList('IdProvincia', $provinciaId, $provinciaItems, array('id' => 'IdProvincia', 'prompt' => 'Seleccionar..'));
        ?>

    </div>

    <div class="row">
        <?php 
                if ($model->isNewRecord){
                    $itemsLocalidad = array();
                }
                else {
                    $itemsLocalidad = CHtml::listData(Localidad::model()->findAll('IdProvincia=:IdProvincia',array(':IdProvincia'=>$model->idLocalidad->IdProvincia)),'Id','Nombre');
                }
            ?>
        <?php echo $form->labelEx($model, 'Localidad'); ?>        
        <?php echo $form->dropDownList($model, 'IdLocalidad', $itemsLocalidad, array('empty'=>'Seleccionar..')); ?>
        <?php ECascadeDropDown::master('IdProvincia')->setDependent('Explotacion_IdLocalidad', array('dependentLoadingLabel' => 'Cargando Localidades...'), '/localidad/data'); ?>
        <?php echo $form->error($model, 'IdLocalidad'); ?>
    </div>

    <div class="row buttons">
    <?php echo TbHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->