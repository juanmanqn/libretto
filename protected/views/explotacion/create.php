<?php
/* @var $this ExplotacionController */
/* @var $model Explotacion */

$this->breadcrumbs=array(
	'Explotaciones'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Listar Explotaciones', 'url'=>array('admin')),
);
?>

<h1>Nueva Explotacion</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>