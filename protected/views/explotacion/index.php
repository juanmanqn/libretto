<?php
/* @var $this ExplotacionController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Explotaciones',
);

$this->menu=array(
	array('label'=>'Nueva Explotacion', 'url'=>array('create')),
	array('label'=>'Listar Explotaciones', 'url'=>array('admin')),
);
?>

<h1>Explotaciones</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
