<?php
/* @var $this UnidadDosisController */
/* @var $model UnidadDosis */

$this->breadcrumbs=array(
	'Unidad Dosises'=>array('index'),
	$model->Id. ' '=>array('view','id'=>$model->Id),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Nuevo UnidadDosis', 'url'=>array('create')),
	array('label'=>'Ver UnidadDosis', 'url'=>array('view', 'id'=>$model->Id)),
	array('label'=>'Listar UnidadDosis', 'url'=>array('admin')),
);
?>

<h1>Modificar UnidadDosis <?php echo $model->Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>