<?php
/* @var $this UnidadDosisController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Unidad Dosises',
);

$this->menu=array(
	array('label'=>'Nuevo UnidadDosis', 'url'=>array('create')),
	array('label'=>'Listar UnidadDosis', 'url'=>array('admin')),
);
?>

<h1>Unidad Dosises</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
