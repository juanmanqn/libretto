<?php
/* @var $this UnidadDosisController */
/* @var $model UnidadDosis */

$this->breadcrumbs=array(
	'Unidad Dosises'=>array('index'),
	$model->Id,
);

$this->menu=array(
	array('label'=>'Listar UnidadDosis', 'url'=>array('admin')),
	array('label'=>'Nuevo UnidadDosis', 'url'=>array('create')),
	array('label'=>'Modificar UnidadDosis', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Eliminar UnidadDosis', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Esta seguro de querer eliminar este item?')),
	);
?>

<h1>UnidadDosis #<?php echo $model->Id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'Unidad',
	),
)); ?>
