<?php
/* @var $this UnidadDosisController */
/* @var $data UnidadDosis */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Id), array('view', 'id'=>$data->Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Unidad')); ?>:</b>
	<?php echo CHtml::encode($data->Unidad); ?>
	<br />


</div>