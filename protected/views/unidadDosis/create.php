<?php
/* @var $this UnidadDosisController */
/* @var $model UnidadDosis */

$this->breadcrumbs=array(
	'Unidad Dosises'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Listar UnidadDosis', 'url'=>array('admin')),
);
?>

<h1>Nuevo UnidadDosis</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>