<?php
/* @var $this PlagaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Plagas',
);

$this->menu=array(
	array('label'=>'Nuevo Plaga', 'url'=>array('create')),
	array('label'=>'Listar Plaga', 'url'=>array('admin')),
);
?>

<h1>Plagas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
