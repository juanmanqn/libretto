<?php
/* @var $this PlagaController */
/* @var $data Plaga */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Id), array('view', 'id'=>$data->Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Nombre')); ?>:</b>
	<?php echo CHtml::encode($data->Nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Especie')); ?>:</b>
	<?php echo CHtml::encode($data->Especie); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Genero')); ?>:</b>
	<?php echo CHtml::encode($data->Genero); ?>
	<br />


</div>