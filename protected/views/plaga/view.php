<?php
/* @var $this PlagaController */
/* @var $model Plaga */

$this->breadcrumbs=array(
	'Plagas'=>array('index'),
	$model->Id,
);

$this->menu=array(
	array('label'=>'Listar Plaga', 'url'=>array('admin')),
	array('label'=>'Nuevo Plaga', 'url'=>array('create')),
	array('label'=>'Modificar Plaga', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Eliminar Plaga', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Esta seguro de querer eliminar este item?')),
	);
?>

<h1>Plaga #<?php echo $model->Id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'Nombre',
		'Especie',
		'Genero',
	),
)); ?>
