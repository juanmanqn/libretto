<?php
/* @var $this PlagaController */
/* @var $model Plaga */

$this->breadcrumbs=array(
	'Plagas'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Listar Plaga', 'url'=>array('admin')),
);
?>

<h1>Nuevo Plaga</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>