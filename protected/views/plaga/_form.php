<?php
/* @var $this PlagaController */
/* @var $model Plaga */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'plaga-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son obligatorios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'Nombre'); ?>
		<?php echo $form->textField($model,'Nombre',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'Nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Especie'); ?>
		<?php echo $form->textField($model,'Especie',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'Especie'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Genero'); ?>
		<?php echo $form->textField($model,'Genero',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'Genero'); ?>
	</div>

	<div class="row buttons">
		<?php echo TbHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->