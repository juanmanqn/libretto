<?php
/* @var $this PlagaController */
/* @var $model Plaga */

$this->breadcrumbs=array(
	'Plagas'=>array('index'),
	$model->Id. ' '=>array('view','id'=>$model->Id),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Nuevo Plaga', 'url'=>array('create')),
	array('label'=>'Ver Plaga', 'url'=>array('view', 'id'=>$model->Id)),
	array('label'=>'Listar Plaga', 'url'=>array('admin')),
);
?>

<h1>Modificar Plaga <?php echo $model->Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>