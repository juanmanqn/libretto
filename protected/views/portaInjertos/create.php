<?php
/* @var $this PortaInjertosController */
/* @var $model PortaInjertos */

$this->breadcrumbs=array(
	'Porta Injertoses'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Listar PortaInjertos', 'url'=>array('admin')),
);
?>

<h1>Nuevo PortaInjertos</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>