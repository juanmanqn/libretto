<?php
/* @var $this PortaInjertosController */
/* @var $model PortaInjertos */

$this->breadcrumbs=array(
	'Porta Injertoses'=>array('index'),
	$model->Id. ' '=>array('view','id'=>$model->Id),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Nuevo PortaInjertos', 'url'=>array('create')),
	array('label'=>'Ver PortaInjertos', 'url'=>array('view', 'id'=>$model->Id)),
	array('label'=>'Listar PortaInjertos', 'url'=>array('admin')),
);
?>

<h1>Modificar PortaInjertos <?php echo $model->Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>