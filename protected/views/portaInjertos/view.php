<?php
/* @var $this PortaInjertosController */
/* @var $model PortaInjertos */

$this->breadcrumbs=array(
	'Porta Injertoses'=>array('index'),
	$model->Id,
);

$this->menu=array(
	array('label'=>'Listar PortaInjertos', 'url'=>array('admin')),
	array('label'=>'Nuevo PortaInjertos', 'url'=>array('create')),
	array('label'=>'Modificar PortaInjertos', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Eliminar PortaInjertos', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Esta seguro de querer eliminar este item?')),
	);
?>

<h1>PortaInjertos #<?php echo $model->Id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'IdEspecie',
		'Nombre',
	),
)); ?>
