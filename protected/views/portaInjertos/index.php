<?php
/* @var $this PortaInjertosController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Porta Injertoses',
);

$this->menu=array(
	array('label'=>'Nuevo PortaInjertos', 'url'=>array('create')),
	array('label'=>'Listar PortaInjertos', 'url'=>array('admin')),
);
?>

<h1>Porta Injertoses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
