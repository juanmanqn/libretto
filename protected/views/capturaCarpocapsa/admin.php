<?php
/* @var $this CapturaCarpocapsaController */
/* @var $model CapturaCarpocapsa */
/* @var $cuaderno Cuaderno */

$this->breadcrumbs = array(
    $cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
    'RENSPA: ' . $cuaderno->NumeroRenspa => array('/cuaderno/view', 'id' => $cuaderno->Id),
    'Capturas y Trampas',
);


$this->menu = array(
    array('label' => 'Nueva Trampa', 'url' => array('/trampa/create', 'idCuaderno' => $cuaderno->Id)),
    array('label' => 'Nueva Captura', 'url' => array('create', 'idCuaderno' => $cuaderno->Id)),
    array('label'=>'Imprimir', 'url'=>array('imprimir', 'idCuaderno'=> $cuaderno->Id)),
);
$this->idCuaderno = $cuaderno->Id;
?>

<h1>Capturas Carpocapsa</h1>

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => TbHtml::GRID_TYPE_STRIPED,
    'id' => 'captura-carpocapsa-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'idTrampa.Numero',
        'idTrampa.idCuadro.NumeroCuadro',
        'Fecha',
        'Cantidad',
        'Detalle',
        array(
            'class' => 'TbButtonColumn',
            'template' => '{update}{delete}'
        ),
    ),
));
?>

<h1>Trampas</h1>

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => TbHtml::GRID_TYPE_STRIPED,
    'id' => 'trampa-grid',
    'dataProvider' => $trampas->search(),
    'filter' => $trampas,
    'columns' => array(
        'Numero',
        'idCuadro.NumeroCuadro',
        array(
            'class' => 'TbButtonColumn',
            'template' => '{update}{delete}',
            'buttons' => array(
                'update' => array(
                    'url' => 'Yii::app()->createUrl("/trampa/update",array("id"=>$data->Id))'
                ),
                'delete' => array(
                    'url' => 'Yii::app()->createUrl("/trampa/delete",array("id"=>$data->Id))'
                )
            )
        ),),
));
