<?php

ob_start();
$this->widget('ext.PdfGrid.EPDFGrid', array(
    'id'        => 'capturas-carpocapsa-pdf',
    'fileName'  => 'CapturasCarpocapsa',//Nombre del archivo generado sin la extension pdf (.pdf)
    'dataProvider'  => $model->search(), //puede ser $model->search()
    'columns'   => array(     
        'idTrampa.Numero',
        'idTrampa.idCuadro.NumeroCuadro',
        'Fecha',
        'Cantidad',
        'Detalle',
    ),
    'config'    => array(
        'title'     => 'Capturas Carpocapsa',
        'subTitle'  => 'Cuaderno: '.$cuaderno->NumeroRenspa,
        //'colWidths' => array(40, 90, 40, 70),
    ),
));   
ob_end_flush(); 

