<?php
/* @var $this CapturaCarpocapsaController */
/* @var $model CapturaCarpocapsa */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'captura-carpocapsa-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son obligatorios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'IdTrampa'); ?>
            
		<?php 
                $trampas=  Trampa::model()->with(array('idCuadro.idUmi'))->findAll('idUmi.IdCuaderno=:IdCuaderno', array(':IdCuaderno'=>$cuaderno->Id));
                $items=CHtml::listData($trampas, 'Id','Numero');
                echo $form->dropDownList($model,'IdTrampa',$items)   ?>
		<?php echo $form->error($model,'IdTrampa'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Fecha'); ?>
		<?php
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model' => $model,
                        'attribute' => 'Fecha',
                        'language' => 'es',                    
                    ));
                ?>   
		<?php echo $form->error($model,'Fecha'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'Cantidad'); ?>
		<?php echo $form->textField($model,'Cantidad',array('size'=>60)); ?>
		<?php echo $form->error($model,'Cantidad'); ?>
	</div>
        
	<div class="row">
		<?php echo $form->labelEx($model,'Detalle'); ?>
		<?php echo $form->textField($model,'Detalle',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'Detalle'); ?>
	</div>

	<div class="row buttons">
		<?php echo TbHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->