<?php
/* @var $this CapturaCarpocapsaController */
/* @var $model CapturaCarpocapsa */

$this->breadcrumbs=array(
	'Captura Carpocapsas'=>array('index'),
	$model->Id,
);

$this->menu=array(
	array('label'=>'Listar CapturaCarpocapsa', 'url'=>array('admin')),
	array('label'=>'Nuevo CapturaCarpocapsa', 'url'=>array('create')),
	array('label'=>'Modificar CapturaCarpocapsa', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Eliminar CapturaCarpocapsa', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Esta seguro de querer eliminar este item?')),
	);
?>

<h1>CapturaCarpocapsa #<?php echo $model->Id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'IdTrampa',
		'Fecha',
		'Detalle',
	),
)); ?>
