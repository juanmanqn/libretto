<?php
/* @var $this CapturaCarpocapsaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Captura Carpocapsas',
);

$this->menu=array(
	array('label'=>'Nuevo CapturaCarpocapsa', 'url'=>array('create')),
	array('label'=>'Listar CapturaCarpocapsa', 'url'=>array('admin')),
);
?>

<h1>Captura Carpocapsas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
