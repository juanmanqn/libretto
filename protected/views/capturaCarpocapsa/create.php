<?php
/* @var $this CapturaCarpocapsaController */
/* @var $model CapturaCarpocapsa */
/* @var $cuaderno Cuaderno */
$this->breadcrumbs=array(	
        $cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Capturas y Trampas' => array('/capturaCarpocapsa/admin','idCuaderno'=>$cuaderno->Id),
        'Nueva Captura'
);


?>

<h1>Nueva Captura Carpocapsa</h1>

<?php $this->renderPartial('_form', array('model'=>$model,'cuaderno'=>$cuaderno)); ?>