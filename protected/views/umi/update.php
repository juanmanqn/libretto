<?php
/* @var $this UmiController */
/* @var $model Umi */

$this->breadcrumbs=array(
        $cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
        'Umis' => array('umi/admin', 'idCuaderno'=> $cuaderno->Id),
	'Modificar #'.$model->Codigo,
);

?>

<h1>Modificar Umi <?php echo $model->Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>