<?php
/* @var $this UmiController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Umis',
);

$this->menu=array(
	array('label'=>'Nuevo Umi', 'url'=>array('create')),
	array('label'=>'Listar Umi', 'url'=>array('admin')),
);
?>

<h1>Umis</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
