<?php
/* @var $this UmiController */
/* @var $model Umi */

$this->breadcrumbs=array(	
        $cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'UMIs',
);

$this->menu=array(	
	array('label'=>'Nuevo UMI', 'url'=>array('create', 'idCuaderno'=> $cuaderno->Id)),
);

$this->idCuaderno = $cuaderno->Id;

?>

<h1>UMIs</h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => TbHtml::GRID_TYPE_STRIPED,
	'id'=>'umi-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'Id',
		//'IdCuaderno',
		'Codigo',
		'Descripcion',
		array(
			'class'=>'TbButtonColumn',
                        'template'=>'{update}{delete}',
		),
	),
)); ?>
