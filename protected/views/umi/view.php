<?php
/* @var $this UmiController */
/* @var $model Umi */

$this->breadcrumbs=array(
        $cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Umis'=>array('admin', 'idCuaderno' => $cuaderno->Id),
	$model->Codigo,
);

$this->menu=array(
	array('label'=>'Modificar Umi', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Eliminar Umi', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Esta seguro de querer eliminar este item?')),
	);
?>

<h1>Umi #<?php echo $model->Codigo; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'IdCuaderno',
		'Codigo',
		'Descripcion',
	),
)); ?>
