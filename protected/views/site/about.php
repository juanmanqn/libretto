<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Acerca de';
$this->breadcrumbs=array(
	'Acerca de',
);
?>
<h1>Acerca de</h1>

<p>Esta aplicación a sido desarrollada en el marco del proyecto: “Construcción conjunta de herramientas agromáticas de gestión para los productores frutícolas de los Valles de la Norpatagonia”,
ganador del segundo premio SENASA INVESTIGACION TRANSFERENCIA COMUNICACIÓN 2013</p>
<ul>
    <li>Responsable del Proyecto: Dr. Mario Leopoldo Leskovar</li>
    <li>Coodirectores: Dra. Patricia Villarreal, Lic. Guillermo Grosso</li>
    <li>Cordinadores: Dr. Darío Fernández, Ing. Agr. Gabriel Podgornik, Mag. María Laura Malaspina</li>
    <li>Participantes: Nájla Elisabeth Caixeta, María Beatriz Arce</li>
    <li>Desarrolladores: Lic. Pablo Kogan, An. Juan Manuel Relloso</li>
</ul>

<p>
    El presente proyecto contempla el desarrollo y transferencia de un software

que contribuya a modernizar el registro de datos y colabore en la toma de 

decisiones para productores primarios de frutas de los Valles de la Norpatagonia, 

con el fin de aportar en la mejora de la calidad. Se espera además que esta 

herramienta permita al productor conocer el costo de producción de cada uno de 

sus productos.
</p>
