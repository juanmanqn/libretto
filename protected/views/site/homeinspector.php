<?php
/* @var $this SiteController */
/* @var $model Cuaderno */

$this->pageTitle = Yii::app()->name;
?>
<h1>Bienvenido Inspector a <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>
<p style="text-align: justify">
    Aplicación informática para el registro de la información de los cuadernos de campos.
</p>
<p>
    Esta herramienta permite a los productores mejorar el control de sus procesos productivos, contribuyendo a la gestión
    y planificación de sus establecimientos. Además, provee a los organismos públicos
    y privados, ya sean certificadores o fiscalizadores, el acceso a datos seguros y confiables.
</p>

<h2>Cuadernos</h2>


<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => TbHtml::GRID_TYPE_STRIPED,
    'id' => 'permisos-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        //'Id',
        
        //''
        //'idCuaderno.',
        //'IdCuaderno',
        array(
            'name' => 'productor',
            'value' => '$data->idCuaderno->idExplotacion->idProductor->id->NombreApellido'),
        array(
            'name' => 'chacra',
            'value' => '$data->idCuaderno->idExplotacion->NombreChacra'),
        'idCuaderno.Temporada',
        'idCuaderno.NumeroRenspa',
        array(
            'class' => 'TbButtonColumn',
            'template' => '{view}',
            'buttons' => array(
                'view' => array(
                    'url' => 'Yii::app()->createUrl("/cuaderno/view",array("id"=>$data->IdCuaderno))'
                )
            )
        ),
    )
));


