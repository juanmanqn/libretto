<?php
/* @var $this UsuarioController */
/* @var $model Usuario */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'usuario-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">Campos con <span class="required">*</span> son obligatorios.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'Usuario'); ?>
        <?php echo $form->textField($model, 'Usuario', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'Usuario'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'Clave'); ?>
        <?php echo $form->passwordField($model, 'Clave', array('size' => 32, 'maxlength' => 32)); ?>
        <?php echo $form->error($model, 'Clave'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'Rol'); ?>
        <?php echo $form->dropDownList($model, 'IdRol', CHtml::listData(Rol::model()->findAll('Id > 2'), 'Id', 'Rol')); ?>
        <?php echo $form->error($model, 'IdRol'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'NombreApellido'); ?>
        <?php echo $form->textField($model, 'NombreApellido', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'NombreApellido'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'Email'); ?>
        <?php echo $form->textField($model, 'Email', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'Email'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'Direccion'); ?>
        <?php echo $form->textField($model, 'Direccion', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'Direccion'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'Provincia'); ?>

        <?php
            $provinciaItems = CHtml::listData(Provincia::model()->findAll(), 'Id', 'Nombre');
            echo CHtml::DropDownList('IdProvincia', 0, $provinciaItems, array('id' => 'IdProvincia', 'prompt' => 'Seleccionar..'));
        ?>

        <?php // echo $form->dropDownList($model, 'IdProvincia', CHtml::listData(Provincia::model()->findAll(), 'Id', 'Nombre')); ?>
        <?php // echo $form->error($model, 'IdProvincia'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'Localidad'); ?>
        <?php
//            $loc = 0;//$model->provincia;
//            $localidadItems = CHtml::listData(Localidad::model()->findAll('Provincia=:Provincia', array(':Provincia' => $loc)), 'Id', 'Nombre');
//            echo CHtml::DropDownList($model, 'Localidad', $localidadItems, array('id' => 'IdLocalidad', 'prompt' => '-'));
//            ECascadeDropDown::master('IdProvincia')->setDependent('IdLocalidad', array('dependentLoadingLabel' => 'Cargando Localidades...'), 'site/Localidaddata');
        ?>

        <?php echo $form->dropDownList($model, 'IdLocalidad', array(), array('empty'=>'Seleccionar..')); ?>
        <?php ECascadeDropDown::master('IdProvincia')->setDependent('Usuario_IdLocalidad', array('dependentLoadingLabel' => 'Cargando Localidades...'), '/localidad/data'); ?>
        <?php echo $form->error($model, 'IdLocalidad'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'Telefono'); ?>
        <?php echo $form->textField($model, 'Telefono', array('size' => 45, 'maxlength' => 45)); ?>
        <?php echo $form->error($model, 'Telefono'); ?>
    </div>

    <div class="row buttons">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->