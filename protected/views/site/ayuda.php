<?php
/* @var $this SiteController */
/* @var $usuario Usuario */
?>
<h1>Manual Paso a Paso</h1>
<hr/>
<p>
    <b>Primero</b>. 

    <?php
    echo TbHtml::linkButton('Registrarse', array('color' => TbHtml::BUTTON_COLOR_SUCCESS,
        'size' => TbHtml::BUTTON_SIZE_LARGE,
        'url' => Yii::app()->createUrl('/site/registro'),
        'id' => 'nuevaCuenta',
        'style' => 'margin-left:20px'
    ));
    ?> <b>ó</b>
    <?php
    echo TbHtml::linkButton('Ingresar', array('color' => TbHtml::BUTTON_COLOR_PRIMARY,
        'size' => TbHtml::BUTTON_SIZE_LARGE,
        'url' => Yii::app()->createUrl('/site/login'),
    ));
    ?>
    <?php
    if (!Yii::app()->user->isGuest) {
        echo 'Ya cumplió este paso';
    }
    ?>
</p>
<hr/>
<p>
    <b>Segundo</b>. 
    <?php
    echo TbHtml::linkButton('Crear una Explotación', array('color' => TbHtml::BUTTON_COLOR_SUCCESS,
        'size' => TbHtml::BUTTON_SIZE_LARGE,
        'url' => Yii::app()->createUrl('/explotacion/create'),
    ));
    ?>


<?php
if (!is_null($usuario) and count($usuario->productor->explotacions) > 0) {
    echo 'Ya cumplió este paso.  Ha creado explotaciones.';
}
?>
</p>
<hr/>
<p>
    <b>Tercero</b>. Crear Cuadernos para las explotaciones. </p>
<?php
if (!is_null($usuario) and count($usuario->productor->explotacions) > 0) {
    foreach ($usuario->productor->explotacions as $explotacion) {
        /* @var $explotacion Explotacion */
        echo '<p>';

        echo TbHtml::linkButton('Crear un Cuaderno para explotación ' . $explotacion->NombreChacra, array('color' => TbHtml::BUTTON_COLOR_SUCCESS,
            'size' => TbHtml::BUTTON_SIZE_LARGE,
            'url' => Yii::app()->createUrl('/cuaderno/create', array('idExplotacion' => $explotacion->Id
            )),
        ));
        if (count($explotacion->cuadernos) > 0) {
            echo 'Ya cumplió este paso.  Ha creado al menos un cuadro para la explotación.';
        }
        echo '</p>';
    }
}
?>

</p>
<hr/>
<p>
    <b>Cuarto</b>. Cargar cada hoja del cuaderno. </p>
<?php
if (!is_null($usuario) and count($usuario->productor->explotacions) > 0) {
    echo '<ul>';
    foreach ($usuario->productor->explotacions as $explotacion) {
        /* @var $explotacion Explotacion */
       

        
        if (count($explotacion->cuadernos) > 0) {
             echo '<li>';
            echo ' Explotación ' . $explotacion->NombreChacra;
            echo '<ul>';
            foreach ($explotacion->cuadernos as $cuaderno) {
                /* @var $cuaderno Cuaderno */
                echo '<li>';
                echo TbHtml::linkButton('Ver Cuaderno ' . $cuaderno->Temporada, array('color' => TbHtml::BUTTON_COLOR_SUCCESS,
            'size' => TbHtml::BUTTON_SIZE_LARGE,
            'url' => Yii::app()->createUrl('/cuaderno/view', array('id' => $cuaderno->Id
            )),
        ));
                echo '</li>';
                
                
            }
            echo '</ul>';
              echo '</li>';
        }
      
    }
    echo '</ul>';
}
?>
<hr/>
<h1>Composición del Cuaderno</h1>
<p>Cada cuaderno está compuesto por 16 hojas que se muestran en forma de solapas.
    Cada Solapa muestra un listado de los registros cargados y permite Imprimir, Crear, Borrar o Modificar.</p>
<p>Cuaderno. Informacion basica del Cuaderno.
<p>UMIS - Cuadros (TRV) - Plantaciones.
<p>Tablas Auxiliares: Tractoristas necesria para cargar Equipos y Tratamientos Fitosanitarios, Tipo Tratamientos necesarios para Tratamientos Fitosanitarios, Tipo Labores nesesarios para Labores Culturales, Destinos necesarios para Salidas</p>
<p>Croquis Ubicación, Huerto y Umis.
<p>Equipos</p> 
<p>Tratamientos Fitosanitarios – Maquinadas - Aplicaciones. Incluye cálculo del Costo basado en valor litro de los Tratamientos</p>
<p>Aplicación de Confusión Sexual. Formulario A
<p>Capturas Carpocapsa. Trampas - Capturas
<p>Muestreo de Daños en Fruta. Muestreo.
<p>Registros Fenológicos.
 <p>   Inspecciones. Realizadas por los inspectores de SENASA.
<p>Salida de Fruta.  Con grafico de comparación de Rindes por Variedad.

<p>    Registro de labores Culturales, según certificación Global G.A.P.  Incluye cálculo del Costo basado en valor hora por Tipo de Labor.
<p>    Permisos de lectura y/o escritura a otros usuario que cumplan con el rol de Técnicos o Inspectores.



<hr/>
<h1>Ayudar al Libretto</h1>
<p>Libretto es software libre, si queres participar del desarrollo del libretto podes descargar los fuentes y
    subir modificaciones a los stubs o a la aplicación en: 
    <a href="https://bitbucket.org/juanmanqn/libretto">https://bitbucket.org/juanmanqn/libretto</a></p>
<p>Otra forma de ayudar es siendo usuario, cargando información e informando posiblilidades de mejoras y/o errores que pueda tener la aplicación.</p>
<hr/>
