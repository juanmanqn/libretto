<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<h1 style='font-size:50px'>Bienvenido a <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>

<div style="font-size: 14px; text-align: justify; padding: 5px">
<p>
    Aplicación informática para el registro de la información de los cuadernos de campos.
</p>
<p>
    Esta herramienta permite a los productores mejorar el control de sus procesos productivos, contribuyendo a la gestión
    y planificación de sus establecimientos. Además, provee a los organismos públicos
    y privados, ya sean certificadores o fiscalizadores, el acceso a datos seguros y confiables.
</p>
<p>
    Si ya posee una cuenta, ingrese al sistema mediante el menu "Ingresar".
    De lo contrario regístrese para crear una.
</p>
</div>
<?php 
    echo TbHtml::linkButton('Ingresar', 
        array('color' => TbHtml::BUTTON_COLOR_PRIMARY,
            'size' => TbHtml::BUTTON_SIZE_LARGE,
            'url' => Yii::app()->createUrl('/site/login'),
        ));
?>
<?php 

    echo TbHtml::linkButton('Nueva Cuenta', 
        array('color' => TbHtml::BUTTON_COLOR_SUCCESS,
            'size' => TbHtml::BUTTON_SIZE_LARGE,
            'url' => Yii::app()->createUrl('/site/registro'),
            'id'=>'nuevaCuenta',
            'style'=> 'margin-left:20px'
        )); 
?>
