<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<h1>Bienvenido a <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>
<p style="text-align: justify">
    Aplicación informática para el registro de la información de los cuadernos de campos.
</p>
<p>
    Esta herramienta permite a los productores mejorar el control de sus procesos productivos, contribuyendo a la gestión
    y planificación de sus establecimientos. Además, provee a los organismos públicos
    y privados, ya sean certificadores o fiscalizadores, el acceso a datos seguros y confiables.
</p>

