<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
//$this->breadcrumbs=array(
//	'Ingreso',
//);
?>

<h1>Ingreso</h1>

<p>Por favor, ingrese con sus datos.</p>

<div class="form">
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>
    
	<div class="row">
		<?php echo $form->labelEx($model,'usuario'); ?>
		<?php echo $form->textField($model,'username'); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'clave'); ?>
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>		
	</div>
    
	<div class="row rememberMe">
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'recordarme'); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
	</div>

	<div class="row buttons">		
                <?php echo TbHtml::submitButton('Ingresar'); ?>
	</div>

    <p class="hint">
        <?php echo CHtml::link("Olvidó su clave?",array(
                          '/cambioClave/create'));?>
</p>
    <br><br>
    <p style="font-size:12px">Puede usar los siguientes usuarios de prueba:</li>
    <li style="font-size:12px">Usuario Productor: messi (contraseña messi).</li>
    <li style="font-size:12px">Usuario Profesional: sabella (contraseña sabella).</li>
    <li style="font-size:12px">Usuario Inspector: brienza (contraseña brienza).</li>
<?php $this->endWidget(); ?>
</div><!-- form -->
