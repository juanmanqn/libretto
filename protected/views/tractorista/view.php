<?php
/* @var $this TractoristaController */
/* @var $model Tractorista */

$this->breadcrumbs=array(
        $cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Tractoristas' => array('admin','idCuaderno'=> $cuaderno->Id ),
	$model->NombreApellido,
);

$this->menu=array(
	array('label'=>'Modificar Tractorista', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Eliminar Tractorista', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Esta seguro de querer eliminar este item?')),
	);
?>

<h1>Tractorista #<?php echo $model->Id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'NombreApellido',
	),
)); ?>
