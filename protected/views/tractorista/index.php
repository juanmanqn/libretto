<?php
/* @var $this TractoristaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tractoristas',
);

$this->menu=array(
	array('label'=>'Nuevo Tractorista', 'url'=>array('create')),
	array('label'=>'Listar Tractorista', 'url'=>array('admin')),
);
?>

<h1>Tractoristas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
