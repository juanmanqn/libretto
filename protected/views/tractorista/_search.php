<?php
/* @var $this TractoristaController */
/* @var $model Tractorista */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'Id'); ?>
		<?php echo $form->textField($model,'Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdCuaderno'); ?>
		<?php echo $form->textField($model,'IdCuaderno'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NombreApellido'); ?>
		<?php echo $form->textField($model,'NombreApellido',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row buttons">
		<?php echo TbHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->