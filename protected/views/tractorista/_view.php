<?php
/* @var $this TractoristaController */
/* @var $data Tractorista */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Id), array('view', 'id'=>$data->Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdCuaderno')); ?>:</b>
	<?php echo CHtml::encode($data->IdCuaderno); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NombreApellido')); ?>:</b>
	<?php echo CHtml::encode($data->NombreApellido); ?>
	<br />


</div>