<?php
/* @var $this TractoristaController */
/* @var $model Tractorista */

$this->breadcrumbs=array(	
        $cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Tractoristas',
);

$this->menu=array(	
	array('label'=>'Nuevo Tractorista', 'url'=>array('create', 'idCuaderno'=> $cuaderno->Id)),
);

$this->idCuaderno = $cuaderno->Id;

?>

<h1>Tractoristas</h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => TbHtml::GRID_TYPE_STRIPED,
	'id'=>'tractorista-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'Id',		
		'NombreApellido',
		array(
			'class'=>'TbButtonColumn',
                        'template'=>'{update}{delete}'
		),
	),
)); ?>
