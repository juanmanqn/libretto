<?php
/* @var $this TractoristaController */
/* @var $model Tractorista */

$this->breadcrumbs=array(
        $cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
        'Tractoristas' => array('tractorista/admin', 'idCuaderno'=> $cuaderno->Id),
	'Modificar #'.$model->NombreApellido,
);

?>

<h1>Modificar Tractorista <?php echo $model->Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>