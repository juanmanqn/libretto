<?php
/* @var $this TractoristaController */
/* @var $model Tractorista */

$this->breadcrumbs=array(
        $cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Tractoristas' => array('tractorista/admin', 'idCuaderno'=> $cuaderno->Id),
        'Nuevo'
);

?>

<h1>Nuevo Tractorista</h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'cuaderno'=>$cuaderno)); ?>