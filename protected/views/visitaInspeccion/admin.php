<?php
/* @var $this VisitaInspeccionController */
/* @var $model VisitaInspeccion */

$this->breadcrumbs=array(
	$cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),    
	'Inspecciones'
);

$this->menu=array(	
	array('label'=>'Nueva Visita Inspección', 'url'=>array('create', 'idCuaderno'=> $cuaderno->Id)),
);

$this->idCuaderno=$cuaderno->Id;
?>

<h1>Inspecciones</h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => TbHtml::GRID_TYPE_STRIPED,
	'id'=>'visita-inspeccion-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'Id',
		'idCuaderno.NumeroRenspa',
		//'idInspector.NombreApellido',
		'NombreApellido',
		'Legajo',
		'Fecha',
		/*
		'Observaciones',
		*/
		array(
			'class'=>'TbButtonColumn',
                        'template'=>'{view}'
		),
	),
)); ?>
