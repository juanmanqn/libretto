<?php
/* @var $this VisitaInspeccionController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Visita Inspeccions',
);

$this->menu=array(
	array('label'=>'Nuevo VisitaInspeccion', 'url'=>array('create')),
	array('label'=>'Listar VisitaInspeccion', 'url'=>array('admin')),
);
?>

<h1>Visita Inspeccions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
