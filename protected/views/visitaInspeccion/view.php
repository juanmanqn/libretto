<?php
/* @var $this VisitaInspeccionController */
/* @var $model VisitaInspeccion */

$this->breadcrumbs=array(
	$cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
        'Inspecciones'=>array('/visitaInspeccion/admin','idCuaderno'=>$cuaderno->Id),
        'Visita Inspección #'.$model->Fecha
);

?>

<h1>VisitaInspeccion #<?php echo $model->Id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'IdCuaderno',
		'IdInspector',
		'NombreApellido',
		'Legajo',
		'Fecha',
		'Observaciones',
	),
)); ?>
