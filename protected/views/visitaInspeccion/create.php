<?php
/* @var $this VisitaInspeccionController */
/* @var $model VisitaInspeccion */

$this->breadcrumbs=array(
        $cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Inspecciones'=>array('visitaInspeccion/admin','idCuaderno'=>$cuaderno->Id),
        'Visita Inspección #'.$model->Fecha
);

?>

<h1>Nueva Visita Inspección</h1>

<?php $this->renderPartial('_form', array('model'=>$model,'cuaderno'=>$cuaderno)); ?>