<?php
/* @var $this VisitaInspeccionController */
/* @var $data VisitaInspeccion */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Id), array('view', 'id'=>$data->Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdCuaderno')); ?>:</b>
	<?php echo CHtml::encode($data->IdCuaderno); ?>
	<br />

<!--	<b><?php //echo CHtml::encode($data->getAttributeLabel('IdInspector')); ?>:</b>
	<?php //echo CHtml::encode($data->IdInspector); ?>
	<br />-->

	<b><?php echo CHtml::encode($data->getAttributeLabel('NombreApellido')); ?>:</b>
	<?php echo CHtml::encode($data->NombreApellido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Legajo')); ?>:</b>
	<?php echo CHtml::encode($data->Legajo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Fecha')); ?>:</b>
	<?php echo CHtml::encode($data->Fecha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Observaciones')); ?>:</b>
	<?php echo CHtml::encode($data->Observaciones); ?>
	<br />


</div>