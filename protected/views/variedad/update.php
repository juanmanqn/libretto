<?php
/* @var $this VariedadController */
/* @var $model Variedad */

$this->breadcrumbs=array(
	'Variedads'=>array('index'),
	$model->Id. ' '=>array('view','id'=>$model->Id),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Nuevo Variedad', 'url'=>array('create')),
	array('label'=>'Ver Variedad', 'url'=>array('view', 'id'=>$model->Id)),
	array('label'=>'Listar Variedad', 'url'=>array('admin')),
);
?>

<h1>Modificar Variedad <?php echo $model->Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>