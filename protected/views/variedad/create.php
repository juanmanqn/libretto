<?php
/* @var $this VariedadController */
/* @var $model Variedad */

$this->breadcrumbs=array(
	'Variedads'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Listar Variedad', 'url'=>array('admin')),
);
?>

<h1>Nuevo Variedad</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>