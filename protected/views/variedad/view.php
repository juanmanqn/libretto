<?php
/* @var $this VariedadController */
/* @var $model Variedad */

$this->breadcrumbs=array(
	'Variedads'=>array('index'),
	$model->Id,
);

$this->menu=array(
	array('label'=>'Listar Variedad', 'url'=>array('admin')),
	array('label'=>'Nuevo Variedad', 'url'=>array('create')),
	array('label'=>'Modificar Variedad', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Eliminar Variedad', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Esta seguro de querer eliminar este item?')),
	);
?>

<h1>Variedad #<?php echo $model->Id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'Nombre',
		'Codigo',
		'IdEspecie',
	),
)); ?>
