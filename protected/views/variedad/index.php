<?php
/* @var $this VariedadController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Variedads',
);

$this->menu=array(
	array('label'=>'Nuevo Variedad', 'url'=>array('create')),
	array('label'=>'Listar Variedad', 'url'=>array('admin')),
);
?>

<h1>Variedads</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
