<?php
/* @var $this UsuarioController */
/* @var $model Usuario */

$this->breadcrumbs=array(
	'Usuarios'=>array('index'),
	'Listar',
);

$this->menu=array(
	array('label'=>'Nuevo Usuario', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#usuario-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Usuarios</h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => TbHtml::GRID_TYPE_STRIPED,
	'id'=>'usuario-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'Id',
		'Usuario',
                array(
                    'name'=>'Rol',
                    'value'=>'$data->idRol ?  $data->idRol->Rol: "-";'
                ),
		'NombreApellido',
		array(
                    'name'=>'Localidad',
                    'value'=>'$data->idLocalidad ?  $data->idLocalidad->Nombre: "-";'
                ),
                'Email',
                array(
                    'class'=>'TbButtonColumn',
                ),
	),
)); ?>
