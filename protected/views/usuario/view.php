<?php
/* @var $this UsuarioController */
/* @var $model Usuario */

$this->breadcrumbs=array(
	'Usuarios'=>array('index'),
	$model->Id,
);

$this->menu=array(
	array('label'=>'Listar Usuarios', 'url'=>array('admin')),
	array('label'=>'Nuevo Usuario', 'url'=>array('create')),
	array('label'=>'Modificar Usuario', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Eliminar Usuario', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Esta seguro de querer eliminar este item?')),
);
?>

<h1>Usuario #<?php echo $model->Id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(		
		'Id',
                'Usuario',
		array('name'=>'idRol.Rol', 'label'=>'Rol'),
		'NombreApellido',
		'Email',
		'Direccion',
		array('name'=>'idLocalidad.Nombre', 'label'=>'Localidad'),
		'Telefono',
	),
)); ?>
