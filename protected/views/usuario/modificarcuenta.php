<?php
/* @var $this UsuarioController */
/* @var $model Usuario */

$this->breadcrumbs=array(
	'Modificar Cuenta',
);

?>

<h1>Modificar Usuario <?php echo $model->NombreApellido; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>