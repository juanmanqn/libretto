<?php

ob_start();
$this->widget('ext.PdfGrid.EPDFGrid', array(
    'id'        => 'plantacion-pdf',
    'fileName'  => 'Plantaciones',//Nombre del archivo generado sin la extension pdf (.pdf)
    'dataProvider'  => $model->search(), //puede ser $model->search()
    'columns'   => array(                    
        'idCuadro.NumeroCuadro',
        'idCuadro.idUmi.Codigo',
        'idTipoProduccion.Nombre',
        'idVariedad.idEspecie.Codigo',
        'idVariedad.Codigo',
        'idPie.Nombre',
        'idCond.Nombre',
        'Anio',
        'DFilas',
        'DPlantas',
        'NumeroPlantas',
        'SuperficieNeta',
        'Catastro',
    ),
    'config'    => array(
        'title'     => 'Plantaciones',
        'subTitle'  => 'Cuaderno: '.$cuaderno->NumeroRenspa,
        //'colWidths' => array(40, 90, 40, 70),
    ),
));   
ob_end_flush(); 

