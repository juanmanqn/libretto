<?php
/* @var $this PlantacionController */
/* @var $data Plantacion */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Id), array('view', 'id'=>$data->Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdCuadro')); ?>:</b>
	<?php echo CHtml::encode($data->IdCuadro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdTipoProduccion')); ?>:</b>
	<?php echo CHtml::encode($data->IdTipoProduccion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdVariedad')); ?>:</b>
	<?php echo CHtml::encode($data->IdVariedad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdPie')); ?>:</b>
	<?php echo CHtml::encode($data->IdPie); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdCond')); ?>:</b>
	<?php echo CHtml::encode($data->IdCond); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Anio')); ?>:</b>
	<?php echo CHtml::encode($data->Anio); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('DFilas')); ?>:</b>
	<?php echo CHtml::encode($data->DFilas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DPlantas')); ?>:</b>
	<?php echo CHtml::encode($data->DPlantas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NumeroPlantas')); ?>:</b>
	<?php echo CHtml::encode($data->NumeroPlantas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('SuperficieNeta')); ?>:</b>
	<?php echo CHtml::encode($data->SuperficieNeta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Catastro')); ?>:</b>
	<?php echo CHtml::encode($data->Catastro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Latitud')); ?>:</b>
	<?php echo CHtml::encode($data->Latitud); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Longitud')); ?>:</b>
	<?php echo CHtml::encode($data->Longitud); ?>
	<br />

	*/ ?>

</div>