<?php
/* @var $this PlantacionController */
/* @var $model Plantacion */
/* @var $cuaderno Cuaderno */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'plantacion-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son obligatorios.</p>

	<?php echo $form->errorSummary($model); ?>

	
        <div class="row">
            <?php 
                if ($model->isNewRecord){
                    $umiId = 0;
                }
                else {
                    $umiId = $model->idCuadro->IdUmi;
                }
            ?>
            <?php echo $form->labelEx($model, 'Umi'); ?>
            <?php
                $items = CHtml::listData(Umi::model()->findAll('IdCuaderno=:IdCuaderno', array(':IdCuaderno'=>$cuaderno->Id)), 'Id', 'Codigo');
                echo CHtml::DropDownList('idUmi',$umiId, $items, array('id' => 'IdUmi', 'prompt' => 'Seleccione una opción'));
            ?>
        </div>

        <div class="row">
            <?php 
                if ($model->isNewRecord){
                    $arrayCuadro = array();
                }
                else {
                    $arrayCuadro = CHtml::listData(Cuadro::model()->findAll('IdUmi=:IdUmi',array(':IdUmi'=>$model->idCuadro->IdUmi)),'Id','NumeroCuadro');
                }
            ?>
            <?php echo $form->labelEx($model, 'IdCuadro'); ?>   
            <?php echo $form->dropDownList($model, 'IdCuadro', $arrayCuadro, array('empty'=>'Seleccionar..')); ?>
            <?php ECascadeDropDown::master('IdUmi')->setDependent('Plantacion_IdCuadro', array('dependentLoadingLabel' => 'Cargando Cuadros...'), '/cuadro/data'); ?>
            <?php echo $form->error($model, 'IdCuadro'); ?>
        </div>
        
	<div class="row">
		<?php echo $form->labelEx($model,'IdTipoProduccion'); ?>
		<?php echo $form->dropDownList($model,'IdTipoProduccion',CHtml::listData(TipoProduccion::model()->findAll(''), 'Id', 'Nombre')); ?>
		<?php echo $form->error($model,'IdTipoProduccion'); ?>
	</div>

        <div class="row">
            <?php 
                if ($model->isNewRecord){
                    $especieId = 0;
                }
                else {
                    $especieId = $model->idVariedad->IdEspecie;
                }
            ?>
            <?php echo $form->labelEx($model, 'Especie'); ?>
            <?php $items2 = CHtml::listData(Especie::model()->findAll(), 'Id', 'Nombre'); ?>
            <?php echo CHtml::DropDownList('idEspecie',$especieId, $items2, array('id' => 'IdEspecie', 'prompt' => 'Seleccione una opción'));?>            
        </div>        
        
	<div class="row">
                <?php 
                    if ($model->isNewRecord){
                        $arrayVariedad = array();
                    }
                    else {
                        $arrayVariedad = CHtml::listData(Variedad::model()->findAll('IdEspecie=:IdEspecie',
                                array(':IdEspecie'=>$model->idVariedad->IdEspecie)),'Id','Nombre');
                    }
                ?>
		<?php echo $form->labelEx($model,'IdVariedad'); ?>
		<?php echo $form->dropDownList($model,'IdVariedad', $arrayVariedad, array('empty'=>'Seleccionar..')); ?>
                <?php ECascadeDropDown::master('IdEspecie')->setDependent('Plantacion_IdVariedad', array('dependentLoadingLabel' => 'Cargando Variedades...'), '/variedad/data'); ?>
		<?php echo $form->error($model,'IdVariedad'); ?>
	</div>

        <div class="row">
                <?php 
                    if ($model->isNewRecord){
                        $arrayPie = array();
                    }
                    else {
                        $arrayPie = CHtml::listData(PortaInjertos::model()->findAll('IdEspecie=:IdEspecie',
                                array(':IdEspecie'=>$model->idPie->IdEspecie)),'Id','Nombre');
                    }
                ?>
		<?php echo $form->labelEx($model,'IdPie'); ?>
		<?php echo $form->dropDownList($model,'IdPie', $arrayPie, array('empty'=>'Seleccionar..')); ?>
                <?php $dropdown = new ECascadeDropDown('IdEspecie');// ECascadeDropDown::master('IdEspecie')
                    $dropdown->setDependent('Plantacion_IdPie', array(
                    'dependentLoadingLabel' => 'Cargando Pies...'), '/portaInjertos/data'); ?>
		<?php echo $form->error($model,'IdPie'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'IdCond'); ?>
		<?php echo $form->dropDownList($model,'IdCond',CHtml::listData(SistemaConduccion::model()->findAll(''), 'Id', 'Nombre')); ?>
		<?php echo $form->error($model,'IdCond'); ?>
	</div>
        
	<div class="row">
		<?php echo $form->labelEx($model,'Anio'); ?>
		<?php echo $form->textField($model,'Anio'); ?>
		<?php echo $form->error($model,'Anio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'DFilas'); ?>
		<?php echo $form->textField($model,'DFilas',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'DFilas'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'DPlantas'); ?>
		<?php echo $form->textField($model,'DPlantas',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'DPlantas'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'NumeroPlantas'); ?>
		<?php echo $form->textField($model,'NumeroPlantas'); ?>
		<?php echo $form->error($model,'NumeroPlantas'); ?>
	</div>

<!--	<div class="row">
		<?php //echo $form->labelEx($model,'SuperficieNeta'); ?>
		<?php //echo $form->textField($model,'SuperficieNeta',array('size'=>10,'maxlength'=>10)); ?>
		<?php //echo $form->error($model,'SuperficieNeta'); ?>
	</div>-->

	<div class="row">
		<?php echo $form->labelEx($model,'Catastro'); ?>
		<?php echo $form->textField($model,'Catastro',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'Catastro'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Latitud'); ?>
		<?php echo $form->textField($model,'Latitud'); ?>
		<?php echo $form->error($model,'Latitud'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Longitud'); ?>
		<?php echo $form->textField($model,'Longitud'); ?>
		<?php echo $form->error($model,'Longitud'); ?>
	</div>

	<div class="row buttons">
		<?php echo TbHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->