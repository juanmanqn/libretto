<?php
/* @var $this PlantacionController */
/* @var $model Plantacion */
/* @var $cuaderno Cuaderno */

$this->breadcrumbs=array(
        $cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Plantaciones',
);

$this->menu=array(	
	array('label'=>'Nueva Plantacion', 'url'=>array('create','idCuaderno'=> $cuaderno->Id)),
        array('label'=>'Imprimir', 'url'=>array('imprimir', 'idCuaderno'=> $cuaderno->Id)),
);

$this->idCuaderno = $cuaderno->Id;
?>

<h1>Plantaciones</h1>

<?php 
$this->widget('bootstrap.widgets.TbGridView', array(
        'type' => TbHtml::GRID_TYPE_STRIPED,
	'id'=>'plantacion-grid',
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'columns'=>array(
		'idCuadro.NumeroCuadro',
		'idCuadro.idUmi.Codigo',
		'idTipoProduccion.Nombre',
                'idVariedad.idEspecie.Codigo',
		'idVariedad.Codigo',
		'idPie.Nombre',
		'idCond.Nombre',
		'Anio',
		'DFilas',
		'DPlantas',
		'NumeroPlantas',
		'SuperficieNeta',
		'Catastro',		
		array(
			'class'=>'TbButtonColumn',
		),
	),
)); ?>
