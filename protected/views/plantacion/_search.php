<?php
/* @var $this PlantacionController */
/* @var $model Plantacion */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'Id'); ?>
		<?php echo $form->textField($model,'Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdCuadro'); ?>
		<?php echo $form->textField($model,'IdCuadro'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdTipoProduccion'); ?>
		<?php echo $form->textField($model,'IdTipoProduccion'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdVariedad'); ?>
		<?php echo $form->textField($model,'IdVariedad'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdPie'); ?>
		<?php echo $form->textField($model,'IdPie'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdCond'); ?>
		<?php echo $form->textField($model,'IdCond'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Anio'); ?>
		<?php echo $form->textField($model,'Anio'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DFilas'); ?>
		<?php echo $form->textField($model,'DFilas',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DPlantas'); ?>
		<?php echo $form->textField($model,'DPlantas',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NumeroPlantas'); ?>
		<?php echo $form->textField($model,'NumeroPlantas'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'SuperficieNeta'); ?>
		<?php echo $form->textField($model,'SuperficieNeta',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Catastro'); ?>
		<?php echo $form->textField($model,'Catastro',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Latitud'); ?>
		<?php echo $form->textField($model,'Latitud'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Longitud'); ?>
		<?php echo $form->textField($model,'Longitud'); ?>
	</div>

	<div class="row buttons">
		<?php echo TbHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->