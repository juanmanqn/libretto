<?php
/* @var $this PlantacionController */
/* @var $model Plantacion */
/* @var $cuaderno Cuaderno */

$this->breadcrumbs=array(
	$cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Plantaciones' => array('admin','idCuaderno'=> $cuaderno->Id ),
	$model->Id,
);

$this->menu=array(
	array('label'=>'Modificar Plantacion', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Eliminar Plantacion', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Esta seguro de querer eliminar este item?')),
	);
?>

<h1>Plantacion #<?php echo $model->idCuadro->idUmi->Codigo." - ".$model->idCuadro->NumeroCuadro." - ".$model->idVariedad->Codigo ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idCuadro.NumeroCuadro',
		'idCuadro.idUmi.Codigo',
                'idCuadro.idUmi.Descripcion',
		'idTipoProduccion.Nombre',
                'idVariedad.idEspecie.Nombre',
		'idVariedad.Nombre',
		'idPie.Nombre',
		'idCond.Nombre',
		'Anio',
		'DFilas',
		'DPlantas',
		'NumeroPlantas',
		'SuperficieNeta',
		'Catastro',
	),
)); ?>
