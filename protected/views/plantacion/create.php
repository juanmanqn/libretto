<?php
/* @var $this PlantacionController */
/* @var $model Plantacion */

$this->breadcrumbs=array(
	$cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Plantaciones' => array('plantacion/admin', 'idCuaderno'=> $cuaderno->Id),
        'Nueva'
);

$this->menu=array(
	//array('label'=>'Listar Plantaciones', 'url'=>array('admin')),
);
?>

<h1>Nueva Plantacion</h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'cuaderno'=>$cuaderno)); ?>