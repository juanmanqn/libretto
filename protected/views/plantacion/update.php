<?php
/* @var $this PlantacionController */
/* @var $model Plantacion */

$this->breadcrumbs=array(
	$cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Plantaciones' => array('plantacion/admin', 'idCuaderno'=> $cuaderno->Id),
	'Modificar #'.$model->Id
);

?>

<h1>Modificar Plantacion <?php echo $model->Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'cuaderno'=>$cuaderno)); ?>