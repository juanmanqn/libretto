<?php
/* @var $this PlantacionController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Plantacions',
);

$this->menu=array(
	array('label'=>'Nuevo Plantacion', 'url'=>array('create')),
	array('label'=>'Listar Plantacion', 'url'=>array('admin')),
);
?>

<h1>Plantacions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
