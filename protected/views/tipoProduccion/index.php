<?php
/* @var $this TipoProduccionController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tipo Produccions',
);

$this->menu=array(
	array('label'=>'Nuevo TipoProduccion', 'url'=>array('create')),
	array('label'=>'Listar TipoProduccion', 'url'=>array('admin')),
);
?>

<h1>Tipo Produccions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
