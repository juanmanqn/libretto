<?php
/* @var $this TipoProduccionController */
/* @var $model TipoProduccion */

$this->breadcrumbs=array(
	'Tipo Produccions'=>array('index'),
	$model->Id,
);

$this->menu=array(
	array('label'=>'Listar TipoProduccion', 'url'=>array('admin')),
	array('label'=>'Nuevo TipoProduccion', 'url'=>array('create')),
	array('label'=>'Modificar TipoProduccion', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Eliminar TipoProduccion', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Esta seguro de querer eliminar este item?')),
	);
?>

<h1>TipoProduccion #<?php echo $model->Id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'Nombre',
	),
)); ?>
