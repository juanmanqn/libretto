<?php
/* @var $this TipoProduccionController */
/* @var $model TipoProduccion */

$this->breadcrumbs=array(
	'Tipo Produccions'=>array('index'),
	$model->Id. ' '=>array('view','id'=>$model->Id),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Nuevo TipoProduccion', 'url'=>array('create')),
	array('label'=>'Ver TipoProduccion', 'url'=>array('view', 'id'=>$model->Id)),
	array('label'=>'Listar TipoProduccion', 'url'=>array('admin')),
);
?>

<h1>Modificar TipoProduccion <?php echo $model->Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>