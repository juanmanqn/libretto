<?php
/* @var $this TipoProduccionController */
/* @var $model TipoProduccion */

$this->breadcrumbs=array(
	'Tipo Produccions'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Listar TipoProduccion', 'url'=>array('admin')),
);
?>

<h1>Nuevo TipoProduccion</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>