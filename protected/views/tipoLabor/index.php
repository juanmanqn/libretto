<?php
/* @var $this TipoLaborController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tipo Labors',
);

$this->menu=array(
	array('label'=>'Nuevo TipoLabor', 'url'=>array('create')),
	array('label'=>'Listar TipoLabor', 'url'=>array('admin')),
);
?>

<h1>Tipo Labors</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
