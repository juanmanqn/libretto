<?php
/* @var $this TipoLaborController */
/* @var $model TipoLabor */

$this->breadcrumbs=array(
	$cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Tipos de Labor' => array('tipoLabor/admin', 'idCuaderno'=> $cuaderno->Id),
	'Modificar',
);

?>

<h1>Modificar Tipo de Labor <?php echo $model->Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>