<?php
/* @var $this TipoLaborController */
/* @var $model TipoLabor */

$this->breadcrumbs=array(
	'Tipo Labors'=>array('index'),
	$model->Id,
);

$this->menu=array(
	array('label'=>'Listar TipoLabor', 'url'=>array('admin')),
	array('label'=>'Nuevo TipoLabor', 'url'=>array('create')),
	array('label'=>'Modificar TipoLabor', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Eliminar TipoLabor', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Esta seguro de querer eliminar este item?')),
	);
?>

<h1>TipoLabor #<?php echo $model->Id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'Nombre',
	),
)); ?>
