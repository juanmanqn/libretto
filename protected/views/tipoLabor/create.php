<?php
/* @var $this TipoLaborController */
/* @var $model TipoLabor */

$this->breadcrumbs=array(
	$cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Tipos de Labor' => array('tipoLabor/admin', 'idCuaderno'=> $cuaderno->Id),
        'Nuevo'
);

?>

<h1>Nuevo Tipo de Labor</h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'cuaderno'=>$cuaderno)); ?>