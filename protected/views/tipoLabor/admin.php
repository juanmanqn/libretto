<?php
/* @var $this TipoLaborController */
/* @var $model TipoLabor */

$this->breadcrumbs=array(
	$cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Tipos de Labores',
);

$this->menu=array(	
	array('label'=>'Nuevo Tipo de Labor', 'url'=>array('create', 'idCuaderno'=> $cuaderno->Id)),
);

$this->idCuaderno = $cuaderno->Id;

?>

<h1>Tipos de Labores</h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => TbHtml::GRID_TYPE_STRIPED,
	'id'=>'tipo-labor-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'Id',
		'Nombre',
            'CostoPorHora',
		array(
			'class'=>'TbButtonColumn',
                        'template'=>'{update}{delete}'
		),
	),
)); ?>
