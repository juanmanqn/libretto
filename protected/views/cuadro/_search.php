<?php
/* @var $this CuadroController */
/* @var $model Cuadro */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'Id'); ?>
		<?php echo $form->textField($model,'Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdUmi'); ?>
		<?php echo $form->textField($model,'IdUmi'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NumeroCuadro'); ?>
		<?php echo $form->textField($model,'NumeroCuadro'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Altura'); ?>
		<?php echo $form->textField($model,'Altura',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Ancho'); ?>
		<?php echo $form->textField($model,'Ancho',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DistanciaFilas'); ?>
		<?php echo $form->textField($model,'DistanciaFilas',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row buttons">
		<?php echo TbHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->