<?php
/* @var $this CuadroController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cuadros',
);

$this->menu=array(
	array('label'=>'Nuevo Cuadro', 'url'=>array('create')),
	array('label'=>'Listar Cuadro', 'url'=>array('admin')),
);
?>

<h1>Cuadros</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
