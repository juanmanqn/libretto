<?php
/* @var $this CuadroController */
/* @var $model Cuadro */

$this->breadcrumbs=array(
	$cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	//'Cuadros' => array('cuadro/admin', 'idCuaderno' => $cuaderno->Id),
	$model->NumeroCuadro,
);

$this->menu=array(
	array('label'=>'Modificar Cuadro', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Eliminar Cuadro', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Esta seguro de querer eliminar este item?')),
	);
?>

<h1>Cuadro #<?php echo $model->NumeroCuadro; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'idUmi.Codigo',
		'NumeroCuadro',
		'Altura',
		'Ancho',
		'DistanciaFilas',
	),
)); ?>
