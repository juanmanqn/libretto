<?php
/* @var $this CuadroController */
/* @var $model Cuadro */

$this->breadcrumbs=array(
	$cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
        'Cuadros' => array('cuadro/admin', 'idCuaderno'=> $cuaderno->Id),
	'Modificar #'.$model->NumeroCuadro,
);

?>

<h1>Modificar Cuadro <?php echo $model->NumeroCuadro; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'cuaderno'=>$cuaderno)); ?>