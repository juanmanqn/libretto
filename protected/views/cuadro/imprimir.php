<?php

ob_start();
$this->widget('ext.PdfGrid.EPDFGrid', array(
    'id'        => 'cuadro-pdf',
    'fileName'  => 'Cuadros',//Nombre del archivo generado sin la extension pdf (.pdf)
    'dataProvider'  => $model->search(), //puede ser $model->search()
    'columns'   => array(                    
        'NumeroCuadro',
        'idUmi.Codigo',		
        'Altura',
        'Ancho',
        'DistanciaFilas',
        array('header'=>'TRV',
                'value'=>'$data->getTRV()'),
        array('header'=>'Superfice',
                'value'=>'$data->getSuperficie()'),
    ),
    'config'    => array(
        'title'     => 'Cuadros',
        'subTitle'  => 'Cuaderno: '.$cuaderno->NumeroRenspa,
        //'colWidths' => array(40, 90, 40, 70),
    ),
));   
ob_end_flush(); 

