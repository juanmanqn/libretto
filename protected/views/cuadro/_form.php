<?php
/* @var $this CuadroController */
/* @var $model Cuadro */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cuadro-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son obligatorios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">                
                <?php echo $form->labelEx($model,'IdUmi'); ?>
		<?php echo $form->dropDownList($model,'IdUmi',CHtml::listData(Umi::model()->findAll('IdCuaderno=:IdCuaderno',
                        array(':IdCuaderno'=>$cuaderno->Id)), 'Id', 'Codigo')); ?>
		<?php echo $form->error($model,'IdUmi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'NumeroCuadro'); ?>
		<?php echo $form->textField($model,'NumeroCuadro'); ?>
		<?php echo $form->error($model,'NumeroCuadro'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Altura'); ?>
		<?php echo $form->textField($model,'Altura',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'Altura'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Ancho'); ?>
		<?php echo $form->textField($model,'Ancho',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'Ancho'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'DistanciaFilas'); ?>
		<?php echo $form->textField($model,'DistanciaFilas',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'DistanciaFilas'); ?>
	</div>

	<div class="row buttons">
		<?php echo TbHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->