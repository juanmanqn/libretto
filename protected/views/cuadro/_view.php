<?php
/* @var $this CuadroController */
/* @var $data Cuadro */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Id), array('view', 'id'=>$data->Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdUmi')); ?>:</b>
	<?php echo CHtml::encode($data->IdUmi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NumeroCuadro')); ?>:</b>
	<?php echo CHtml::encode($data->NumeroCuadro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Altura')); ?>:</b>
	<?php echo CHtml::encode($data->Altura); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Ancho')); ?>:</b>
	<?php echo CHtml::encode($data->Ancho); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DistanciaFilas')); ?>:</b>
	<?php echo CHtml::encode($data->DistanciaFilas); ?>
	<br />


</div>