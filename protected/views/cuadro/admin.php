<?php
/* @var $this CuadroController */
/* @var $model Cuadro */

$this->breadcrumbs=array(    
        $cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Cuadros',
);

$this->menu=array(	
	array('label'=>'Nuevo Cuadro', 'url'=>array('create', 'idCuaderno'=> $cuaderno->Id)),
        array('label'=>'Imprimir', 'url'=>array('imprimir', 'idCuaderno'=> $cuaderno->Id)),
);

$this->idCuaderno = $cuaderno->Id;

?>

<h1>Cuadros</h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => TbHtml::GRID_TYPE_STRIPED,
	'id'=>'cuadro-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
                'NumeroCuadro',
		'idUmi.Codigo',		
		'Altura',
		'Ancho',
		'DistanciaFilas',
                array('header'=>'TRV',
                        'value'=>'$data->getTRV()'),
                array('header'=>'Superfice',
                        'value'=>'$data->getSuperficie()'),
            
		array(
			'class'=>'TbButtonColumn',
                        'template'=>'{update}{delete}',
		),
	),
)); ?>
