<?php
/* @var $this TrampaController */
/* @var $model Trampa */
/* @var $cuaderno Cuaderno */

$this->breadcrumbs=array(	
        $cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Capturas y Trampas' => array('/capturaCarpocapsa/admin','idCuaderno'=>$cuaderno->Id),
        'Modificar Trampa #'.$model->Numero
);

?>

<h1>Modificar Trampa <?php echo $model->Numero; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,'cuaderno'=>$cuaderno)); ?>