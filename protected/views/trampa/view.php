<?php
/* @var $this TrampaController */
/* @var $model Trampa */

$this->breadcrumbs=array(
	'Trampas'=>array('index'),
	$model->Id,
);

$this->menu=array(
	array('label'=>'Listar Trampa', 'url'=>array('admin')),
	array('label'=>'Nuevo Trampa', 'url'=>array('create')),
	array('label'=>'Modificar Trampa', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Eliminar Trampa', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Esta seguro de querer eliminar este item?')),
	);
?>

<h1>Trampa #<?php echo $model->Id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'IdCuadro',
		'Numero',
	),
)); ?>
