<?php
/* @var $this TrampaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Trampas',
);

$this->menu=array(
	array('label'=>'Nuevo Trampa', 'url'=>array('create')),
	array('label'=>'Listar Trampa', 'url'=>array('admin')),
);
?>

<h1>Trampas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
