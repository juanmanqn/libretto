<?php
/* @var $this PermisosController */
/* @var $model Permisos */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'permisos-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son obligatorios.</p>

	<?php echo $form->errorSummary($model); ?>

        <div class="row">
            <?php 
                if ($model->isNewRecord){
                    $rolId = 0;
                }
                else {
                    $rolId = $model->idUsuario->IdRol;
                }
            ?>
            <?php echo $form->labelEx($model, 'Rol'); ?>
            <?php
                $items = CHtml::listData(Rol::model()->findAll('Id<>1 and Id<>3'), 'Id', 'Rol');
                echo CHtml::DropDownList('idRol',$rolId, $items, array('id' => 'IdRol', 'prompt' => 'Seleccione una opción'));
            ?>
        </div>
        
        

        
        
        
        
	<div class="row">
             <?php 
                if ($model->isNewRecord){
                    $items = array();
                }
                else {
                    $items = CHtml::listData(Rol::model()->findAll('Id=:Id',array(':Id'=>$model->idUsuario->IdRol)),'Id','NombreApellido');
                }
            ?>
		<?php echo $form->labelEx($model,'IdUsuario'); ?>
		<?php echo $form->dropDownList($model,'IdUsuario',$items,array('empty'=>'Seleccionar..')); ?>
                <?php ECascadeDropDown::master('IdRol')->setDependent('Permisos_IdUsuario', array('dependentLoadingLabel' => 'Cargando Usuarios...'), '/usuario/data'); ?>
		<?php echo $form->error($model,'IdUsuario'); ?>
	</div>

	<div class="row buttons">
		<?php echo TbHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->