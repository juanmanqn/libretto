<?php
/* @var $this PermisosController */
/* @var $model Permisos */

$this->breadcrumbs=array(
        $cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Permisos'=>array('permisos/admin', 'idCuaderno'=> $cuaderno->Id ),
	'Modificar '.$model->idUsuario->NombreApellido,
);

?>

<h1>Modificar Permisos <?php echo $model->Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>