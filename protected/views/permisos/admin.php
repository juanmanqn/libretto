<?php
/* @var $this PermisosController */
/* @var $model Permisos */

$this->breadcrumbs=array(	
        $cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Permisos',
);

$this->menu=array(	
	array('label'=>'Nuevo Permiso', 'url'=>array('create', 'idCuaderno'=> $cuaderno->Id)),
);
$this->idCuaderno=$cuaderno->Id;


?>

<h1>Permisos</h1>


<?php $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => TbHtml::GRID_TYPE_STRIPED,
	'id'=>'permisos-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'Id',
		
                'idUsuario.idRol.Rol',
		'idUsuario.NombreApellido',
                //'IdCuaderno',
		array(
                    'template'=>'{delete}',
			'class'=>'TbButtonColumn',
		),
	),
)); ?>
