<?php
/* @var $this PermisosController */
/* @var $model Permisos */


$this->breadcrumbs=array(
        $cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Permisos'=>array('permisos/admin', 'idCuaderno'=> $cuaderno->Id ),
	'Nuevo',
);

?>

<h1>Nuevo Permiso</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>