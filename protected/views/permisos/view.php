<?php
/* @var $this PermisosController */
/* @var $model Permisos */

$this->breadcrumbs=array(
	'Permisoses'=>array('index'),
	$model->Id,
);

$this->menu=array(
	array('label'=>'Listar Permisos', 'url'=>array('admin')),
	array('label'=>'Nuevo Permisos', 'url'=>array('create')),
	array('label'=>'Modificar Permisos', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Eliminar Permisos', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Esta seguro de querer eliminar este item?')),
	);
?>

<h1>Permisos #<?php echo $model->Id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'IdUsuario',
		'IdCuaderno',
	),
)); ?>
