<?php
/* @var $this PermisosController */
/* @var $data Permisos */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Id), array('view', 'id'=>$data->Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdUsuario')); ?>:</b>
	<?php echo CHtml::encode($data->IdUsuario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdCuaderno')); ?>:</b>
	<?php echo CHtml::encode($data->IdCuaderno); ?>
	<br />


</div>