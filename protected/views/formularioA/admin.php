<?php
/* @var $this FormularioAController */
/* @var $model FormularioA */
/* @var $cuaderno Cuaderno */

$this->breadcrumbs=array(	
        $cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'FormularioA',
);

$this->menu=array(	
	array('label'=>'Nuevo Item FormularioA', 'url'=>array('create','idCuaderno'=>$cuaderno->Id)),
        array('label'=>'Imprimir', 'url'=>array('imprimir', 'idCuaderno'=> $cuaderno->Id)),
);
$this->idCuaderno=$cuaderno->Id;

?>

<h1>Formulario A</h1>


<?php 
//$model->idCuadro->getSuperficieEspecie($idEspecie)
$this->widget('bootstrap.widgets.TbGridView', array(
        'type' => TbHtml::GRID_TYPE_STRIPED,
	'id'=>'formulario-a-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'idCuadro.NumeroCuadro',
                array('header'=>'Superficie',
                       'value'=>'$data->idCuadro->getSuperficieEspecie($data->IdEspecie)'),
//		'Superficie',
		'idEspecie.Nombre',
		'TipoTCS',
		'DispNroPorCuadroDosis1',
		'DispNroPorHectareaDosis1',
		'DispNroPorCuadroDosis2',
		'DispNroPorHectareaDosis2',
		array(
			'class'=>'TbButtonColumn',
                        'template' => '{update}{delete}'
		),
	),
)); ?>
