<?php
/* @var $this FormularioAController */
/* @var $model FormularioA */

$this->breadcrumbs=array(
	'Formulario As'=>array('index'),
	$model->Id,
);

$this->menu=array(
	array('label'=>'Listar FormularioA', 'url'=>array('admin')),
	array('label'=>'Nuevo FormularioA', 'url'=>array('create')),
	array('label'=>'Modificar FormularioA', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Eliminar FormularioA', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Esta seguro de querer eliminar este item?')),
	);
?>

<h1>FormularioA #<?php echo $model->Id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'IdCuadro',
		'Superficie',
		'IdEspecie',
		'TipoTCS',
		'DispNroPorCuadroDosis1',
		'DispNroPorHectareaDosis1',
		'DispNroPorCuadroDosis2',
		'DispNroPorHectareaDosis2',
	),
)); ?>
