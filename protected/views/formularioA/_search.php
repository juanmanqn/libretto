<?php
/* @var $this FormularioAController */
/* @var $model FormularioA */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'Id'); ?>
		<?php echo $form->textField($model,'Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdCuadro'); ?>
		<?php echo $form->textField($model,'IdCuadro'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Superficie'); ?>
		<?php echo $form->textField($model,'Superficie',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdEspecie'); ?>
		<?php echo $form->textField($model,'IdEspecie'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TipoTCS'); ?>
		<?php echo $form->textField($model,'TipoTCS',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DispNroPorCuadroDosis1'); ?>
		<?php echo $form->textField($model,'DispNroPorCuadroDosis1',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DispNroPorHectareaDosis1'); ?>
		<?php echo $form->textField($model,'DispNroPorHectareaDosis1',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DispNroPorCuadroDosis2'); ?>
		<?php echo $form->textField($model,'DispNroPorCuadroDosis2',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DispNroPorHectareaDosis2'); ?>
		<?php echo $form->textField($model,'DispNroPorHectareaDosis2',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row buttons">
		<?php echo TbHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->