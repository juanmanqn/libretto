<?php
/* @var $this FormularioAController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Formulario As',
);

$this->menu=array(
	array('label'=>'Nuevo FormularioA', 'url'=>array('create')),
	array('label'=>'Listar FormularioA', 'url'=>array('admin')),
);
?>

<h1>Formulario As</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
