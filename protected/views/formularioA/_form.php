<?php
/* @var $this FormularioAController */
/* @var $model FormularioA */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'formulario-a-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son obligatorios.</p>

	<?php echo $form->errorSummary($model); ?>
        <div class="row">
            <?php 
                if ($model->isNewRecord){
                    $umiId = 0;
                }
                else {
                    $umiId = $model->idCuadro->IdUmi;
                }
            ?>
            <?php echo $form->labelEx($model, 'Umi'); ?>
            <?php
                $items = CHtml::listData(Umi::model()->findAll('IdCuaderno=:IdCuaderno', array(':IdCuaderno'=>$cuaderno->Id)), 'Id', 'Codigo');
                echo CHtml::DropDownList('idUmi',$umiId, $items, array('id' => 'IdUmi', 'prompt' => 'Seleccione una opción'));
            ?>
        </div>
	<div class="row">
            <?php 
                if ($model->isNewRecord){
                    $arrayCuadro = array();
                }
                else {
                    $arrayCuadro = CHtml::listData(Cuadro::model()->findAll('IdUmi=:IdUmi',array(':IdUmi'=>$model->idCuadro->IdUmi)),'Id','NumeroCuadro');
                }
            ?>
            <?php echo $form->labelEx($model, 'IdCuadro'); ?>   
            <?php echo $form->dropDownList($model, 'IdCuadro', $arrayCuadro, array('empty'=>'Seleccionar..')); ?>
            <?php ECascadeDropDown::master('IdUmi')->setDependent('FormularioA_IdCuadro', array('dependentLoadingLabel' => 'Cargando Cuadros...'), '/cuadro/data'); ?>
            <?php echo $form->error($model, 'IdCuadro'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'IdEspecie'); ?>
		<?php echo $form->dropDownList($model,'IdEspecie',CHtml::listData(Especie::model()->findAll(), 'Id', 'Nombre'),array('empty'=>'Seleccionar..'));?>            
                <?php echo $form->error($model,'IdEspecie'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TipoTCS'); ?>
		<?php echo $form->textField($model,'TipoTCS',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'TipoTCS'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'DispNroPorCuadroDosis1'); ?>
		<?php echo $form->textField($model,'DispNroPorCuadroDosis1',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'DispNroPorCuadroDosis1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'DispNroPorHectareaDosis1'); ?>
		<?php echo $form->textField($model,'DispNroPorHectareaDosis1',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'DispNroPorHectareaDosis1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'DispNroPorCuadroDosis2'); ?>
		<?php echo $form->textField($model,'DispNroPorCuadroDosis2',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'DispNroPorCuadroDosis2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'DispNroPorHectareaDosis2'); ?>
		<?php echo $form->textField($model,'DispNroPorHectareaDosis2',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'DispNroPorHectareaDosis2'); ?>
	</div>

	<div class="row buttons">
		<?php echo TbHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->