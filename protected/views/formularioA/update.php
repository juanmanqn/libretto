<?php
/* @var $this FormularioAController */
/* @var $model FormularioA */
/* @var $cuaderno Cuaderno */


$this->breadcrumbs=array(	
        $cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'FormularioA' => array('/formularioA/admin','idCuaderno'=>$cuaderno->Id),
        'Modificar'
);

?>

<h1>Modificar FormularioA <?php echo $model->Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'cuaderno'=>$cuaderno)); ?>