<?php
/* @var $this FormularioAController */
/* @var $data FormularioA */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Id), array('view', 'id'=>$data->Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdCuadro')); ?>:</b>
	<?php echo CHtml::encode($data->IdCuadro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Superficie')); ?>:</b>
	<?php echo CHtml::encode($data->Superficie); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdEspecie')); ?>:</b>
	<?php echo CHtml::encode($data->IdEspecie); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TipoTCS')); ?>:</b>
	<?php echo CHtml::encode($data->TipoTCS); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DispNroPorCuadroDosis1')); ?>:</b>
	<?php echo CHtml::encode($data->DispNroPorCuadroDosis1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DispNroPorHectareaDosis1')); ?>:</b>
	<?php echo CHtml::encode($data->DispNroPorHectareaDosis1); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('DispNroPorCuadroDosis2')); ?>:</b>
	<?php echo CHtml::encode($data->DispNroPorCuadroDosis2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DispNroPorHectareaDosis2')); ?>:</b>
	<?php echo CHtml::encode($data->DispNroPorHectareaDosis2); ?>
	<br />

	*/ ?>

</div>