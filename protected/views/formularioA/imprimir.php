<?php

ob_start();
$this->widget('ext.PdfGrid.EPDFGrid', array(
    'id'        => 'formularioA-pdf',
    'fileName'  => 'FormularioA',//Nombre del archivo generado sin la extension pdf (.pdf)
    'dataProvider'  => $model->search(), //puede ser $model->search()
    'columns'   => array(     
                'idCuadro.NumeroCuadro',
                array('header'=>'Superficie',
                       'value'=>'$data->idCuadro->getSuperficieEspecie($data->IdEspecie)'),
//		'Superficie',
		'idEspecie.Nombre',
		'TipoTCS',
		'DispNroPorCuadroDosis1',
		'DispNroPorHectareaDosis1',
		'DispNroPorCuadroDosis2',
		'DispNroPorHectareaDosis2',
    ),
    'config'    => array(
        'title'     => 'Formulario A',
        'subTitle'  => 'Cuaderno: '.$cuaderno->NumeroRenspa,
        //'colWidths' => array(40, 90, 40, 70),
    ),
));   
ob_end_flush(); 

