<?php
/* @var $this ProductorController */
/* @var $model Productor */

$this->breadcrumbs=array(
	'Productores'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Listar Productores', 'url'=>array('admin')),
);
?>

<h1>Nuevo Productor</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>