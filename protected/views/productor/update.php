<?php
/* @var $this ProductorController */
/* @var $model Productor */

$this->breadcrumbs=array(
	'Productores'=>array('index'),
	$model->Id. ' '=>array('view','id'=>$model->Id),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Nuevo Productor', 'url'=>array('create')),
	array('label'=>'Ver Productor', 'url'=>array('view', 'id'=>$model->Id)),
	array('label'=>'Listar Productor', 'url'=>array('admin')),
);
?>

<h1>Modificar Productor <?php echo $model->Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>