<?php
/* @var $this ProductorController */
/* @var $model Productor */

$this->breadcrumbs=array(
	'Productores'=>array('index'),
	$model->Id,
);

$this->menu=array(
	array('label'=>'Listar Productores', 'url'=>array('admin')),
	array('label'=>'Nuevo Productor', 'url'=>array('create')),
	array('label'=>'Modificar Productor', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Eliminar Productor', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Esta seguro de querer eliminar este item?')),
	);
?>

<h1>Productor #<?php echo $model->Id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
	),
)); ?>
