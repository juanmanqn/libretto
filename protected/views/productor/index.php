<?php
/* @var $this ProductorController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Productors',
);

$this->menu=array(
	array('label'=>'Nuevo Productor', 'url'=>array('create')),
	array('label'=>'Listar Productor', 'url'=>array('admin')),
);
?>

<h1>Productores</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
