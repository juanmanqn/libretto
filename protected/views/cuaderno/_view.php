<?php
/* @var $this CuadernoController */
/* @var $data Cuaderno */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Id), array('view', 'id'=>$data->Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdExplotacion')); ?>:</b>
	<?php echo CHtml::encode($data->IdExplotacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NumeroRenspa')); ?>:</b>
	<?php echo CHtml::encode($data->NumeroRenspa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NumeroCuaderno')); ?>:</b>
	<?php echo CHtml::encode($data->NumeroCuaderno); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Temporada')); ?>:</b>
	<?php echo CHtml::encode($data->Temporada); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NombreChacra')); ?>:</b>
	<?php echo CHtml::encode($data->NombreChacra); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NumeroInterno')); ?>:</b>
	<?php echo CHtml::encode($data->NumeroInterno); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('Localidad')); ?>:</b>
	<?php echo CHtml::encode($data->idLocalidad->Nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('SuperficieTotal')); ?>:</b>
	<?php echo CHtml::encode($data->SuperficieTotal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('SuperficieNeta')); ?>:</b>
	<?php echo CHtml::encode($data->SuperficieNeta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdProductor')); ?>:</b>
	<?php echo CHtml::encode($data->IdProductor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ProductorNombreApellido')); ?>:</b>
	<?php echo CHtml::encode($data->ProductorNombreApellido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ProductorEmail')); ?>:</b>
	<?php echo CHtml::encode($data->ProductorEmail); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ProductorDireccion')); ?>:</b>
	<?php echo CHtml::encode($data->ProductorDireccion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ProductorIdLocalidad')); ?>:</b>
	<?php echo CHtml::encode($data->ProductorIdLocalidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ProductorTelefono')); ?>:</b>
	<?php echo CHtml::encode($data->ProductorTelefono); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdProfesional')); ?>:</b>
	<?php echo CHtml::encode($data->IdProfesional); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ProfesionalNombreApellido')); ?>:</b>
	<?php echo CHtml::encode($data->ProfesionalNombreApellido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ProfesionalMatricula')); ?>:</b>
	<?php echo CHtml::encode($data->ProfesionalMatricula); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ProfesionalEmail')); ?>:</b>
	<?php echo CHtml::encode($data->ProfesionalEmail); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ProfesionalDireccion')); ?>:</b>
	<?php echo CHtml::encode($data->ProfesionalDireccion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ProfesionalIdLocalidad')); ?>:</b>
	<?php echo CHtml::encode($data->ProfesionalIdLocalidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ProfesionalTelefono')); ?>:</b>
	<?php echo CHtml::encode($data->ProfesionalTelefono); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdRecorredor')); ?>:</b>
	<?php echo CHtml::encode($data->IdRecorredor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('RecorredorNombreApellido')); ?>:</b>
	<?php echo CHtml::encode($data->RecorredorNombreApellido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('RecorredorTelefono')); ?>:</b>
	<?php echo CHtml::encode($data->RecorredorTelefono); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('UbicacionUmis')); ?>:</b>
	<?php echo CHtml::encode($data->UbicacionUmis); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('UbicacionHuerto')); ?>:</b>
	<?php echo CHtml::encode($data->UbicacionHuerto); ?>
	<br />

	*/ ?>

</div>