<?php
/* @var $this CuadernoController */
/* @var $model Cuaderno */

$this->breadcrumbs=array(
        $model->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $model->IdExplotacion),
	' Cuaderno #'.$model->NumeroInterno.' - '.$model->Temporada
);

$this->menu=array(
	array('label'=>'Modificar Cuaderno', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Eliminar Cuaderno', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Esta seguro de querer eliminar este item?')),
	);
$this->idCuaderno = $model->Id;
?>

<?php /* $this->widget('bootstrap.widgets.TbNav', array(
    'type' => TbHtml::NAV_TYPE_PILLS,
    'htmlOptions' => array('class'=>'menuDerecho'),
    
    'items' => array(
        array('label' => 'Inicio', 'url' => '#', 'active' => true),
        array('label' => 'Umi', 'url' => array('/plantacion/admin', 'idCuaderno'=>$model->Id)),
        array('label' => 'Cuadros/TRV', 'url' => array('/plantacion/admin', 'idCuaderno'=>$model->Id)),
        array('label' => 'Plantaciones', 'url' => array('/plantacion/admin', 'idCuaderno'=>$model->Id)),        
        array('label' => 'Ubicacion Huerto', 'url' => array('/plantacion/admin', 'idCuaderno'=>$model->Id)),
        array('label' => 'Ubicacion Umis', 'url' => array('/plantacion/admin', 'idCuaderno'=>$model->Id)),
        array('label' => 'Equipos', 'url' => array('/plantacion/admin', 'idCuaderno'=>$model->Id)),
        array('label' => 'Tratamientos', 'url' => array('/plantacion/admin', 'idCuaderno'=>$model->Id)),
        array('label' => 'Formulario A', 'url' => array('/plantacion/admin', 'idCuaderno'=>$model->Id)),
        array('label' => 'Capturas', 'url' => array('/plantacion/admin', 'idCuaderno'=>$model->Id)),
        array('label' => 'Muestreo', 'url' => array('/plantacion/admin', 'idCuaderno'=>$model->Id)),
        array('label' => 'Reg. Fenológicos', 'url' => array('/plantacion/admin', 'idCuaderno'=>$model->Id)),
        array('label' => 'Inspecciones', 'url' => array('/plantacion/admin', 'idCuaderno'=>$model->Id)),
        array('label' => 'Salidas', 'url' => array('/plantacion/admin', 'idCuaderno'=>$model->Id)),
        array('label' => 'Labores', 'url' => array('/plantacion/admin', 'idCuaderno'=>$model->Id)),
        array('label' => 'Permisos', 'url' => array('/plantacion/admin', 'idCuaderno'=>$model->Id)),
        array('label' => 'Costos', 'url' => array('/plantacion/admin', 'idCuaderno'=>$model->Id)),
    ),
)); */
?>

<h3><?php echo $model->NombreChacra.' - '.$model->Temporada.' - Cuaderno #'.$model->NumeroInterno; ?> </h3>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'Id',
		//'IdExplotacion',
		'NumeroRenspa',
		'NumeroCuaderno',
		'Temporada',
		'NombreChacra',
		'NumeroInterno',
		array('name'=>'idExplotacion.idLocalidad.Nombre', 'label'=>'Localidad'),
		'SuperficieTotal',
		'SuperficieNeta',
		//'IdProductor',
		'ProductorNombreApellido',
		//'ProductorEmail',
		//'ProductorDireccion',
		//'ProductorIdLocalidad',
		//'ProductorTelefono',
		//'IdProfesional',
                
		//'ProfesionalNombreApellido',
//		'ProfesionalMatricula',
//		'ProfesionalEmail',
//		'ProfesionalDireccion',
//		'ProfesionalIdLocalidad',
//		'ProfesionalTelefono',
//		'IdRecorredor',
		//'RecorredorNombreApellido',
//		'RecorredorTelefono',
//		'UbicacionUmis',
//		'UbicacionHuerto',
	),
)); ?>
