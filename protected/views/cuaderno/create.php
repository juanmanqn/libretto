<?php
/* @var $this CuadernoController */
/* @var $model Cuaderno */
/* @var $explotacion Explotacion */

$this->breadcrumbs=array(
	$explotacion->NombreChacra => array('/explotacion/view', 'id' => $explotacion->Id),
	'Nuevo Cuaderno',
);

$this->menu=array(
	array('label'=>'Listar Cuaderno', 'url'=>array('admin')),
);
?>

<h1>Nuevo Cuaderno</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>