<?php
/* @var $this CuadernoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cuadernos',
);

$this->menu=array(
	array('label'=>'Nuevo Cuaderno', 'url'=>array('create')),
	array('label'=>'Listar Cuadernos', 'url'=>array('admin')),
);
?>

<h1>Cuadernos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
