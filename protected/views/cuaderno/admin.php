<?php
/* @var $this CuadernoController */
/* @var $model Cuaderno */

$this->breadcrumbs=array(
	'Cuadernos'=>array('index'),
	'Listar',
);

$this->menu=array(	
	array('label'=>'Nuevo Cuaderno', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#cuaderno-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Cuadernos</h1>

<p>
Opcionalmente se pueden utilizar operadores de comparación (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
ó <b>=</b>) al principio de cada valor a buscar, para especificar como debe hacerse la comparación.
</p>

<?php echo CHtml::link('Búsqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => TbHtml::GRID_TYPE_STRIPED,
	'id'=>'cuaderno-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'Id',
		'IdExplotacion',
		'NumeroRenspa',
		'NumeroCuaderno',
		'Temporada',
		'NombreChacra',
		/*
		'NumeroInterno',
		array(
                    'name'=>'Localidad',
                    'value'=>'$data->idLocalidad ?  $data->idLocalidad->Nombre: "-";'
                ),
		'SuperficieTotal',
		'SuperficieNeta',
		'IdProductor',
		'ProductorNombreApellido',
		'ProductorEmail',
		'ProductorDireccion',
		'ProductorIdLocalidad',
		'ProductorTelefono',
		'IdProfesional',
		'ProfesionalNombreApellido',
		'ProfesionalMatricula',
		'ProfesionalEmail',
		'ProfesionalDireccion',
		'ProfesionalIdLocalidad',
		'ProfesionalTelefono',
		'IdRecorredor',
		'RecorredorNombreApellido',
		'RecorredorTelefono',
		'UbicacionUmis',
		'UbicacionHuerto',
		*/
		array(
			'class'=>'TbButtonColumn',
		),
	),
)); ?>
