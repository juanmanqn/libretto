<?php
/* @var $this CuadernoController */
/* @var $model Cuaderno */

$this->breadcrumbs=array(
        $model->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $model->IdExplotacion),
	'Modificar RENSPA: '.$model->NumeroRenspa,	
);

?>

<h1>Modificar Cuaderno <?php echo $model->Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>