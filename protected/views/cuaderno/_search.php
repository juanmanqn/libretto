<?php
/* @var $this CuadernoController */
/* @var $model Cuaderno */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'Id'); ?>
		<?php echo $form->textField($model,'Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdExplotacion'); ?>
		<?php echo $form->textField($model,'IdExplotacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NumeroRenspa'); ?>
		<?php echo $form->textField($model,'NumeroRenspa',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NumeroCuaderno'); ?>
		<?php echo $form->textField($model,'NumeroCuaderno'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Temporada'); ?>
		<?php echo $form->textField($model,'Temporada',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NombreChacra'); ?>
		<?php echo $form->textField($model,'NombreChacra',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NumeroInterno'); ?>
		<?php echo $form->textField($model,'NumeroInterno',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdLocalidad'); ?>
		<?php echo $form->textField($model,'IdLocalidad'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'SuperficieTotal'); ?>
		<?php echo $form->textField($model,'SuperficieTotal',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'SuperficieNeta'); ?>
		<?php echo $form->textField($model,'SuperficieNeta',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdProductor'); ?>
		<?php echo $form->textField($model,'IdProductor'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ProductorNombreApellido'); ?>
		<?php echo $form->textField($model,'ProductorNombreApellido',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ProductorEmail'); ?>
		<?php echo $form->textField($model,'ProductorEmail',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ProductorDireccion'); ?>
		<?php echo $form->textField($model,'ProductorDireccion',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ProductorIdLocalidad'); ?>
		<?php echo $form->textField($model,'ProductorIdLocalidad'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ProductorTelefono'); ?>
		<?php echo $form->textField($model,'ProductorTelefono',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdProfesional'); ?>
		<?php echo $form->textField($model,'IdProfesional'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ProfesionalNombreApellido'); ?>
		<?php echo $form->textField($model,'ProfesionalNombreApellido',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ProfesionalMatricula'); ?>
		<?php echo $form->textField($model,'ProfesionalMatricula',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ProfesionalEmail'); ?>
		<?php echo $form->textField($model,'ProfesionalEmail',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ProfesionalDireccion'); ?>
		<?php echo $form->textField($model,'ProfesionalDireccion',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ProfesionalIdLocalidad'); ?>
		<?php echo $form->textField($model,'ProfesionalIdLocalidad'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ProfesionalTelefono'); ?>
		<?php echo $form->textField($model,'ProfesionalTelefono',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdRecorredor'); ?>
		<?php echo $form->textField($model,'IdRecorredor'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'RecorredorNombreApellido'); ?>
		<?php echo $form->textField($model,'RecorredorNombreApellido',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'RecorredorTelefono'); ?>
		<?php echo $form->textField($model,'RecorredorTelefono',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'UbicacionUmis'); ?>
		<?php echo $form->textField($model,'UbicacionUmis',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'UbicacionHuerto'); ?>
		<?php echo $form->textField($model,'UbicacionHuerto',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row buttons">
		<?php echo TbHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->