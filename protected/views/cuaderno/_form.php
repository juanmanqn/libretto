<?php
/* @var $this CuadernoController */
/* @var $model Cuaderno */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cuaderno-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Campos con <span class="required">*</span> son obligatorios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'NumeroRenspa'); ?>
		<?php echo $form->textField($model,'NumeroRenspa',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'NumeroRenspa'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'NumeroCuaderno'); ?>
		<?php echo $form->textField($model,'NumeroCuaderno'); ?>
		<?php echo $form->error($model,'NumeroCuaderno'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Temporada'); ?>
		<?php echo $form->textField($model,'Temporada',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'Temporada'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'NumeroInterno'); ?>
		<?php echo $form->textField($model,'NumeroInterno',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'NumeroInterno'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'SuperficieTotal'); ?>
		<?php echo $form->textField($model,'SuperficieTotal',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'SuperficieTotal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'SuperficieNeta'); ?>
		<?php echo $form->textField($model,'SuperficieNeta',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'SuperficieNeta'); ?>
	</div>

	<div class="row">
		<?php // echo $form->labelEx($model,'ProductorNombreApellido'); ?>
		<?php // echo $form->textField($model,'ProductorNombreApellido',array('size'=>60,'maxlength'=>255)); ?>
		<?php // echo $form->error($model,'ProductorNombreApellido'); ?>
	</div>

	<div class="row">
		<?php // echo $form->labelEx($model,'ProductorEmail'); ?>
		<?php // echo $form->textField($model,'ProductorEmail',array('size'=>60,'maxlength'=>255)); ?>
		<?php // echo $form->error($model,'ProductorEmail'); ?>
	</div>

	<div class="row">
		<?php // echo $form->labelEx($model,'ProductorDireccion'); ?>
		<?php // echo $form->textField($model,'ProductorDireccion',array('size'=>60,'maxlength'=>255)); ?>
		<?php // echo $form->error($model,'ProductorDireccion'); ?>
	</div>

	<div class="row">
		<?php // echo $form->labelEx($model,'ProductorIdLocalidad'); ?>
		<?php // echo $form->textField($model,'ProductorIdLocalidad'); ?>
		<?php // echo $form->error($model,'ProductorIdLocalidad'); ?>
	</div>

	<div class="row">
		<?php // echo $form->labelEx($model,'ProductorTelefono'); ?>
		<?php // echo $form->textField($model,'ProductorTelefono',array('size'=>45,'maxlength'=>45)); ?>
		<?php // echo $form->error($model,'ProductorTelefono'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'IdProfesional'); ?>
		<?php echo $form->dropDownList($model, 'IdProfesional', CHtml::listData(Usuario::model()->findAll('idRol = 4'), 'Id', 'NombreApellido')); ?>
		<?php echo $form->error($model,'IdProfesional'); ?>
	</div>

	<div class="row">
		<?php // echo $form->labelEx($model,'ProfesionalNombreApellido'); ?>
		<?php // echo $form->textField($model,'ProfesionalNombreApellido',array('size'=>60,'maxlength'=>255)); ?>
		<?php // echo $form->error($model,'ProfesionalNombreApellido'); ?>
	</div>

	<div class="row">
		<?php // echo $form->labelEx($model,'ProfesionalMatricula'); ?>
		<?php // echo $form->textField($model,'ProfesionalMatricula',array('size'=>60,'maxlength'=>255)); ?>
		<?php // echo $form->error($model,'ProfesionalMatricula'); ?>
	</div>

	<div class="row">
		<?php // echo $form->labelEx($model,'ProfesionalEmail'); ?>
		<?php // echo $form->textField($model,'ProfesionalEmail',array('size'=>60,'maxlength'=>255)); ?>
		<?php // echo $form->error($model,'ProfesionalEmail'); ?>
	</div>

	<div class="row">
		<?php // echo $form->labelEx($model,'ProfesionalDireccion'); ?>
		<?php // echo $form->textField($model,'ProfesionalDireccion',array('size'=>60,'maxlength'=>255)); ?>
		<?php // echo $form->error($model,'ProfesionalDireccion'); ?>
	</div>

	<div class="row">
		<?php // echo $form->labelEx($model,'ProfesionalIdLocalidad'); ?>
		<?php // echo $form->textField($model,'ProfesionalIdLocalidad'); ?>
		<?php // echo $form->error($model,'ProfesionalIdLocalidad'); ?>
	</div>

	<div class="row">
		<?php // echo $form->labelEx($model,'ProfesionalTelefono'); ?>
		<?php // echo $form->textField($model,'ProfesionalTelefono',array('size'=>45,'maxlength'=>45)); ?>
		<?php // echo $form->error($model,'ProfesionalTelefono'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'IdRecorredor'); ?>
		<?php echo $form->dropDownList($model, 'IdRecorredor', CHtml::listData(Usuario::model()->findAll('idRol = 4'), 'Id', 'NombreApellido')); ?>
		<?php echo $form->error($model,'IdRecorredor'); ?>
	</div>

	<div class="row">
		<?php // echo $form->labelEx($model,'RecorredorNombreApellido'); ?>
		<?php // echo $form->textField($model,'RecorredorNombreApellido',array('size'=>60,'maxlength'=>255)); ?>
		<?php // echo $form->error($model,'RecorredorNombreApellido'); ?>
	</div>

	<div class="row">
		<?php // echo $form->labelEx($model,'RecorredorTelefono'); ?>
		<?php // echo $form->textField($model,'RecorredorTelefono',array('size'=>45,'maxlength'=>45)); ?>
		<?php // echo $form->error($model,'RecorredorTelefono'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'UbicacionUmis'); ?>
		<?php echo $form->fileField($model,'UbicacionUmis',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'UbicacionUmis'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'UbicacionHuerto'); ?>
		<?php echo $form->fileField($model,'UbicacionHuerto',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'UbicacionHuerto'); ?>
	</div>

	<div class="row buttons">
		<?php echo TbHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->