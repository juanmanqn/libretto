<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<!--<div class="col-md-4" style="">
	<div class="well" style="max-width: 300px; padding: 8px 0;">        
        <?php    
//            echo TbHtml::stackedPills(
//                array_merge(array(array('label'=>'Operaciones')), $this->menu)                  
//            );            
        ?>    
        </div> Sidebar 
</div>-->
<div class="col-md-8">
	<div id="content" class="well">
            <?php    
            echo TbHtml::nav(
                    TbHtml::NAV_TYPE_PILLS,
                    $this->menu,
                    array('class'=>'menuDerecho')
            );            
        ?>
            <div><?php echo $content; ?></div>		
	</div><!-- content -->
</div>
<?php $this->endContent(); ?>