<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />

        <!-- blueprint CSS framework -->
<!--        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />-->
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
        <![endif]-->

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
        <?php Yii::app()->bootstrap->register(); ?>        
        <style>
            body { //background-color: #f6f6f6 !important; }
        </style>

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>

        <?php if (Yii::app()->request->getParam("tab") != null): ?>
            <script>
                $(document).ready(function() {
                    $('#tabs a:contains("<?php echo CHttpRequest::getParam("tab"); ?>")').tab('show');
                });
            </script>
        <?php endif; ?>        
    </head>

    <body>
        <div style="height:40px"></div>
        <div class="container" id="content" role="main">
            <div id="menu">
                <?php
                $menuPrincipal = array(
                            'class' => 'bootstrap.widgets.TbNav',
                            'items' => array(
                                array('label' => 'Inicio', 'url' => array('/site/index')),
/*                                array('label' => 'Cuaderno', 'items' => array(
                                        array('label' => 'Listar', 'url' => array('/cuaderno/admin')),
                                        array('label' => 'Nuevo', 'url' => array('/cuaderno/create')),
                                    ), 'visible' => Yii::app()->user->checkAccess('administrador')),*/
                                array('label' => 'Explotaciones', 'url' => array('/explotacion/admin'),'visible' => Yii::app()->user->checkAccess('administrador')),
                              /*  array('label' => 'Cuadro', 'items' => array(
                                        array('label' => 'Listar', 'url' => array('/cuadro/admin')),
                                        array('label' => 'Nuevo', 'url' => array('/cuadro/create')),
                                    ), 'visible' => Yii::app()->user->checkAccess('administrador')),*/
//            array('label'=>'Umi', 'items' => array(
//                        array('label' => 'Listar', 'url' => array('/umi/admin')),
//                        array('label' => 'Nuevo', 'url' => array('/umi/create')),
//                ),'visible'=>Yii::app()->user->checkAccess('administrador')),
//            array('label'=>'Labores Culturales', 'items' => array(
//                        array('label' => 'Listar', 'url' => array('/laborCultural/admin')),
//                        array('label' => 'Nuevo', 'url' => array('/laborCultural/create')),
//                ),'visible'=>!Yii::app()->user->isGuest),
//            array('label'=>'Tratamientos', 'items' => array(
//                        array('label' => 'Listar', 'url' => array('/tratamientoFitosanitario/admin')),
//                        array('label' => 'Nuevo', 'url' => array('/tratamientoFitosanitario/create')),
//                ),'visible'=>!Yii::app()->user->isGuest),
//            array('label'=>'Participantes', 'items' => array(
//                    array('label' => 'Productores', 'url' => array('/productor/admin')),
//                    array('label' => 'Profesionales', 'url' => array('/profesional/admin')),
//                    array('label' => 'Cuentas', 'url' => array('#')),
//                ),'visible'=>Yii::app()->user->checkAccess('administrador')),
//                array('label'=>'Productores', 'items' => array(
//                            array('label' => 'Listar', 'url' => array('/productor/admin')),
//                            array('label' => 'Nuevo', 'url' => array('/productor/create')),
//                    ),'visible'=>Yii::app()->user->checkAccess('administrador')),
//                array('label'=>'Profesionales', 'items' => array(
//                            array('label' => 'Listar', 'url' => array('/profesional/admin')),
//                            array('label' => 'Nuevo', 'url' => array('/profesional/create')),
//                    ),'visible'=>Yii::app()->user->checkAccess('administrador')),
//                array('label'=>'Equipos', 'items' => array(
//                            array('label' => 'Listar', 'url' => array('/equipo/admin')),
//                            array('label' => 'Nuevo', 'url' => array('/equipo/create')),
//                    ),'visible'=>Yii::app()->user->checkAccess('administrador')),
                                array('label' => 'Admin', 'items' => array(
                                        array('label' => 'Usuarios', 'url' => array('/usuario/admin')),
                                       // array('label' => 'Roles', 'url' => array('/rol/admin')),
                                    ), 'visible' => Yii::app()->user->checkAccess('administrador')),
                                array('label' => 'Contacto', 'url' => array('/site/contact')),
                                array('label' => 'Ingresar', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
                                array('label' => 'Modificar Cuenta', 'url' => array('/usuario/modificarcuenta', 'id' => Yii::app()->user->Id),
                                    'visible' => !Yii::app()->user->isGuest),
                                array('label' => 'Cerrar Sesión (' . Yii::app()->user->name . ')', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest),
                            array('label' => 'Ayuda', 'url' => array('/site/ayuda')),
                            array('label' => 'Acerca de', 'url' => array('/site/about')),
                                ));
                
                $this->widget('bootstrap.widgets.TbNavbar', array(
                    'id' => 'tabs',
                    'brandLabel'=> '<img alt="libro" style="height:25px; width:25px" src="'.Yii::app()->request->baseUrl.'/images/libro_azul.png"></img>&nbsp;'.Yii::app()->name,
                    //'type' => TbHtml::NAV_TYPE_TABS,
                    //'color' => TbHtml::NAVBAR_COLOR_INVERSE,
                    'items' => array( $menuPrincipal
                        ),
                ));
                
                if(isset($this->idCuaderno)){    
                    
                    $menuCuaderno=array(
                        array('label' => 'Cuaderno', 'url' => array('/cuaderno/view', 'id'=>$this->idCuaderno)),
                        array('label' => 'UMI', 'url' => array('/umi/admin', 'idCuaderno'=>$this->idCuaderno)),
                        array('label' => 'Cuadros/TRV', 'url' => array('/cuadro/admin', 'idCuaderno'=>$this->idCuaderno)),
                        array('label' => 'Plantaciones', 'url' => array('/plantacion/admin', 'idCuaderno'=>$this->idCuaderno)),        
                        array('label' => 'Auxiliares', 'items'=> array(
                            array('label' => 'Tractoristas', 'url' => array('/tractorista/admin', 'idCuaderno'=>$this->idCuaderno)),
                            array('label'=>'Tipo Tratamiento','url'=>array('/tratamiento/admin', 'idCuaderno'=>$this->idCuaderno)),
                            array('label'=>'Tipo Labor','url'=>array('/tipoLabor/admin', 'idCuaderno'=>$this->idCuaderno)),
                            array('label'=>'Destinos','url'=>array('/destino/admin', 'idCuaderno'=>$this->idCuaderno))
                        )),
                        array('label' => 'Ubicacion Huerto', 'url' => array('/cuaderno/descargar', 'idCuaderno'=>$this->idCuaderno,'tipo'=>'Huerto')),
                        array('label' => 'Ubicacion UMIs', 'url' => array('/cuaderno/descargar', 'idCuaderno'=>$this->idCuaderno,'tipo'=>'Umis')),
                        
                        array('label' => 'Equipos', 'url' => array('/equipo/admin', 'idCuaderno'=>$this->idCuaderno)),
                        array('label' => 'Tratamientos', 'url' => array('/tratamientoFitosanitario/admin', 'idCuaderno'=>$this->idCuaderno)),
                        array('label' => 'Formulario A', 'url' => array('/formularioA/admin', 'idCuaderno'=>$this->idCuaderno)),
                        array('label' => 'Capturas', 'url' => array('/capturaCarpocapsa/admin', 'idCuaderno'=>$this->idCuaderno)),
                        array('label' => 'Muestreo', 'url' => array('/muestreoDanioCarpocapsa/admin', 'idCuaderno'=>$this->idCuaderno)),
                        array('label' => 'Reg. Fenológicos', 'url' => array('/registrosFenologicos/admin', 'idCuaderno'=>$this->idCuaderno)),
                        array('label' => 'Inspecciones', 'url' => array('/visitaInspeccion/admin', 'idCuaderno'=>$this->idCuaderno)),
                        array('label' => 'Salidas', 'url' => array('/salidaFruta/admin', 'idCuaderno'=>$this->idCuaderno)),
                        array('label' => 'Labores', 'url' => array('/laborCultural/admin', 'idCuaderno'=>$this->idCuaderno)),
                        array('label' => 'Permisos', 'url' => array('/permisos/admin', 'idCuaderno'=>$this->idCuaderno)),
                        //array('label' => 'Imprimir', 'url' => array('/imprimir/index', 'idCuaderno'=>$this->idCuaderno)),
                        
                        
                    );
                    
                    $this->widget('bootstrap.widgets.TbNav', array(
                       // 'id' => 'tabs',                        
                        'type' => TbHtml::NAV_TYPE_PILLS,                        
                        'items' =>  $menuCuaderno,
                    ));
                }     
                
                ?>
            </div><!-- mainmenu -->
        
            <div id="breadcrumbs">
                <?php if (isset($this->breadcrumbs)): ?>
                    <?php
                    $this->widget('bootstrap.widgets.TbBreadcrumb', array(
                        'links' => $this->breadcrumbs,
                    ));
                    ?> 
                <?php endif ?>
            </div>
            <div class="">
                
            </div>
        <?php echo $content; ?>

            <div class="clear"></div>

            <div id="footer">
                <div id="logo">
                    <div style="margin:auto;">
                        <small style="color: #cccccc">Cuaderno Fitosanitario</small>
                    </div>
                    <br/>
                    <div style="margin:auto; max-width:100%;">
                        <img alt="logos" src="<?php echo Yii::app()->request->baseUrl; ?>/images/logos_aqui.png"></img>
                    </div>
                </div>
                <hr/>
                GNU GPLv3 <?php echo date('Y'); ?><br/>
                por JMR y PK<br/>
            </div><!-- footer -->

        </div><!-- page -->

    </body>
</html>