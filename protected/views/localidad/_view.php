<?php
/* @var $this LocalidadController */
/* @var $data Localidad */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Id), array('view', 'id'=>$data->Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Nombre')); ?>:</b>
	<?php echo CHtml::encode($data->Nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdProvincia')); ?>:</b>
	<?php echo CHtml::encode($data->IdProvincia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CodigoPostal')); ?>:</b>
	<?php echo CHtml::encode($data->CodigoPostal); ?>
	<br />


</div>