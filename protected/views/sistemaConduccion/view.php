<?php
/* @var $this SistemaConduccionController */
/* @var $model SistemaConduccion */

$this->breadcrumbs=array(
	'Sistema Conduccions'=>array('index'),
	$model->Id,
);

$this->menu=array(
	array('label'=>'Listar SistemaConduccion', 'url'=>array('admin')),
	array('label'=>'Nuevo SistemaConduccion', 'url'=>array('create')),
	array('label'=>'Modificar SistemaConduccion', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Eliminar SistemaConduccion', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Esta seguro de querer eliminar este item?')),
	);
?>

<h1>SistemaConduccion #<?php echo $model->Id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'Nombre',
		'Codigo',
	),
)); ?>
