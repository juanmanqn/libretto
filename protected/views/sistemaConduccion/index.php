<?php
/* @var $this SistemaConduccionController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Sistema Conduccions',
);

$this->menu=array(
	array('label'=>'Nuevo SistemaConduccion', 'url'=>array('create')),
	array('label'=>'Listar SistemaConduccion', 'url'=>array('admin')),
);
?>

<h1>Sistema Conduccions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
