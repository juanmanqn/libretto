<?php
/* @var $this SistemaConduccionController */
/* @var $model SistemaConduccion */

$this->breadcrumbs=array(
	'Sistema Conduccions'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Listar SistemaConduccion', 'url'=>array('admin')),
);
?>

<h1>Nuevo SistemaConduccion</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>