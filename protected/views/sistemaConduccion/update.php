<?php
/* @var $this SistemaConduccionController */
/* @var $model SistemaConduccion */

$this->breadcrumbs=array(
	'Sistema Conduccions'=>array('index'),
	$model->Id. ' '=>array('view','id'=>$model->Id),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Nuevo SistemaConduccion', 'url'=>array('create')),
	array('label'=>'Ver SistemaConduccion', 'url'=>array('view', 'id'=>$model->Id)),
	array('label'=>'Listar SistemaConduccion', 'url'=>array('admin')),
);
?>

<h1>Modificar SistemaConduccion <?php echo $model->Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>