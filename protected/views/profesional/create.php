<?php
/* @var $this ProfesionalController */
/* @var $model Profesional */

$this->breadcrumbs=array(
	'Profesionals'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Listar Profesional', 'url'=>array('admin')),
);
?>

<h1>Nuevo Profesional</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>