<?php
/* @var $this ProfesionalController */
/* @var $model Profesional */

$this->breadcrumbs=array(
	'Profesionals'=>array('index'),
	$model->Id,
);

$this->menu=array(
	array('label'=>'Listar Profesional', 'url'=>array('admin')),
	array('label'=>'Nuevo Profesional', 'url'=>array('create')),
	array('label'=>'Modificar Profesional', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Eliminar Profesional', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Esta seguro de querer eliminar este item?')),
	);
?>

<h1>Profesional #<?php echo $model->Id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'Matricula',
	),
)); ?>
