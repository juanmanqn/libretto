<?php
/* @var $this ProfesionalController */
/* @var $model Profesional */

$this->breadcrumbs=array(
	'Profesionals'=>array('index'),
	$model->Id. ' '=>array('view','id'=>$model->Id),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Nuevo Profesional', 'url'=>array('create')),
	array('label'=>'Ver Profesional', 'url'=>array('view', 'id'=>$model->Id)),
	array('label'=>'Listar Profesional', 'url'=>array('admin')),
);
?>

<h1>Modificar Profesional <?php echo $model->Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>