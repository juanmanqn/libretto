<?php
/* @var $this ProfesionalController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Profesionals',
);

$this->menu=array(
	array('label'=>'Nuevo Profesional', 'url'=>array('create')),
	array('label'=>'Listar Profesional', 'url'=>array('admin')),
);
?>

<h1>Profesionals</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
