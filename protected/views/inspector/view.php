<?php
/* @var $this InspectorController */
/* @var $model Inspector */

$this->breadcrumbs=array(
	'Inspectores'=>array('index'),
	$model->Id,
);

$this->menu=array(
	array('label'=>'Listar Inspectores', 'url'=>array('admin')),
	array('label'=>'Nuevo Inspector', 'url'=>array('create')),
	array('label'=>'Modificar Inspector', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Eliminar Inspector', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Esta seguro de querer eliminar este item?')),
);
?>

<h1>Inspector #<?php echo $model->Id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'Legajo',
	),
)); ?>
