<?php
/* @var $this InspectorController */
/* @var $data Inspector */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Id), array('view', 'id'=>$data->Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Legajo')); ?>:</b>
	<?php echo CHtml::encode($data->Legajo); ?>
	<br />


</div>