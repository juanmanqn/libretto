<?php
/* @var $this InspectorController */
/* @var $model Inspector */

$this->breadcrumbs=array(
	'Inspectors'=>array('index'),
	$model->Id. ' '=>array('view','id'=>$model->Id),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Nuevo Inspector', 'url'=>array('create')),
	array('label'=>'Ver Inspector', 'url'=>array('view', 'id'=>$model->Id)),
	array('label'=>'Listar Inspectores', 'url'=>array('admin')),
);
?>

<h1>Modificar Inspector <?php echo $model->Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>