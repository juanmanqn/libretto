<?php
/* @var $this InspectorController */
/* @var $model Inspector */

$this->breadcrumbs=array(
	'Inspectores'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Listar Inspectores', 'url'=>array('admin')),
);
?>

<h1>Nuevo Inspector</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>