<?php
/* @var $this InspectorController */
/* @var $model Inspector */

$this->breadcrumbs=array(
	'Inspectores'=>array('index'),
	'Listar',
);

$this->menu=array(	
	array('label'=>'Nuevo Inspector', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#inspector-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Inspectores</h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => TbHtml::GRID_TYPE_STRIPED,
	'id'=>'inspector-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'Id',
		'Legajo',
		array(
			'class'=>'TbButtonColumn',
		),
	),
)); ?>
