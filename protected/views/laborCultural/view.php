<?php
/* @var $this LaborCulturalController */
/* @var $model LaborCultural */

$this->breadcrumbs=array(
	$cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
        'Labores' => array('/laborCultural/admin', 'idCuaderno'=> $cuaderno->Id),
	$model->Id,
);

?>

<h1>Labor Cultural #<?php echo $model->Id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'idCuadro.NumeroCuadro',
		'idTipoLabor.Nombre',
		'Detalle',
		'Fecha',
		'Horas',
		'idEquipo.NumeroEquipo',
	),
)); ?>
