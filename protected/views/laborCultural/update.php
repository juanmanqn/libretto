<?php
/* @var $this LaborCulturalController */
/* @var $model LaborCultural */

$this->breadcrumbs=array(
	$cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
        'Labores' => array('/laborCultural/admin', 'idCuaderno'=> $cuaderno->Id),
	'Modificar',
);

?>

<h1>Modificar Labor Cultural <?php echo $model->Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'cuaderno'=>$cuaderno)); ?>