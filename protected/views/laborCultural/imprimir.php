<?php

ob_start();
$this->widget('ext.PdfGrid.EPDFGrid', array(
    'id'        => 'labores-pdf',
    'fileName'  => 'LaboresCulturales',//Nombre del archivo generado sin la extension pdf (.pdf)
    'dataProvider'  => $model->search(), //puede ser $model->search()
    'columns'   => array(     
        'Id',
        'idCuadro.NumeroCuadro',
        'idTipoLabor.Nombre',
        'Detalle',
        'Fecha',
        'Horas',
        array('header'=>'Costo',
               'value'=>'$data->getCosto()'),
        'idEquipo.NumeroEquipo',	
    ),
    'config'    => array(
        'title'     => 'Labores Culturales',
        'subTitle'  => 'Cuaderno: '.$cuaderno->NumeroRenspa,
        //'colWidths' => array(40, 90, 40, 70),
    ),
));   
ob_end_flush(); 

