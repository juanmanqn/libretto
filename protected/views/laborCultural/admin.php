<?php
/* @var $this LaborCulturalController */
/* @var $model LaborCultural */

$this->breadcrumbs=array(
	$cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Labores Culturales',
);

$this->menu=array(	
	array('label'=>'Nueva Labor Cultural', 'url'=>array('create', 'idCuaderno'=> $cuaderno->Id)),
        array('label'=>'Imprimir', 'url'=>array('imprimir', 'idCuaderno'=> $cuaderno->Id)),
);
$this->idCuaderno = $cuaderno->Id;
?>

<h1>Labores Culturales</h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => TbHtml::GRID_TYPE_STRIPED,
	'id'=>'labor-cultural-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'Id',
		'idCuadro.NumeroCuadro',
		'idTipoLabor.Nombre',
		'Detalle',
		'Fecha',
		'Horas',
                array('header'=>'Gasto',
                       'value'=>'$data->getCosto()'),
		'idEquipo.NumeroEquipo',		
		array(
			'class'=>'TbButtonColumn',
		),
	),
)); ?>
