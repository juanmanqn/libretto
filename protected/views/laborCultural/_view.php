<?php
/* @var $this LaborCulturalController */
/* @var $data LaborCultural */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Id), array('view', 'id'=>$data->Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdCuadro')); ?>:</b>
	<?php echo CHtml::encode($data->IdCuadro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdTipoLabor')); ?>:</b>
	<?php echo CHtml::encode($data->IdTipoLabor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Detalle')); ?>:</b>
	<?php echo CHtml::encode($data->Detalle); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Fecha')); ?>:</b>
	<?php echo CHtml::encode($data->Fecha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Horas')); ?>:</b>
	<?php echo CHtml::encode($data->Horas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdEquipo')); ?>:</b>
	<?php echo CHtml::encode($data->IdEquipo); ?>
	<br />


</div>