<?php
/* @var $this LaborCulturalController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Labor Culturals',
);

$this->menu=array(
	array('label'=>'Nuevo LaborCultural', 'url'=>array('create')),
	array('label'=>'Listar LaborCultural', 'url'=>array('admin')),
);
?>

<h1>Labor Culturals</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
