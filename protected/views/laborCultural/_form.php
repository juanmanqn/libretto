<?php
/* @var $this LaborCulturalController */
/* @var $model LaborCultural */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'labor-cultural-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son obligatorios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
            <?php 
                if ($model->isNewRecord){
                    $umiId = 0;
                }
                else {
                    $umiId = $model->idCuadro->IdUmi;
                }
            ?>
            <?php echo $form->labelEx($model, 'Umi'); ?>
            <?php
                $items = CHtml::listData(Umi::model()->findAll('IdCuaderno=:IdCuaderno', array(':IdCuaderno'=>$cuaderno->Id)), 'Id', 'Codigo');
                echo CHtml::DropDownList('idUmi',$umiId, $items, array('id' => 'IdUmi', 'prompt' => 'Seleccione una opción'));
            ?>
        </div>

        <div class="row">
            <?php 
                if ($model->isNewRecord){
                    $arrayCuadro = array();
                }
                else {
                    $arrayCuadro = CHtml::listData(Cuadro::model()->findAll('IdUmi=:IdUmi',array(':IdUmi'=>$model->idCuadro->IdUmi)),'Id','NumeroCuadro');
                }
            ?>
            <?php echo $form->labelEx($model, 'IdCuadro'); ?>   
            <?php echo $form->dropDownList($model, 'IdCuadro', $arrayCuadro, array('empty'=>'Seleccionar..')); ?>
            <?php ECascadeDropDown::master('IdUmi')->setDependent('LaborCultural_IdCuadro', array('dependentLoadingLabel' => 'Cargando Cuadros...'), '/cuadro/data'); ?>
            <?php echo $form->error($model, 'IdCuadro'); ?>
        </div>

        <div class="row">
                <?php echo $form->labelEx($model,'IdTipoLabor'); ?>
		<?php echo $form->dropDownList($model,'IdTipoLabor',CHtml::listData(TipoLabor::model()->findAll(), 'Id', 'Nombre')); ?>
		<?php echo $form->error($model,'IdTipoLabor'); ?>
	</div>
        
	<div class="row">
		<?php echo $form->labelEx($model,'Detalle'); ?>
		<?php echo $form->textArea($model,'Detalle',array('size'=>60,'maxlength'=>1000)); ?>
		<?php echo $form->error($model,'Detalle'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Fecha'); ?>
		<?php
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model' => $model,
                        'attribute' => 'Fecha',
                        'language' => 'es', 
                        'options' => array(
                            'dateFormat' => 'yy-mm-dd',     // format of "2012-12-25" 
                        ),
                    ));
                ?> 
		<?php echo $form->error($model,'Fecha'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Horas'); ?>
		<?php echo $form->textField($model,'Horas'); ?>
		<?php echo $form->error($model,'Horas'); ?>
	</div>
        
        <div class="row">                
                <?php echo $form->labelEx($model,'IdEquipo'); ?>
		<?php echo $form->dropDownList($model,'IdEquipo',CHtml::listData(Equipo::model()->findAll('IdCuaderno=:IdCuaderno',
                        array(':IdCuaderno'=>$cuaderno->Id)), 'Id', 'NumeroEquipo'), array('empty'=>'Ningúno')); ?>
		<?php echo $form->error($model,'IdEquipo'); ?>
	</div>
        
	<div class="row buttons">
		<?php echo TbHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->