<?php
/* @var $this LogController */
/* @var $model Log */

$this->breadcrumbs=array(
	'Logs'=>array('index'),
	$model->Id,
);

$this->menu=array(
	array('label'=>'Listar Log', 'url'=>array('admin')),
	array('label'=>'Nuevo Log', 'url'=>array('create')),
	array('label'=>'Modificar Log', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Eliminar Log', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Esta seguro de querer eliminar este item?')),
	);
?>

<h1>Log #<?php echo $model->Id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'Detalle',
		'IdUsuario',
		'Fecha',
	),
)); ?>
