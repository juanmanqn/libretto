<?php
/* @var $this LogController */
/* @var $model Log */

$this->breadcrumbs=array(
	'Logs'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Listar Log', 'url'=>array('admin')),
);
?>

<h1>Nuevo Log</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>