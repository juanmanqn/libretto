<?php
/* @var $this LogController */
/* @var $model Log */

$this->breadcrumbs=array(
	'Logs'=>array('index'),
	$model->Id. ' '=>array('view','id'=>$model->Id),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Nuevo Log', 'url'=>array('create')),
	array('label'=>'Ver Log', 'url'=>array('view', 'id'=>$model->Id)),
	array('label'=>'Listar Log', 'url'=>array('admin')),
);
?>

<h1>Modificar Log <?php echo $model->Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>