<?php
/* @var $this TratamientoFitosanitarioController */
/* @var $model TratamientoFitosanitario */

$this->breadcrumbs=array(
	$cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
        'Tratamientos'=>array('tratamientoFitosanitario/admin', 'idCuaderno'=> $cuaderno->Id ),
	'Nuevo',
);

?>

<h1>Nuevo Tratamiento Fitosanitario</h1>

<?php $this->renderPartial('_form', array('model'=>$model,'cuaderno'=>$cuaderno)); ?>