<?php

ob_start();
$this->widget('ext.PdfGrid.EPDFGrid', array(
    'id'        => 'tratamiento-fitosanitario-pdf',
    'fileName'  => 'TratamientosFitosanitarios',//Nombre del archivo generado sin la extension pdf (.pdf)
    'dataProvider'  => $model->search(), //puede ser $model->search()
    'columns'   => array(     
                'Id',
		//'idEquipo.Nombre',
		//'IdTratamiento',
		'DosisMaquina',
		'idUnidadDosis.Unidad',		
		'TiempoCarencia',
		'TiempoReingreso',
		'idAplicador.NombreApellido',
		'idMotivo.Nombre',
		'idVariedad.Nombre',
                array('header'=>'Litros',
                    'value'=>'$data->getLitrosAplicaciones()'),
                array('header'=>'Costo',
                    'value'=>'$data->getCostoAplicaciones()'),
    ),
    'config'    => array(
        'title'     => 'Tratamientos Fitosanitarios',
        'subTitle'  => 'Cuaderno: '.$cuaderno->NumeroRenspa,
        //'colWidths' => array(40, 90, 40, 70),
    ),
));   
ob_end_flush(); 

