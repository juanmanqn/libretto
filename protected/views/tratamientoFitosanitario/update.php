<?php
/* @var $this TratamientoFitosanitarioController */
/* @var $model TratamientoFitosanitario */

$this->breadcrumbs=array(
        $cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Tratamientos'=>array('tratamientoFitosanitario/admin', 'idCuaderno'=> $cuaderno->Id),
	'Modificar #'.$model->Id,
);

?>

<h2>Modificar Tratamiento <?php echo $model->Id; ?></h2>

<?php $this->renderPartial('_form', array('model'=>$model,'cuaderno'=>$cuaderno)); ?>