<?php
/* @var $this TratamientoFitosanitarioController */
/* @var $model TratamientoFitosanitario */

$this->breadcrumbs=array(
        $cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Tratamientos'=>array('tratamientoFitosanitario/admin', 'idCuaderno'=> $cuaderno->Id),
	$model->Id,
);

$this->menu=array(
	array('label'=>'Nueva Aplicación', 'url'=>array('/aplicacionTratamiento/create', 'idTratamiento' => $model->Id)),
        array('label'=>'Imprimir Aplicaciones', 'url'=>array('/aplicacionTratamiento/imprimir', 'idCuaderno'=> $cuaderno->Id)),
        );
?>

<h2>Tratamiento Fitosanitario #<?php echo $model->Id; ?></h2>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'idEquipo.NumeroEquipo',
                'litrosAplicaciones',
                //'costoAplicaciones',
		'idTratamiento.NombreComercial',
		'DosisMaquina',
		'idUnidadDosis.Unidad',
		'TiempoCarencia',
		'TiempoReingreso',
		'idAplicador.NombreApellido',
		'idMotivo.Nombre',
		'idVariedad.Nombre',
		'Observaciones',
	),
)); ?>
<hr>
<h3>Aplicaciones Relacionadas</h3>

<?php 
    $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => TbHtml::GRID_TYPE_STRIPED,
	'id'=>'aplicacion-tratamiento-grid',
	'dataProvider'=>$aplicaciones->search(),
	'filter'=>$aplicaciones,
	'columns'=>array(
		'Id',
		'IdCuadro',
		'FechaHoraInicio',
		'FechaHoraFin',
		'LitrosCuadro',
		'FechaCumplimientoTC',
		'Clima',
		array(
			'class'=>'TbButtonColumn',
                        'template'=>'{view}{update}{delete}',
                        'buttons'=> array(
                            'view' => array(
                                'url'=>'Yii::app()->createUrl("/aplicacionTratamiento/view",array("id"=>$data->Id))'
                            ),
                            'update' => array(
                                'url'=>'Yii::app()->createUrl("/aplicacionTratamiento/update",array("id"=>$data->Id))'
                            ),
                            'delete' => array(
                                'url'=>'Yii::app()->createUrl("/aplicacionTratamiento/delete",array("id"=>$data->Id))'
                            )
                        )
		),
	),
)); 
    ?>

