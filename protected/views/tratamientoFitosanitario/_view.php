<?php
/* @var $this TratamientoFitosanitarioController */
/* @var $data TratamientoFitosanitario */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Id), array('view', 'id'=>$data->Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdOrden')); ?>:</b>
	<?php echo CHtml::encode($data->IdOrden); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdEquipo')); ?>:</b>
	<?php echo CHtml::encode($data->IdEquipo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdTratamiento')); ?>:</b>
	<?php echo CHtml::encode($data->IdTratamiento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DosisMaquina')); ?>:</b>
	<?php echo CHtml::encode($data->DosisMaquina); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdUnidadDosis')); ?>:</b>
	<?php echo CHtml::encode($data->IdUnidadDosis); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TiempoCarencia')); ?>:</b>
	<?php echo CHtml::encode($data->TiempoCarencia); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('TiempoReingreso')); ?>:</b>
	<?php echo CHtml::encode($data->TiempoReingreso); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdAplicador')); ?>:</b>
	<?php echo CHtml::encode($data->IdAplicador); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdMotivo')); ?>:</b>
	<?php echo CHtml::encode($data->IdMotivo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdVariedad')); ?>:</b>
	<?php echo CHtml::encode($data->IdVariedad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Observaciones')); ?>:</b>
	<?php echo CHtml::encode($data->Observaciones); ?>
	<br />

	*/ ?>

</div>