<?php
/* @var $this TratamientoFitosanitarioController */
/* @var $model TratamientoFitosanitario */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tratamiento-fitosanitario-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son obligatorios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'IdEquipo'); ?>
		<?php echo $form->dropDownList($model,'IdEquipo',CHtml::listData(Equipo::model()->findAll('IdCuaderno=:IdCuaderno',
                        array(':IdCuaderno'=>$cuaderno->Id)), 'Id', 'NumeroEquipo')); ?>
		<?php echo $form->error($model,'IdEquipo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'IdTratamiento'); ?>
                <?php echo $form->dropDownList($model,'IdTratamiento',CHtml::listData(Tratamiento::model()->findAll('IdCuaderno=:IdCuaderno',
                        array(':IdCuaderno'=>$cuaderno->Id)), 'Id', 'NombreComercial')); ?>
		<?php echo $form->error($model,'IdTratamiento'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'DosisMaquina'); ?>
		<?php echo $form->textField($model,'DosisMaquina',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'DosisMaquina'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'IdUnidadDosis'); ?>
                <?php echo $form->dropDownList($model,'IdUnidadDosis',CHtml::listData(UnidadDosis::model()->findAll(), 'Id', 'Unidad')); ?>
		<?php echo $form->error($model,'IdUnidadDosis'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TiempoCarencia'); ?>
		<?php echo $form->textField($model,'TiempoCarencia'); ?>
		<?php echo $form->error($model,'TiempoCarencia'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TiempoReingreso'); ?>
		<?php echo $form->textField($model,'TiempoReingreso'); ?>
		<?php echo $form->error($model,'TiempoReingreso'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'IdAplicador'); ?>
		<?php echo $form->dropDownList($model,'IdAplicador',CHtml::listData(Tractorista::model()->findAll('IdCuaderno=:IdCuaderno',
                        array(':IdCuaderno'=>$cuaderno->Id)), 'Id', 'NombreApellido')); ?>
		<?php echo $form->error($model,'IdAplicador'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'IdMotivo'); ?>
		<?php echo $form->dropDownList($model,'IdMotivo',CHtml::listData(Plaga::model()->findAll(), 'Id', 'Nombre')); ?>
		<?php echo $form->error($model,'IdMotivo'); ?>
	</div>

        <div class="row">
            <?php 
                if ($model->isNewRecord){
                    $especieId = 0;
                }
                else {
                    $especieId = $model->idVariedad->IdEspecie;
                }
            ?>
            <?php echo $form->labelEx($model, 'Especie'); ?>
            <?php $items2 = CHtml::listData(Especie::model()->findAll(), 'Id', 'Nombre'); ?>
            <?php echo CHtml::DropDownList('idEspecie',$especieId, $items2, array('id' => 'IdEspecie', 'prompt' => 'Seleccione una opción'));?>            
        </div>   
        
	<div class="row">
		<?php 
                    if ($model->isNewRecord){
                        $arrayVariedad = array();
                    }
                    else {
                        $arrayVariedad = CHtml::listData(Variedad::model()->findAll('IdEspecie=:IdEspecie',
                                array(':IdEspecie'=>$model->idVariedad->IdEspecie)),'Id','Nombre');
                    }
                ?>
		<?php echo $form->labelEx($model,'IdVariedad'); ?>
		<?php echo $form->dropDownList($model,'IdVariedad', $arrayVariedad, array('empty'=>'Seleccionar..')); ?>
                <?php ECascadeDropDown::master('IdEspecie')->setDependent('TratamientoFitosanitario_IdVariedad', array('dependentLoadingLabel' => 'Cargando Variedades...'), '/variedad/data'); ?>
		<?php echo $form->error($model,'IdVariedad'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Observaciones'); ?>
		<?php echo $form->textArea($model,'Observaciones',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'Observaciones'); ?>
	</div>

	<div class="row buttons">
		<?php echo TbHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->