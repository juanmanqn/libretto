<?php
/* @var $this TratamientoFitosanitarioController */
/* @var $model TratamientoFitosanitario */

$this->breadcrumbs=array(
	$cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Tratamientos',
);

$this->menu=array(	
	array('label'=>'Nuevo Tratamiento', 'url'=>array('create','idCuaderno'=> $cuaderno->Id)),
        array('label'=>'Imprimir', 'url'=>array('imprimir', 'idCuaderno'=> $cuaderno->Id)),
);
$this->idCuaderno = $cuaderno->Id;

?>

<h2>Tratamientos Fitosanitarios</h2>


<?php $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => TbHtml::GRID_TYPE_STRIPED,
	'id'=>'tratamiento-fitosanitario-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'Id',
		//'IdEquipo',
		//'IdTratamiento',
		'DosisMaquina',
		'idUnidadDosis.Unidad',		
		'TiempoCarencia',
		'TiempoReingreso',
		'idAplicador.NombreApellido',
		'idMotivo.Nombre',
		'idVariedad.Nombre',
                array('header'=>'Litros',
                    'value'=>'$data->getLitrosAplicaciones()'),
                //array('header'=>'Costo',
                //    'value'=>'$data->getCostoAplicaciones()'),
		//'Observaciones',		
		array(
			'class'=>'TbButtonColumn',
		),
	),
)); ?>
