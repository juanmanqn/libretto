<?php
/* @var $this TratamientoFitosanitarioController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tratamiento Fitosanitarios',
);

$this->menu=array(
	array('label'=>'Nuevo TratamientoFitosanitario', 'url'=>array('create')),
	array('label'=>'Listar TratamientoFitosanitario', 'url'=>array('admin')),
);
?>

<h1>Tratamiento Fitosanitarios</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
