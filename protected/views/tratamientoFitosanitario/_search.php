<?php
/* @var $this TratamientoFitosanitarioController */
/* @var $model TratamientoFitosanitario */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'Id'); ?>
		<?php echo $form->textField($model,'Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdOrden'); ?>
		<?php echo $form->textField($model,'IdOrden'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdEquipo'); ?>
		<?php echo $form->textField($model,'IdEquipo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdTratamiento'); ?>
		<?php echo $form->textField($model,'IdTratamiento'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DosisMaquina'); ?>
		<?php echo $form->textField($model,'DosisMaquina',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdUnidadDosis'); ?>
		<?php echo $form->textField($model,'IdUnidadDosis'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TiempoCarencia'); ?>
		<?php echo $form->textField($model,'TiempoCarencia'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TiempoReingreso'); ?>
		<?php echo $form->textField($model,'TiempoReingreso'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdAplicador'); ?>
		<?php echo $form->textField($model,'IdAplicador'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdMotivo'); ?>
		<?php echo $form->textField($model,'IdMotivo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdVariedad'); ?>
		<?php echo $form->textField($model,'IdVariedad'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Observaciones'); ?>
		<?php echo $form->textField($model,'Observaciones',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row buttons">
		<?php echo TbHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->