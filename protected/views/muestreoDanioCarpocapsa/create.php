<?php
/* @var $this MuestreoDanioCarpocapsaController */
/* @var $model MuestreoDanioCarpocapsa */

$this->breadcrumbs=array(
	$cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
        'Muestreos' => array('/muestreoDanioCarpocapsa/admin', 'idCuaderno'=> $cuaderno->Id),
	'Nuevo Muestreo',
);

?>

<h2>Nuevo Muestreo de Daños por Carpocapsa</h2>

<?php $this->renderPartial('_form', array('model'=>$model, 'cuaderno'=>$cuaderno)); ?>