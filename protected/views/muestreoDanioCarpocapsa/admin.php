<?php
/* @var $this MuestreoDanioCarpocapsaController */
/* @var $model MuestreoDanioCarpocapsa */
/* @var $cuaderno Cuaderno */

$this->breadcrumbs=array(
	$cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Muestreos Carpocapsa',
);

$this->menu=array(	
	array('label'=>'Nuevo Muestreo', 'url'=>array('create','idCuaderno'=> $cuaderno->Id)),
        array('label'=>'Imprimir', 'url'=>array('imprimir','idCuaderno'=> $cuaderno->Id)),
);
$this->idCuaderno = $cuaderno->Id;
?>

<h2>Muestreo de Daños por Carpocapsa</h2>

<?php
    $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => TbHtml::GRID_TYPE_STRIPED,
	'id'=>'muestreo-danio-carpocapsa-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'Id',
		'IdCuadro',
                array('header'=>'Variedad',
                      'value'=>'$data->idVariedad->Nombre'
                ),
		//'idVariedad.Nombre',
                array('header'=>'Superficie',
                      'value'=>'$data->idCuadro->getSuperficieVariedad($data->IdVariedad)'
                ),
		//'Superficie',
		'Fecha',
		'Muestra',		
		'FrutosDaniados',
		'PorcentajeDanio',
		'FechaCosecha',
		'MuestraCosecha',
		'FrutosDaniadosCosecha',
		'PorcentajeDanioCosecha',
		array(
			'class'=>'TbButtonColumn',
		),
	),
));
     
    
?>
