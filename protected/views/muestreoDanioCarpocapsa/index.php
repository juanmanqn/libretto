<?php
/* @var $this MuestreoDanioCarpocapsaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Muestreo Danio Carpocapsas',
);

$this->menu=array(
	array('label'=>'Nuevo MuestreoDanioCarpocapsa', 'url'=>array('create')),
	array('label'=>'Listar MuestreoDanioCarpocapsa', 'url'=>array('admin')),
);
?>

<h1>Muestreo Danio Carpocapsas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
