<?php
/* @var $this MuestreoDanioCarpocapsaController */
/* @var $model MuestreoDanioCarpocapsa */

$this->breadcrumbs=array(
	$cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
        'Muestreos Daños Carpocapsa' => array('muestreDanioCarpocapsa/admin', 'idCuaderno' => $cuaderno->Id),
	$model->Id,
);

$this->menu=array(
	array('label'=>'Modificar Muestreo', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Eliminar Muestreo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Esta seguro de querer eliminar este item?')),
	);
?>

<h2>Muestreo de Daño por Carpocapsa #<?php echo $model->Id; ?></h2>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'IdCuadro',
		'IdVariedad',
		'Superficie',
		'Fecha',
		'Muestra',
		'FrutosDaniados',
		'PorcentajeDanio',
		'FechaCosecha',
		'MuestraCosecha',
		'FrutosDaniadosCosecha',
		'PorcentajeDanioCosecha',
	),
)); ?>
