<?php
/* @var $this MuestreoDanioCarpocapsaController */
/* @var $model MuestreoDanioCarpocapsa */

$this->breadcrumbs=array(
	$cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Muestreos' => array('muestreoDanioCarpocapsa/admin', 'idCuaderno'=> $cuaderno->Id),
        'Modificar'
);

?>

<h2>Modificar Muestreo de Daños por Carpocapsa #<?php echo $model->Id; ?></h2>

<?php $this->renderPartial('_form', array('model'=>$model, 'cuaderno'=>$cuaderno)); ?>