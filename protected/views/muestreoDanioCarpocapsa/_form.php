<?php
/* @var $this MuestreoDanioCarpocapsaController */
/* @var $model MuestreoDanioCarpocapsa */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'muestreo-danio-carpocapsa-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son obligatorios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
            <?php 
                if ($model->isNewRecord){
                    $umiId = 0;
                }
                else {
                    $umiId = $model->idCuadro->IdUmi;
                }
            ?>
            <?php echo $form->labelEx($model, 'Umi'); ?>
            <?php
                $items = CHtml::listData(Umi::model()->findAll('IdCuaderno=:IdCuaderno', array(':IdCuaderno'=>$cuaderno->Id)), 'Id', 'Codigo');
                echo CHtml::DropDownList('idUmi',$umiId, $items, array('id' => 'IdUmi', 'prompt' => 'Seleccione una opción'));
            ?>
        </div>

        <div class="row">
            <?php 
                if ($model->isNewRecord){
                    $arrayCuadro = array();
                }
                else {
                    $arrayCuadro = CHtml::listData(Cuadro::model()->findAll('IdUmi=:IdUmi',array(':IdUmi'=>$model->idCuadro->IdUmi)),'Id','NumeroCuadro');
                }
            ?>
            <?php echo $form->labelEx($model, 'IdCuadro'); ?>   
            <?php echo $form->dropDownList($model, 'IdCuadro', $arrayCuadro, array('empty'=>'Seleccionar..')); ?>
            <?php ECascadeDropDown::master('IdUmi')->setDependent('MuestreoDanioCarpocapsa_IdCuadro', array('dependentLoadingLabel' => 'Cargando Cuadros...'), '/cuadro/data'); ?>
            <?php echo $form->error($model, 'IdCuadro'); ?>
        </div>

	<div class="row">
            <?php 
                if ($model->isNewRecord){
                    $especieId = 0;
                }
                else {
                    $especieId = $model->idVariedad->IdEspecie;
                }
            ?>
            <?php echo $form->labelEx($model, 'Especie'); ?>
            <?php $items2 = CHtml::listData(Especie::model()->findAll(), 'Id', 'Nombre'); ?>
            <?php echo CHtml::DropDownList('idEspecie',$especieId, $items2, array('id' => 'IdEspecie', 'prompt' => 'Seleccione una opción'));?>            
        </div>        
        
	<div class="row">
                <?php 
                    if ($model->isNewRecord){
                        $arrayVariedad = array();
                    }
                    else {
                        $arrayVariedad = CHtml::listData(Variedad::model()->findAll('IdEspecie=:IdEspecie',
                                array(':IdEspecie'=>$model->idVariedad->IdEspecie)),'Id','Nombre');
                    }
                ?>
		<?php echo $form->labelEx($model,'IdVariedad'); ?>
		<?php echo $form->dropDownList($model,'IdVariedad', $arrayVariedad, array('empty'=>'Seleccionar..')); ?>
                <?php ECascadeDropDown::master('IdEspecie')->setDependent('MuestreoDanioCarpocapsa_IdVariedad', array('dependentLoadingLabel' => 'Cargando Variedades...'), '/variedad/data'); ?>
		<?php echo $form->error($model,'IdVariedad'); ?>
	</div>

        <h3>1era Generación</h3>
        
	<div class="row">
		<?php echo $form->labelEx($model,'Fecha'); ?>
		<?php
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model' => $model,
                        'attribute' => 'Fecha',
                        'language' => 'es', 
                        'options' => array(
                            'dateFormat' => 'yy-mm-dd',     // format of "2012-12-25" 
                        ),
                    ));
                ?>   
		<?php echo $form->error($model,'Fecha'); ?>
	</div> 

	<div class="row">
		<?php echo $form->labelEx($model,'Muestra'); ?>
		<?php echo $form->textField($model,'Muestra'); ?>
		<?php echo $form->error($model,'Muestra'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'FrutosDaniados'); ?>
		<?php echo $form->textField($model,'FrutosDaniados'); ?>
		<?php echo $form->error($model,'FrutosDaniados'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'PorcentajeDanio'); ?>
		<?php echo $form->textField($model,'PorcentajeDanio',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'PorcentajeDanio'); ?>
	</div>

        <h3>Cosecha</h3>
        
	<div class="row">
		<?php echo $form->labelEx($model,'FechaCosecha'); ?>
		<?php
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model' => $model,
                        'attribute' => 'FechaCosecha',
                        'language' => 'es', 
                        'options' => array(
                            'dateFormat' => 'yy-mm-dd',     // format of "2012-12-25" 
                        ),
                    ));
                ?>   
		<?php echo $form->error($model,'FechaCosecha'); ?>
	</div> 

	<div class="row">
		<?php echo $form->labelEx($model,'MuestraCosecha'); ?>
		<?php echo $form->textField($model,'MuestraCosecha'); ?>
		<?php echo $form->error($model,'MuestraCosecha'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'FrutosDaniadosCosecha'); ?>
		<?php echo $form->textField($model,'FrutosDaniadosCosecha'); ?>
		<?php echo $form->error($model,'FrutosDaniadosCosecha'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'PorcentajeDanioCosecha'); ?>
		<?php echo $form->textField($model,'PorcentajeDanioCosecha',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'PorcentajeDanioCosecha'); ?>
	</div>

	<div class="row buttons">
		<?php echo TbHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->