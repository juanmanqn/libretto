<?php
/* @var $this MuestreoDanioCarpocapsaController */
/* @var $model MuestreoDanioCarpocapsa */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'Id'); ?>
		<?php echo $form->textField($model,'Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdCuadro'); ?>
		<?php echo $form->textField($model,'IdCuadro'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdVariedad'); ?>
		<?php echo $form->textField($model,'IdVariedad'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Superficie'); ?>
		<?php echo $form->textField($model,'Superficie',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Fecha'); ?>
		<?php echo $form->textField($model,'Fecha'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Muestra'); ?>
		<?php echo $form->textField($model,'Muestra'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'FrutosDaniados'); ?>
		<?php echo $form->textField($model,'FrutosDaniados'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'PorcentajeDanio'); ?>
		<?php echo $form->textField($model,'PorcentajeDanio',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'FechaCosecha'); ?>
		<?php echo $form->textField($model,'FechaCosecha'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'MuestraCosecha'); ?>
		<?php echo $form->textField($model,'MuestraCosecha'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'FrutosDaniadosCosecha'); ?>
		<?php echo $form->textField($model,'FrutosDaniadosCosecha'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'PorcentajeDanioCosecha'); ?>
		<?php echo $form->textField($model,'PorcentajeDanioCosecha',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row buttons">
		<?php echo TbHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->