<?php
/* @var $this MuestreoDanioCarpocapsaController */
/* @var $data MuestreoDanioCarpocapsa */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Id), array('view', 'id'=>$data->Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdCuadro')); ?>:</b>
	<?php echo CHtml::encode($data->IdCuadro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdVariedad')); ?>:</b>
	<?php echo CHtml::encode($data->IdVariedad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Superficie')); ?>:</b>
	<?php echo CHtml::encode($data->Superficie); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Fecha')); ?>:</b>
	<?php echo CHtml::encode($data->Fecha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Muestra')); ?>:</b>
	<?php echo CHtml::encode($data->Muestra); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FrutosDaniados')); ?>:</b>
	<?php echo CHtml::encode($data->FrutosDaniados); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('PorcentajeDanio')); ?>:</b>
	<?php echo CHtml::encode($data->PorcentajeDanio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FechaCosecha')); ?>:</b>
	<?php echo CHtml::encode($data->FechaCosecha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('MuestraCosecha')); ?>:</b>
	<?php echo CHtml::encode($data->MuestraCosecha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FrutosDaniadosCosecha')); ?>:</b>
	<?php echo CHtml::encode($data->FrutosDaniadosCosecha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PorcentajeDanioCosecha')); ?>:</b>
	<?php echo CHtml::encode($data->PorcentajeDanioCosecha); ?>
	<br />

	*/ ?>

</div>