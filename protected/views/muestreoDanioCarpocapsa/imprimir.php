<?php

ob_start();
            $this->widget('ext.PdfGrid.EPDFGrid', array(
                'id'        => 'muestreo-pdf',
                'fileName'  => 'Muestreos Carpocapsa',//Nombre del archivo generado sin la extension pdf (.pdf)
                'dataProvider'  => $model->search(), //puede ser $model->search()
                'columns'   => array(
                    'IdCuadro',
                    array('header'=>'Variedad',
                          'value'=>'$data->idVariedad->Nombre'
                    ),
                    //'idVariedad.Nombre',
                    array('header'=>'Superficie',
                          'value'=>'$data->idCuadro->getSuperficieVariedad($data->IdVariedad)'
                    ),
                    //'Superficie',
                    'Fecha',
                    'Muestra',		
                    'FrutosDaniados',
                    'PorcentajeDanio',
                    'FechaCosecha',
                    'MuestraCosecha',
                    'FrutosDaniadosCosecha',
                    'PorcentajeDanioCosecha',
                ),
                'config'    => array(
                    'title'     => 'Muestreo de Daños por Carpocapsa',
                    'subTitle'  => 'Cuaderno: '.$cuaderno->NumeroRenspa,
                    //'colWidths' => array(40, 90, 40, 70),
                ),
            ));   
            ob_end_flush(); 

