<?php
/* @var $this DestinoController */
/* @var $data Destino */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Id), array('view', 'id'=>$data->Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Destino')); ?>:</b>
	<?php echo CHtml::encode($data->Destino); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdCuaderno')); ?>:</b>
	<?php echo CHtml::encode($data->IdCuaderno); ?>
	<br />


</div>