<?php
/* @var $this DestinoController */
/* @var $model Destino */
/* @var $cuaderno Cuaderno */

$this->breadcrumbs=array(	
        $cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Destinos',
);
$this->idCuaderno=$cuaderno->Id;

$this->menu=array(	
	array('label'=>'Nuevo Destino', 'url'=>array('create', 'idCuaderno'=> $cuaderno->Id)),
);
?>

<h1>Destinos</h1>


<?php $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => TbHtml::GRID_TYPE_STRIPED,
	'id'=>'destino-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'Id',
		'Destino',
//		'IdCuaderno',
		array(
			'class'=>'TbButtonColumn',
                     'template'=>'{update}{delete}'
		),
	),
)); ?>
