<?php
/* @var $this DestinoController */
/* @var $model Destino */

$this->breadcrumbs=array(
	'Destinos'=>array('index'),
	$model->Id,
);

$this->menu=array(
	array('label'=>'Listar Destino', 'url'=>array('admin')),
	array('label'=>'Nuevo Destino', 'url'=>array('create')),
	array('label'=>'Modificar Destino', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Eliminar Destino', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Esta seguro de querer eliminar este item?')),
	);
?>

<h1>Destino #<?php echo $model->Id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'Destino',
		'IdCuaderno',
	),
)); ?>
