<?php
/* @var $this DestinoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Destinos',
);

$this->menu=array(
	array('label'=>'Nuevo Destino', 'url'=>array('create')),
	array('label'=>'Listar Destino', 'url'=>array('admin')),
);
?>

<h1>Destinos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
