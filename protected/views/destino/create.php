<?php
/* @var $this DestinoController */
/* @var $model Destino */
$this->breadcrumbs=array(
        $cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Destinos' => array('/destino/admin', 'idCuaderno'=> $cuaderno->Id),
        'Nuevo'
);

?>

<h1>Nuevo Destino</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>