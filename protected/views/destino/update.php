<?php
/* @var $this DestinoController */
/* @var $model Destino */

$this->breadcrumbs=array(
        $cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Destinos' => array('/destino/admin', 'idCuaderno'=> $cuaderno->Id),
        'Modificar #'.$model->Destino
);


?>

<h1>Modificar Destino <?php echo $model->Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>