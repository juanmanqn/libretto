<?php
/* @var $this TratamientoController */
/* @var $model Tratamiento */

$this->breadcrumbs=array(
	'Tratamientos'=>array('index'),
	$model->Id,
);

$this->menu=array(
	array('label'=>'Listar Tratamiento', 'url'=>array('admin')),
	array('label'=>'Nuevo Tratamiento', 'url'=>array('create')),
	array('label'=>'Modificar Tratamiento', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Eliminar Tratamiento', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Esta seguro de querer eliminar este item?')),
	);
?>

<h1>Tratamiento #<?php echo $model->Id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'NombreComercial',
		'PrincipioActivo',
		'Dosis',
		'IdUnidad',
	),
)); ?>
