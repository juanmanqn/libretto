<?php
/* @var $this TratamientoController */
/* @var $model Tratamiento */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tratamiento-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son obligatorios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'NombreComercial'); ?>
		<?php echo $form->textField($model,'NombreComercial',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'NombreComercial'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'PrincipioActivo'); ?>
		<?php echo $form->textField($model,'PrincipioActivo',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'PrincipioActivo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Dosis'); ?>
		<?php echo $form->textField($model,'Dosis',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'Dosis'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'IdUnidad'); ?>
                <?php echo $form->dropDownList($model,'IdUnidad',CHtml::listData(UnidadDosis::model()->findAll(), 'Id', 'Unidad')); ?>
		<?php echo $form->error($model,'IdUnidad'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'CostoPorLitro'); ?>
		<?php echo $form->textField($model,'CostoPorLitro',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'CostoPorLitro'); ?>
	</div>

	<div class="row buttons">
		<?php echo TbHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->