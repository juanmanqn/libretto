<?php
/* @var $this TratamientoController */
/* @var $data Tratamiento */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Id), array('view', 'id'=>$data->Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NombreComercial')); ?>:</b>
	<?php echo CHtml::encode($data->NombreComercial); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PrincipioActivo')); ?>:</b>
	<?php echo CHtml::encode($data->PrincipioActivo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Dosis')); ?>:</b>
	<?php echo CHtml::encode($data->Dosis); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdUnidad')); ?>:</b>
	<?php echo CHtml::encode($data->IdUnidad); ?>
	<br />


</div>