<?php
/* @var $this TratamientoController */
/* @var $model Tratamiento */

$this->breadcrumbs=array(
        $cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Tipos de Tratamientos' => array('tratamiento/admin', 'idCuaderno'=> $cuaderno->Id),
	'Modificar',
);

?>

<h2>Modificar Tipo de Tratamiento <?php echo $model->Id; ?></h2>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>