<?php
/* @var $this TratamientoController */
/* @var $model Tratamiento */

$this->breadcrumbs=array(
	$cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Tipos de Tratamientos',
);

$this->menu=array(	
	array('label'=>'Nuevo Tipo de Tratamiento', 'url'=>array('create', 'idCuaderno'=> $cuaderno->Id)),
);

$this->idCuaderno = $cuaderno->Id;

?>

<h1>Tipos de Tratamientos</h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => TbHtml::GRID_TYPE_STRIPED,
	'id'=>'tratamiento-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'Id',
		'NombreComercial',
		'PrincipioActivo',
		'Dosis',
		'idUnidad.Unidad',
                'CostoPorLitro',
		array(
			'class'=>'TbButtonColumn',
                        'template'=>'{update}{delete}'
		),
	),
)); ?>
