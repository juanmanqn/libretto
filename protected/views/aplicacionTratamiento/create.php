<?php
/* @var $this AplicacionTratamientoController */
/* @var $model AplicacionTratamiento */
/* @var $tratamiento TratamientoFitosanitario */

$this->breadcrumbs=array(
        $tratamiento->idCuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $tratamiento->idCuaderno->IdExplotacion),
	'RENSPA: '.$tratamiento->idCuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $tratamiento->idCuaderno->Id),
	'Tratamientos' => array('TratamientoFitosanitario/admin', 'idCuaderno'=> $tratamiento->IdCuaderno),
        'Aplicaciones' => array('TratamientoFitosanitario/view', 'id'=> $tratamiento->Id),
	'Nuevo',
);

?>

<h1>Nueva Aplicacion de Tratamiento</h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'cuaderno'=>$tratamiento->idCuaderno)); ?>