<?php
/* @var $this AplicacionTratamientoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Aplicacion Tratamientos',
);

$this->menu=array(
	array('label'=>'Nuevo AplicacionTratamiento', 'url'=>array('create')),
	array('label'=>'Listar AplicacionTratamiento', 'url'=>array('admin')),
);
?>

<h1>Aplicacion Tratamientos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
