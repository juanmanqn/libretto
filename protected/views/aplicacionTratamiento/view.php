<?php
/* @var $this AplicacionTratamientoController */
/* @var $model AplicacionTratamiento */

$this->breadcrumbs=array(
	'Aplicacion Tratamientos'=>array('index'),
	$model->Id,
);

$this->menu=array(
	array('label'=>'Modificar AplicacionTratamiento', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Eliminar AplicacionTratamiento', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Esta seguro de querer eliminar este item?')),
	);
?>

<h1>AplicacionTratamiento #<?php echo $model->Id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'IdCuadro',
		'IdTratamientoFitosanitario',
		'FechaHoraInicio',
		'FechaHoraFin',
		'LitrosCuadro',
		'FechaCumplimientoTC',
		'Clima',
	),
)); ?>
