<?php
/* @var $this AplicacionTratamientoController */
/* @var $model AplicacionTratamiento */

$this->breadcrumbs=array(
	'Aplicacion Tratamientos'=>array('index'),
	'Listar',
);

$this->menu=array(	
	array('label'=>'Nuevo AplicacionTratamiento', 'url'=>array('create')),
);

?>


<?php $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => TbHtml::GRID_TYPE_STRIPED,
	'id'=>'aplicacion-tratamiento-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'Id',
		'IdCuadro',
		'IdTratamientoFitosanitario',
		'FechaHoraInicio',
		'FechaHoraFin',
		'LitrosCuadro',
		/*
		'FechaCumplimientoTC',
		'Clima',
		*/
		array(
			'class'=>'TbButtonColumn',
		),
	),
)); ?>
