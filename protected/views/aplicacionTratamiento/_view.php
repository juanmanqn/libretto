<?php
/* @var $this AplicacionTratamientoController */
/* @var $data AplicacionTratamiento */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Id), array('view', 'id'=>$data->Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdCuadro')); ?>:</b>
	<?php echo CHtml::encode($data->IdCuadro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdTratamientoFitosanitario')); ?>:</b>
	<?php echo CHtml::encode($data->IdTratamientoFitosanitario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FechaHoraInicio')); ?>:</b>
	<?php echo CHtml::encode($data->FechaHoraInicio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FechaHoraFin')); ?>:</b>
	<?php echo CHtml::encode($data->FechaHoraFin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('LitrosCuadro')); ?>:</b>
	<?php echo CHtml::encode($data->LitrosCuadro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FechaCumplimientoTC')); ?>:</b>
	<?php echo CHtml::encode($data->FechaCumplimientoTC); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('Clima')); ?>:</b>
	<?php echo CHtml::encode($data->Clima); ?>
	<br />

	*/ ?>

</div>