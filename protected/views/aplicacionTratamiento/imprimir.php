<?php

ob_start();
$this->widget('ext.PdfGrid.EPDFGrid', array(
    'id'        => 'aplicaciones-tratamiento-fitosanitario-pdf',
    'fileName'  => 'AplicacionesTratamientosFitosanitarios',//Nombre del archivo generado sin la extension pdf (.pdf)
    'dataProvider'  => $model->search(), //puede ser $model->search()
    'columns'   => array(     
                'Id',
		'IdCuadro',
		'FechaHoraInicio',
		'FechaHoraFin',
		'LitrosCuadro',
		'FechaCumplimientoTC',
		'Clima',
    ),
    'config'    => array(
        'title'     => 'Aplicaciones de Tratamientos Fitosanitarios',
        'subTitle'  => 'Cuaderno: '.$cuaderno->NumeroRenspa,
        //'colWidths' => array(40, 90, 40, 70),
    ),
));   
ob_end_flush(); 

