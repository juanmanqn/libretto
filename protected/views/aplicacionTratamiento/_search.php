<?php
/* @var $this AplicacionTratamientoController */
/* @var $model AplicacionTratamiento */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'Id'); ?>
		<?php echo $form->textField($model,'Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdCuadro'); ?>
		<?php echo $form->textField($model,'IdCuadro'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdTratamientoFitosanitario'); ?>
		<?php echo $form->textField($model,'IdTratamientoFitosanitario'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'FechaHoraInicio'); ?>
		<?php echo $form->textField($model,'FechaHoraInicio'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'FechaHoraFin'); ?>
		<?php echo $form->textField($model,'FechaHoraFin'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'LitrosCuadro'); ?>
		<?php echo $form->textField($model,'LitrosCuadro'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'FechaCumplimientoTC'); ?>
		<?php echo $form->textField($model,'FechaCumplimientoTC'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Clima'); ?>
		<?php echo $form->textField($model,'Clima',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row buttons">
		<?php echo TbHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->