<?php
/* @var $this AplicacionTratamientoController */
/* @var $model AplicacionTratamiento */

$this->breadcrumbs=array(
	$cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Tratamientos' => array('TratamientoFitosanitario/admin', 'idCuaderno'=> $cuaderno->Id),
        'Aplicaciones' => array('TratamientoFitosanitario/view', 'id'=> $model->IdTratamientoFitosanitario),
	'Modificar',
);

?>

<h2>Modificar Aplicacion Tratamiento <?php echo $model->Id; ?></h2>

<?php $this->renderPartial('_form', array('model'=>$model,'cuaderno'=>$cuaderno)); ?>