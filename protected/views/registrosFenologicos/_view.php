<?php
/* @var $this RegistrosFenologicosController */
/* @var $data RegistrosFenologicos */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Id), array('view', 'id'=>$data->Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdCuaderno')); ?>:</b>
	<?php echo CHtml::encode($data->IdCuaderno); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdVariedad')); ?>:</b>
	<?php echo CHtml::encode($data->IdVariedad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('InicioFloracion')); ?>:</b>
	<?php echo CHtml::encode($data->InicioFloracion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PlenaFloracion')); ?>:</b>
	<?php echo CHtml::encode($data->PlenaFloracion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CaidaPetalos')); ?>:</b>
	<?php echo CHtml::encode($data->CaidaPetalos); ?>
	<br />


</div>