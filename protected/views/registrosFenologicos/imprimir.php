<?php

ob_start();
$this->widget('ext.PdfGrid.EPDFGrid', array(
    'id'        => 'registros-fenologicos-pdf',
    'fileName'  => 'RegistrosFenologicos',//Nombre del archivo generado sin la extension pdf (.pdf)
    'dataProvider'  => $model->search(), //puede ser $model->search()
    'columns'   => array(     
        'Id',
        //'IdCuaderno',
        'idVariedad.idEspecie.Nombre',
        'idVariedad.Nombre',
        'InicioFloracion',
        'PlenaFloracion',
        'CaidaPetalos',
    ),
    'config'    => array(
        'title'     => 'Registros Fenologicos',
        'subTitle'  => 'Cuaderno: '.$cuaderno->NumeroRenspa,
        //'colWidths' => array(40, 90, 40, 70),
    ),
));   
ob_end_flush(); 

