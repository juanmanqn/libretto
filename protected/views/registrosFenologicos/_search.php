<?php
/* @var $this RegistrosFenologicosController */
/* @var $model RegistrosFenologicos */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'Id'); ?>
		<?php echo $form->textField($model,'Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdCuaderno'); ?>
		<?php echo $form->textField($model,'IdCuaderno'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdVariedad'); ?>
		<?php echo $form->textField($model,'IdVariedad'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'InicioFloracion'); ?>
		<?php echo $form->textField($model,'InicioFloracion'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'PlenaFloracion'); ?>
		<?php echo $form->textField($model,'PlenaFloracion'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'CaidaPetalos'); ?>
		<?php echo $form->textField($model,'CaidaPetalos'); ?>
	</div>

	<div class="row buttons">
		<?php echo TbHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->