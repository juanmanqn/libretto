<?php
/* @var $this RegistrosFenologicosController */
/* @var $model RegistrosFenologicos */

$this->breadcrumbs=array(
	$cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
        'Registros Fenológicos' => array('/registrosFenologicos/admin', 'idCuaderno'=> $cuaderno->Id),
	'Modificar',
);

?>

<h1>Modificar Registro Fenológico <?php echo $model->Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'cuaderno'=>$cuaderno)); ?>