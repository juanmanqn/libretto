<?php
/* @var $this RegistrosFenologicosController */
/* @var $model RegistrosFenologicos */

$this->breadcrumbs=array(
	$cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
        'Registros Fenológicos'
);

$this->menu=array(	
	array('label'=>'Nuevo Registro Fenológico', 'url'=>array('create', 'idCuaderno'=> $cuaderno->Id)),
        array('label'=>'Imprimir', 'url'=>array('imprimir', 'idCuaderno'=> $cuaderno->Id)),
);

$this->idCuaderno = $cuaderno->Id;
?>

<h1>Registros Fenológicos</h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => TbHtml::GRID_TYPE_STRIPED,
	'id'=>'registros-fenologicos-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'Id',
		//'IdCuaderno',
                'idVariedad.idEspecie.Nombre',
		'idVariedad.Nombre',
		'InicioFloracion',
		'PlenaFloracion',
		'CaidaPetalos',
		array(
			'class'=>'TbButtonColumn',
                        'template'=>'{update}{delete}'
		),
	),
)); ?>
