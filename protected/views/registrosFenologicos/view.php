<?php
/* @var $this RegistrosFenologicosController */
/* @var $model RegistrosFenologicos */

$this->breadcrumbs=array(
	'Registros Fenologicoses'=>array('index'),
	$model->Id,
);

$this->menu=array(
	array('label'=>'Listar RegistrosFenologicos', 'url'=>array('admin')),
	array('label'=>'Nuevo RegistrosFenologicos', 'url'=>array('create')),
	array('label'=>'Modificar RegistrosFenologicos', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Eliminar RegistrosFenologicos', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Esta seguro de querer eliminar este item?')),
	);
?>

<h1>RegistrosFenologicos #<?php echo $model->Id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'IdCuaderno',
		'IdVariedad',
		'InicioFloracion',
		'PlenaFloracion',
		'CaidaPetalos',
	),
)); ?>
