<?php
/* @var $this RegistrosFenologicosController */
/* @var $model RegistrosFenologicos */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'registros-fenologicos-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son obligatorios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
            <?php 
                if ($model->isNewRecord){
                    $especieId = 0;
                }
                else {
                    $especieId = $model->idVariedad->IdEspecie;
                }
            ?>
            <?php echo $form->labelEx($model, 'Especie'); ?>
            <?php $items2 = CHtml::listData(Especie::model()->findAll(), 'Id', 'Nombre'); ?>
            <?php echo CHtml::DropDownList('idEspecie',$especieId, $items2, array('id' => 'IdEspecie', 'prompt' => 'Seleccione una opción'));?>            
        </div>   
        
	<div class="row">
		<?php 
                    if ($model->isNewRecord){
                        $arrayVariedad = array();
                    }
                    else {
                        $arrayVariedad = CHtml::listData(Variedad::model()->findAll('IdEspecie=:IdEspecie',
                                array(':IdEspecie'=>$model->idVariedad->IdEspecie)),'Id','Nombre');
                    }
                ?>
		<?php echo $form->labelEx($model,'IdVariedad'); ?>
		<?php echo $form->dropDownList($model,'IdVariedad', $arrayVariedad, array('empty'=>'Seleccionar..')); ?>
                <?php ECascadeDropDown::master('IdEspecie')->setDependent('RegistrosFenologicos_IdVariedad', array('dependentLoadingLabel' => 'Cargando Variedades...'), '/variedad/data'); ?>
		<?php echo $form->error($model,'IdVariedad'); ?>
	</div>

        <div class="row">
		<?php echo $form->labelEx($model,'InicioFloracion'); ?>		            
                <?php
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model' => $model,
                        'attribute' => 'InicioFloracion',
                        'language' => 'es',
                        'options' => array(
                            'dateFormat' => 'yy-mm-dd',     // format of "2012-12-25" 
                        ),
                    ));
                ?>            
		<?php echo $form->error($model,'InicioFloracion'); ?>
	</div>

        <div class="row">
		<?php echo $form->labelEx($model,'PlenaFloracion'); ?>		            
                <?php
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model' => $model,
                        'attribute' => 'PlenaFloracion',
                        'language' => 'es',
                        'options' => array(
                            'dateFormat' => 'yy-mm-dd',     // format of "2012-12-25" 
                        ),
                    ));
                ?>            
		<?php echo $form->error($model,'PlenaFloracion'); ?>
	</div>
        
	<div class="row">
		<?php echo $form->labelEx($model,'CaidaPetalos'); ?>		            
                <?php
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model' => $model,
                        'attribute' => 'CaidaPetalos',
                        'language' => 'es',
                        'options' => array(
                            'dateFormat' => 'yy-mm-dd',     // format of "2012-12-25" 
                        ),
                    ));
                ?>            
		<?php echo $form->error($model,'CaidaPetalos'); ?>
	</div>
        
	<div class="row buttons">
		<?php echo TbHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->