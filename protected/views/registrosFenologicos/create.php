<?php
/* @var $this RegistrosFenologicosController */
/* @var $model RegistrosFenologicos */

$this->breadcrumbs=array(
	$cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
        'Registros Fenológicos' => array('/registrosFenologicos/admin', 'idCuaderno'=> $cuaderno->Id),
	'Nuevo',
);

?>

<h1>Nuevo Registro Fenológico</h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'cuaderno'=>$cuaderno)); ?>