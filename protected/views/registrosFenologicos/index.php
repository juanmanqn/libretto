<?php
/* @var $this RegistrosFenologicosController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Registros Fenologicoses',
);

$this->menu=array(
	array('label'=>'Nuevo RegistrosFenologicos', 'url'=>array('create')),
	array('label'=>'Listar RegistrosFenologicos', 'url'=>array('admin')),
);
?>

<h1>Registros Fenologicoses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
