<?php
/* @var $this RolController */
/* @var $model Rol */

$this->breadcrumbs=array(
	'Roles'=>array('index'),
	$model->Id=>array('view','id'=>$model->Id),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Listar Roles', 'url'=>array('admin')),
	array('label'=>'Nuevo Rol', 'url'=>array('create')),
	array('label'=>'Ver Rol', 'url'=>array('view', 'id'=>$model->Id)),
);
?>

<h1>Modificar Rol <?php echo $model->Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>