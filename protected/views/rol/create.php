<?php
/* @var $this RolController */
/* @var $model Rol */

$this->breadcrumbs=array(
	'Roles'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Listar Roles', 'url'=>array('admin')),
);
?>

<h1>Nuevo Rol</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>