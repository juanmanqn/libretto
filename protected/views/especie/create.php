<?php
/* @var $this EspecieController */
/* @var $model Especie */

$this->breadcrumbs=array(
	'Especies'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Listar Especie', 'url'=>array('admin')),
);
?>

<h1>Nuevo Especie</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>