<?php
/* @var $this EspecieController */
/* @var $model Especie */

$this->breadcrumbs=array(
	'Especies'=>array('index'),
	$model->Id. ' '=>array('view','id'=>$model->Id),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Nuevo Especie', 'url'=>array('create')),
	array('label'=>'Ver Especie', 'url'=>array('view', 'id'=>$model->Id)),
	array('label'=>'Listar Especie', 'url'=>array('admin')),
);
?>

<h1>Modificar Especie <?php echo $model->Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>