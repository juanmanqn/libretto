<?php
/* @var $this EspecieController */
/* @var $model Especie */

$this->breadcrumbs=array(
	'Especies'=>array('index'),
	$model->Id,
);

$this->menu=array(
	array('label'=>'Listar Especie', 'url'=>array('admin')),
	array('label'=>'Nuevo Especie', 'url'=>array('create')),
	array('label'=>'Modificar Especie', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Eliminar Especie', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Esta seguro de querer eliminar este item?')),
	);
?>

<h1>Especie #<?php echo $model->Id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'Nombre',
		'Codigo',
	),
)); ?>
