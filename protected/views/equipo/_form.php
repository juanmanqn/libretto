<?php
/* @var $this EquipoController */
/* @var $model Equipo */
/* @var $form CActiveForm */
/* @var $cuaderno Cuaderno */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'equipo-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son obligatorios.</p>

	<?php echo $form->errorSummary($model); ?>

	
	<div class="row">
		<?php echo $form->labelEx($model,'NumeroEquipo'); ?>
		<?php echo $form->textField($model,'NumeroEquipo'); ?>
		<?php echo $form->error($model,'NumeroEquipo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TractorMarca'); ?>
		<?php echo $form->textField($model,'TractorMarca',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'TractorMarca'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TractorModelo'); ?>
		<?php echo $form->textField($model,'TractorModelo',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'TractorModelo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TractorAnio'); ?>
		<?php echo $form->textField($model,'TractorAnio'); ?>
		<?php echo $form->error($model,'TractorAnio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TractorPotencia'); ?>
		<?php echo $form->textField($model,'TractorPotencia'); ?>
		<?php echo $form->error($model,'TractorPotencia'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'PulverizadoraMarca'); ?>
		<?php echo $form->textField($model,'PulverizadoraMarca',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'PulverizadoraMarca'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'PulverizadoraModelo'); ?>
		<?php echo $form->textField($model,'PulverizadoraModelo',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'PulverizadoraModelo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'PulverizadoraAnio'); ?>
		<?php echo $form->textField($model,'PulverizadoraAnio',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'PulverizadoraAnio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'PulverizadoraVolumen'); ?>
		<?php echo $form->textField($model,'PulverizadoraVolumen'); ?>
		<?php echo $form->error($model,'PulverizadoraVolumen'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'IdTractorista'); ?>
		<?php echo $form->dropDownList($model,'IdTractorista',CHtml::listData(Tractorista::model()->findAll('IdCuaderno=:idCuaderno',array(':idCuaderno'=>$cuaderno->Id)), 'Id', 'NombreApellido')); ?>
		<?php echo $form->error($model,'IdTractorista'); ?>
	</div>

	<div class="row buttons">
		<?php echo TbHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->