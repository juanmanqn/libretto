<?php
/* @var $this EquipoController */
/* @var $model Equipo */

$this->breadcrumbs=array(
        $cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
        'Equipos' => array('admin','idCuaderno'=> $cuaderno->Id ),
	'Modificar #'.$model->Id,
);

?>

<h1>Modificar Equipo <?php echo $model->Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,'cuaderno'=>$cuaderno)); ?>