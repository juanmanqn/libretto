<?php
/* @var $this EquipoController */
/* @var $model Equipo */
/* @var $cuaderno Cuaderno */

$this->breadcrumbs=array(
	$cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Equipos' => array('equipo/admin', 'idCuaderno'=> $cuaderno->Id),
        'Nuevo'
);

//$this->idCuaderno=$cuaderno->Id;
?>

<h1>Nuevo Equipo</h1>

<?php $this->renderPartial('_form', array('model'=>$model,'cuaderno'=>$cuaderno)); ?>