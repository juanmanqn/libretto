<?php
/* @var $this EquipoController */
/* @var $model Equipo */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'Id'); ?>
		<?php echo $form->textField($model,'Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdCuaderno'); ?>
		<?php echo $form->textField($model,'IdCuaderno'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NumeroEquipo'); ?>
		<?php echo $form->textField($model,'NumeroEquipo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TractorMarca'); ?>
		<?php echo $form->textField($model,'TractorMarca',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TractorModelo'); ?>
		<?php echo $form->textField($model,'TractorModelo',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TractorAnio'); ?>
		<?php echo $form->textField($model,'TractorAnio'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TractorPotencia'); ?>
		<?php echo $form->textField($model,'TractorPotencia'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'PulverizadoraMarca'); ?>
		<?php echo $form->textField($model,'PulverizadoraMarca',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'PulverizadoraModelo'); ?>
		<?php echo $form->textField($model,'PulverizadoraModelo',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'PulverizadoraAnio'); ?>
		<?php echo $form->textField($model,'PulverizadoraAnio',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'PulverizadoraVolumen'); ?>
		<?php echo $form->textField($model,'PulverizadoraVolumen'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdTractorista'); ?>
		<?php echo $form->textField($model,'IdTractorista'); ?>
	</div>

	<div class="row buttons">
		<?php echo TbHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->