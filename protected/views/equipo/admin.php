<?php
/* @var $this EquipoController */
/* @var $model Equipo */

$this->breadcrumbs=array(	
        $cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
        'Equipos'

);

$this->menu=array(	
	array('label'=>'Nuevo Equipo', 'url'=>array('create', 'idCuaderno'=> $cuaderno->Id)),
);

$this->idCuaderno=$cuaderno->Id;
?>

<h1>Equipos</h1>


<?php $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => TbHtml::GRID_TYPE_STRIPED,
	'id'=>'equipo-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'Id',
//		'IdCuaderno',
		'NumeroEquipo',
		'TractorMarca',
		'TractorModelo',
		'TractorAnio',		
		//'TractorPotencia',
		'PulverizadoraMarca',
		'PulverizadoraModelo',
		'PulverizadoraAnio',
		//'PulverizadoraVolumen',
		'idTractorista.NombreApellido',
		
		array(
			'class'=>'TbButtonColumn',
		),
	),
)); ?>
