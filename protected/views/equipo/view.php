<?php
/* @var $this EquipoController */
/* @var $model Equipo */

$this->breadcrumbs=array(
        $cuaderno->idExplotacion->NombreChacra => array('/explotacion/view', 'id' => $cuaderno->IdExplotacion),
	'RENSPA: '.$cuaderno->NumeroRenspa => array('/cuaderno/view', 'id'=> $cuaderno->Id),
	'Equipos' => array('admin','idCuaderno'=> $cuaderno->Id ),
	$model->NumeroEquipo,
);

$this->menu=array(
	array('label'=>'Modificar Equipo', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Eliminar Equipo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Esta seguro de querer eliminar este item?')),
	);
?>

<h1>Equipo #<?php echo $model->Id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Id',
		'IdCuaderno',
		'NumeroEquipo',
		'TractorMarca',
		'TractorModelo',
		'TractorAnio',
		'TractorPotencia',
		'PulverizadoraMarca',
		'PulverizadoraModelo',
		'PulverizadoraAnio',
		'PulverizadoraVolumen',
		'idTractorista.NombreApellido',
	),
)); ?>
