<?php
/* @var $this EquipoController */
/* @var $data Equipo */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Id), array('view', 'id'=>$data->Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdCuaderno')); ?>:</b>
	<?php echo CHtml::encode($data->IdCuaderno); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NumeroEquipo')); ?>:</b>
	<?php echo CHtml::encode($data->NumeroEquipo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TractorMarca')); ?>:</b>
	<?php echo CHtml::encode($data->TractorMarca); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TractorModelo')); ?>:</b>
	<?php echo CHtml::encode($data->TractorModelo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TractorAnio')); ?>:</b>
	<?php echo CHtml::encode($data->TractorAnio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TractorPotencia')); ?>:</b>
	<?php echo CHtml::encode($data->TractorPotencia); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('PulverizadoraMarca')); ?>:</b>
	<?php echo CHtml::encode($data->PulverizadoraMarca); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PulverizadoraModelo')); ?>:</b>
	<?php echo CHtml::encode($data->PulverizadoraModelo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PulverizadoraAnio')); ?>:</b>
	<?php echo CHtml::encode($data->PulverizadoraAnio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PulverizadoraVolumen')); ?>:</b>
	<?php echo CHtml::encode($data->PulverizadoraVolumen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdTractorista')); ?>:</b>
	<?php echo CHtml::encode($data->IdTractorista); ?>
	<br />

	*/ ?>

</div>