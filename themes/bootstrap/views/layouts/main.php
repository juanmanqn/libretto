<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->
	
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
        <?php Yii::app()->bootstrap->register(); ?>        
        <style>
            body { background-color: #f6f6f6 !important; }
        </style>
        
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
        
        <?php if(Yii::app()->request->getParam("tab") != null): ?>
            <script>
                $(document).ready(function() {
                    $('#tabs a:contains("<?php echo CHttpRequest::getParam("tab"); ?>")').tab('show');
                });
            </script>
        <?php endif; ?>        
</head>

<body>

<div class="container" id="page">

	<div id="header">
            <div id="logo">
                <div style="float:left">
                    <h1>Cuaderno de Campo </h1><small style="color: #cccccc">Cuaderno Fitosanitario</small>
                </div>
                <div style="float:right; max-width:100%;">
                    <img alt="logos" src="<?php echo Yii::app()->request->baseUrl; ?>/images/logos.png"></img>
                </div>
            </div>
            <hr/>
                <?php // TbHtml::pageHeader('Cuaderno de Campo', 'Cuaderno Fitosanitario'); ?>                      
	</div><!-- header -->

	<div id="menu">
        <?php $this->widget('bootstrap.widgets.TbNav', array(
            'id' => 'tabs',
            'type' => TbHtml::NAV_TYPE_TABS,
            'items' => array(
                array('label' => 'Inicio', 'url' => array('/site/index')),
                array('label' => 'Cuaderno', 'items' => array(
                            array('label' => 'Listar', 'url' => array('/cuaderno/admin')),
                            array('label' => 'Nuevo', 'url' => array('/cuaderno/create')),
                    ),'visible'=>Yii::app()->user->checkAccess('administrador')),
                array('label'=>'Explotaciones', 'items' => array(
                            array('label' => 'Listar', 'url' => array('/explotacion/admin')),
                            array('label' => 'Nuevo', 'url' => array('/explotacion/create')),
                    ),'visible'=>Yii::app()->user->checkAccess('administrador')),
                array('label'=>'LaboresCulturales', 'items' => array(
                            array('label' => 'Listar', 'url' => array('/laborCultural/admin')),
                            array('label' => 'Nuevo', 'url' => array('/laborCultural/create')),
                    ),'visible'=>Yii::app()->user->checkAccess('administrador')),
                array('label'=>'Productores', 'items' => array(
                            array('label' => 'Listar', 'url' => array('/productor/admin')),
                            array('label' => 'Nuevo', 'url' => array('/productor/create')),
                    ),'visible'=>Yii::app()->user->checkAccess('administrador')),
                array('label'=>'Profesionales', 'items' => array(
                            array('label' => 'Listar', 'url' => array('/profesional/admin')),
                            array('label' => 'Nuevo', 'url' => array('/profesional/create')),
                    ),'visible'=>Yii::app()->user->checkAccess('administrador')),
                array('label'=>'Equipos', 'items' => array(
                            array('label' => 'Listar', 'url' => array('/equipo/admin')),
                            array('label' => 'Nuevo', 'url' => array('/equipo/create')),
                    ),'visible'=>Yii::app()->user->checkAccess('administrador')),
                array('label'=>'Contacto', 'url'=>array('/site/contact')),
                array('label'=>'Ingresar', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
                array('label'=>'Cerrar Sesión ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
            ),
        )); 
        ?>
        </div><!-- mainmenu -->
        
	<div id="breadcrumbs">
            <?php if(isset($this->breadcrumbs)):?>
            <?php 
                $this->widget('bootstrap.widgets.TbBreadcrumb', array(
                    'links' => $this->breadcrumbs,
                    'htmlOptions'=>array('style'=>'background-color:#FFF; padding-top:0px;')
                )); 
            ?> 
            <?php endif?>
        </div>
        
	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by JMR.<br/>
		Todos los derechos reservados.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
