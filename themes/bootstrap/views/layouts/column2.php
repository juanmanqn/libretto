<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="span-19">
	<div id="content">
		<?php echo $content; ?>
	</div><!-- content -->
</div>
<div class="span-6 last" style="margin-left: 60px;">
	<div class="well" style="max-width: 300px; padding: 8px 0;">
        <?php    
            echo TbHtml::stackedPills(
                array_merge(array(array('label'=>'Operaciones')), $this->menu)                  
            );            
        ?>    
        </div><!-- Sidebar -->
</div>
<?php $this->endContent(); ?>